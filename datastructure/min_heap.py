import math


class MinIntHeap:
    def __init__(self):
        self.size = 0
        self.items = []

    def leftchildrenindex(self, parent_index):
        return 2 * parent_index + 1

    def rightchildrenindex(self, parent_index):
        return 2 * parent_index + 2

    def parentindex(self, childindex):
        return int(math.ceil((childindex - 2) / 2))

    def hasparent(self, childindex):
        return self.parentindex(childindex) >= 0

    def hasleftchild(self, parentindex):
        return self.leftchildrenindex(parentindex) <= self.size

    def hasrightchild(self, parentindex):
        return self.rightchildrenindex(parentindex) <= self.size

    def getParent(self, childindex):
        return self.items[self.parentindex(childindex)]

    def getleftchild(self, parentindex):
        return self.items[self.leftchildrenindex(parentindex)]

    def getrightchild(self, parentindex):
        return self.items[self.rightchildrenindex(parentindex)]

    def peek(self):
        return self.items[0]

    def swap(self, indexone, indextwo):
        temp = self.items[indexone]
        self.items[indexone] = self.items[indextwo]
        self.items[indextwo] = temp

    def poll(self):
        item = self.items[0]
        self.items[0] = self.items[self.size]
        self.items.pop(self.size)
        self.size = self.size - 1
        self.heapify_down()
        return item

    def heapify_down(self):
        index = 0
        while(self.hasleftchild(index)):
            smallerchildindex = self.leftchildrenindex(index)
            if(self.hasrightchild(index) and self.items[self.rightchildrenindex(index)] <
                    self.items[smallerchildindex]):
                smallerchildindex = self.getrightchild(index)

            if self.items[index] < self.items[smallerchildindex]:
                break
            else:
                self.swap(index, smallerchildindex)

            index = smallerchildindex

    def add(self, item):
        self.items.append(item)
        self.size = self.size + 1
        self.heapifyUp()

    def heapifyUp(self):
        index = self.size - 1
        while self.hasparent(index) and self.items[index] < self.items[self.parentindex(index)]:
            self.swap(index, self.parentindex(index))
            index = self.parentindex(index)


if __name__ == "__main__":
    min_heap = MinIntHeap()
    min_heap.size = 0
    min_heap.add(2)
    min_heap.add(10)
    min_heap.add(8)
    min_heap.add(3)
    min_heap.add(1)
    min_heap.add(16)


    # min_heap.items = [2, 5, 7, 8, 10, 8, 9, 9, 11, 12, 13, 14, 15, 16, 80]
    print(min_heap.items)
    # min_heap.poll()
    # print(min_heap.items)
