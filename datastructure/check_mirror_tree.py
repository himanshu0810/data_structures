

class Node:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BT:

    def __init__(self):
        self.root = None

    def insert_node(self, data):
        if self.root == None:
            self.root = Node(data)
            return self.root

        this_level = [self.root]
        while this_level:
            current_node = this_level.pop(0)
            if current_node.left and current_node.right:
                this_level.append(current_node.left)
                this_level.append(current_node.right)
            elif current_node.left:
                current_node.right = Node(data)
                break
            else:
                current_node.left = Node(data)
                break
        return

    def preorder(self, root):
        print(root.data)
        if root.left:
            self.preorder(root.left)
        if root.right:
            self.preorder(root.right)

    def inorder(self, root):
        if root.left:
            self.inorder(root.left)
        print(root.data)
        if root.right:
            self.inorder(root.right)


def mirror_tree(root1, root2):

    if not root1 and not root2:
        return True

    if not root1:
        return False

    if not root2:
        return False

    if (root1.data == root2.data and mirror_tree(root1.left, root2.right) and
            mirror_tree(root1.right, root2.left)):
        return True

    return False


if __name__ == "__main__":

    bt = BT()
    root = bt.insert_node(5)
    bt.insert_node(3)
    bt.insert_node(10)
    bt.insert_node(15)
    bt.insert_node(20)


    bt1 = BT()
    root1 = bt1.insert_node(5)
    bt1.insert_node(10)
    bt1.insert_node(3)
    bt1.insert_node(2)
    bt1.insert_node(1)

    print(mirror_tree(root, root1))

