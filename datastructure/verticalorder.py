

class Node:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BT:

    def __init__(self):
        self.root = None

    def insert_node(self, data):
        if self.root == None:
            self.root = Node(data)
            return self.root

        this_level = [self.root]
        while this_level:
            current_node = this_level.pop(0)
            if current_node.left and current_node.right:
                this_level.append(current_node.left)
                this_level.append(current_node.right)
            elif current_node.left:
                current_node.right = Node(data)
                break
            else:
                current_node.left = Node(data)
                break
        return

    def preorder(self, root):
        print(root.data)
        if root.left:
            self.preorder(root.left)
        if root.right:
            self.preorder(root.right)

    def inorder(self, root):
        if root.left:
            self.inorder(root.left)
        print(root.data, end=' ')
        if root.right:
            self.inorder(root.right)


def vertical_order_print(root, paths, column):
    if root == None:
        return paths

    if column in paths.keys():
        paths[column].append(root.data)
    else:
        paths[column] = [root.data]

    if root.left:
        paths = vertical_order_print(root.left, paths, column - 1)
    if root.right:
        paths = vertical_order_print(root.right, paths, column + 1)
    return paths


if __name__ == "__main__":
    bt = BT()
    root = bt.insert_node(5)
    bt.insert_node(3)
    bt.insert_node(15)
    bt.insert_node(2)
    bt.insert_node(7)
    bt.insert_node(10)
    bt.insert_node(5)

    bt.inorder(root)
    paths = {}
    paths = vertical_order_print(root, paths, 0)
    print("paths are")
    print(paths)

