


def binary_search(arr, item):
    if len(arr) == 0:
        return False

    mid = len(arr) // 2
    if arr[mid] == item:
        return True

    if arr[mid] < item:
        result = binary_search(arr[mid+1:], item)
    else:
        result = binary_search(arr[:mid], item)
    return result


if __name__ == "__main__":
    li = [2,4,6.9, 14, 20, 25, 38, 40]
    item = 6
    result = binary_search(li, item)
    print('result is ', result)

