class Node:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BT:

    def __init__(self):
        self.root = None

    def insert_node(self, data):
        if self.root == None:
            self.root = Node(data)
            return self.root

        this_level = [self.root]
        while this_level:
            current_node = this_level.pop(0)
            if current_node.left and current_node.right:
                this_level.append(current_node.left)
                this_level.append(current_node.right)
            elif current_node.left:
                current_node.right = Node(data)
                break
            else:
                current_node.left = Node(data)
                break
        return

    def inorder(self, root):
        if root.left:
            self.inorder(root.left)
        print(root.data, end=' ')
        if root.right:
            self.inorder(root.right)


def create_binary_tree(preorder, inorder):
    if not preorder:
        return None
    root = Node(preorder[0])
    if len(preorder) == 1:
        return root

    i = inorder.index(root.data)
    lin = inorder[:i]
    rin = inorder[i + 1:]
    lpre = preorder[1:i + 1]
    rpre = preorder[i + 1:]
    root.left = create_binary_tree(lpre, lin)
    root.right = create_binary_tree(rpre, rin)
    return root


def create_binary_tree_2(inorder, postorder):
    if not postorder:
        return None
    root = Node(postorder[-1])
    i = inorder.index(root.data)
    lin = inorder[:i]
    rin = inorder[i+1:]
    lpos = postorder[:i]
    rpos = postorder[i:-1]
    root.left = create_binary_tree(lin, lpos)
    root.right = create_binary_tree(rin, rpos)
    return root


if __name__ == "__main__":
    bt = BT()
    inorder = [0, 1, 5, 6, 7, 10, 11, 13, 15, 18, 20]
    preorder = [10, 5, 1, 0, 7, 6, 15, 11, 13, 18, 20]
    postorder = [0, 1, 6, 7, 5, 13, 11, 20, 18, 15, 10]

    root = create_binary_tree(preorder, inorder)
    bt.inorder(root)
    root = None
    bt = BT()
    print("")
    root = create_binary_tree_2(inorder, postorder)
    bt.inorder(root)
