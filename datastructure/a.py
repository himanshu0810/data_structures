
import math
import os
import random
import re
import sys
import inspect

class SinglyLinkedListNode:
    def __init__(self, node_data):
        self.data = node_data
        self.next = None

class SinglyLinkedList:
    def __init__(self):
        self.head = None
        self.tail = None

    def insert_node(self, node_data):
        node = SinglyLinkedListNode(node_data)

        if not self.head:
            self.head = node
        else:
            self.tail.next = node


        self.tail = node

def print_singly_linked_list(node, sep, fptr):
    while node:
        fptr.write(str(node.data))

        node = node.next

        if node:
            fptr.write(sep)



def findMergeNode(head1, head2):
    # traceback.print_stack(file=sys.stdout)
    f = inspect.currentframe()
    v = inspect.getargvalues(f)

    print(inspect.getmembers(f))
    # for arg in v.args:
    #     var_value = v.locals[arg]
    #     if var_value:
    #         print(arg, '=> ', var_value.data)


    if head1 == None or head2 == None:
        return

    findMergeNode(head1.next, head2)
    findMergeNode(head1, head2.next)
    print(head1.data, head2.data)

if __name__ == '__main__':


    llist1_count = 5

    llist1 = SinglyLinkedList()

    llist1_item = [1,2,3,4,3]

    for each in llist1_item:
        llist1.insert_node(each)

    llist2_count = 3

    llist2 = SinglyLinkedList()
    llist2_item = [1, 4, 3]

    for each in llist2_item:
        llist2.insert_node(each)

    ptr1 = llist1.head
    ptr2 = llist2.head


    index = 3
    for i in range(llist1_count):
        if i < index:
            ptr1 = ptr1.next

    ptr2 = llist2.head
    ptr2.next = ptr1

    ptr2 = ptr2.next
    result = findMergeNode(llist1.head, llist2.head)

