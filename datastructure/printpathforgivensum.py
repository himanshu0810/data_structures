

class Node:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BT:

    def __init__(self):
        self.root = None

    def insert_node(self, data):
        if self.root == None:
            self.root = Node(data)
            return self.root

        this_level = [self.root]
        while this_level:
            current_node = this_level.pop(0)
            if current_node.left and current_node.right:
                this_level.append(current_node.left)
                this_level.append(current_node.right)
            elif current_node.left:
                current_node.right = Node(data)
                break
            else:
                current_node.left = Node(data)
                break
        return

    def inorder(self, root):
        if root.left:
            self.inorder(root.left)
        print(root.data, end=' ')
        if root.right:
            self.inorder(root.right)

    def print_path_for_given_sum(self, root, sum):
        if root == None:
            return
        paths = []

        self.printpathforgivensum(root, paths, 0, sum)

    def printpathforgivensum(self, root, paths, pathlen, sum):
        print("first thing ", sum, root.data)
        if root == None:
            return

        sum = sum - root.data

        if sum < 0:
            return

        if sum == 0:
            if len(paths) > pathlen:
                paths[pathlen] = root.data
            else:
                paths.append(root.data)
            print(paths, pathlen)
            return

        if len(paths) > pathlen:
            paths[pathlen] = root.data
        else:
            paths.append(root.data)

        if root.left:
            self.printpathforgivensum(root.left, paths, pathlen + 1, sum)
        if root.right:
            self.printpathforgivensum(root.right, paths, pathlen + 1, sum)
        return


if __name__ == "__main__":
    bt = BT()
    root = bt.insert_node(5)
    bt.insert_node(3)
    bt.insert_node(15)
    bt.insert_node(2)
    bt.insert_node(7)
    bt.insert_node(10)

    bt.inorder(root)
    print("")
    print("path for given sum algo started")
    bt.print_path_for_given_sum(root, 20)


