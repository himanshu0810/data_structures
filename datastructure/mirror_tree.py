

class Node:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BT:

    def __init__(self):
        self.root = None

    def insert_node(self, data):
        if self.root == None:
            self.root = Node(data)
            return self.root

        this_level = [self.root]
        while this_level:
            current_node = this_level.pop(0)
            if current_node.left and current_node.right:
                this_level.append(current_node.left)
                this_level.append(current_node.right)
            elif current_node.left:
                current_node.right = Node(data)
                break
            else:
                current_node.left = Node(data)
                break
        return


    def preorder(self, root):
        print(root.data)
        if root.left:
            self.preorder(root.left)
        if root.right:
            self.preorder(root.right)


    def inorder(self, root):
        if root.left:
            self.inorder(root.left)
        print(root.data, end=' ')
        if root.right:
            self.inorder(root.right)

def create_mirror_tree(root):
    if root == None:
        return None

    left = create_mirror_tree(root.left)
    right = create_mirror_tree(root.right)

    root.right = left
    root.left = right
    return root

if __name__ == "__main__":
    bt = BT()
    root = bt.insert_node(5)
    bt.insert_node(3)
    bt.insert_node(15)
    bt.insert_node(2)
    bt.insert_node(7)
    bt.insert_node(1)
    bt.insert_node(10)

    root1 = root
    bt.inorder(root)
    create_mirror_tree(root)
    print("")
    bt.inorder(root1)