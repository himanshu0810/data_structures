
class Node:
    def __init__(self, data, parent):
        self.data = data
        self.balance_factor = 0
        self.parent = parent
        self.left = None
        self.right = None


class AVLTree:
    def __init__(self):
        self.root = None

    def insert(self, data):

        if self.root is None:
            self.root = Node(data, None)
            return self.root

        head = self.root
        while head:
            if head.data > data:
                if head.left:
                    head = head.left
                else:
                    head.left = Node(data, head.left)
                    self.update_balance(self.root)
                    break
            else:
                if head.right:
                    head = head.right
                else:
                    head.right = Node(data, head.right)
                    self.update_balance(self.root)
                    break
        return

    def update_balance(self, node):
        if node.balance_factor > 1 or node.balance_factor < -1:
            self.rebalance(node)

        if node.parent != None:
            if node.left:
                node.parent.balance_factor = node.parent.balance_factor + 1
            elif node.right:
                node.parent.balance_factor = node.parent.balance_factor - 1


    def preorder(self, root):
        if root.left:
            self.preorder(root.left)
        print(root.data, end=' ')
        if root.right:
            self.preorder(root.right)


if __name__ == "__main__":
    avltree = AVLTree()
    root = avltree.insert(10)
    avltree.insert(3)
    avltree.insert(20)
    avltree.insert(5)
    avltree.insert(17)
    avltree.insert(2)
    avltree.insert(19)
    avltree.insert(12)
    avltree.insert(11)

    avltree.preorder(root)




