from pprint import pprint


def updatepath(li):
    for i in range(len(li)):
        for j in range(len(li[0])):
            if i == 0 and j == 0:
                continue

            elif i == 0:
                li[i][j] = li[i][j] + li[i][j-1]
            elif j == 0:
                li [i][j] = li[i][j] + li[i-1][j]
            else:
                li[i][j] = max(li[i][j] + li[i-1][j], li[i][j] + li[i][j-1])

    return li

def bottomup(li):
    i = len(li) - 1
    j = len(li[0]) - 1
    print(li[i][j])
    print(i, j )
    while i > 0 and j > 0:
        upper = li[i-1][j]
        left = li[i][j-1]
        elem = max(upper, left)
        if elem == upper:
            print(i-1, j)
            i = i - 1
        else:
            print(i, j-1)
            j = j - 1



if  __name__ == "__main__":

    li = [[1, 2, 1, 3,1],
          [2, 1, 3, 4, 1],
          [1, 1, 2, 3, 1],
          [4, 1, 1, 5, 1],
          [1, 1, 1 ,1, 1]
          ]
    li = updatepath(li)
    path = bottomup(li)
    pprint(li)