class Node:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BT:

    def __init__(self):
        self.root = None

    def insert_node(self, data):
        if self.root == None:
            self.root = Node(data)
            return self.root

        this_level = [self.root]
        while this_level:
            current_node = this_level.pop(0)
            if current_node.left and current_node.right:
                this_level.append(current_node.left)
                this_level.append(current_node.right)
            elif current_node.left:
                current_node.right = Node(data)
                break
            else:
                current_node.left = Node(data)
                break
        return

    def inorder(self, root):
        if root.left:
            self.inorder(root.left)
        print(root.data, end=' ')
        if root.right:
            self.inorder(root.right)


def findLastSmaller(array, data):
    if len(array) == 1:
        return array[0]

    length = len(array)
    low = 0
    high = length - 1
    while(low < high and array[high] > data):
        print(low, high)
        mid = low + (high - low + 1) //2
        if array[mid] > data:
            high = mid -1
        else:
            low = mid
    return high



def create_bst_using_postorder(postorder, leftindex, rightindex):
    if len(postorder) == 0:
        return None

    if len(postorder) == 1:
        root = Node(postorder[-1])
        return root

    root = Node(postorder[-1])
    val = root.data
    leftlast = findLastSmaller(postorder[:-1], val)
    print("comin here", leftlast)
    root.left = create_bst_using_postorder(postorder[:leftlast], leftindex, leftindex )
    root.right = create_bst_using_postorder(postorder[leftlast:-1])
    return root


if __name__ == "__main__":
    bt = BT()
    # inorder = [0, 1, 5, 6, 7, 10, 11, 13, 15, 18, 20]
    # preorder = [10, 5, 1, 0, 7, 6, 15, 11, 13, 18, 20]
    postorder = [0, 1, 6, 7, 5, 13, 11, 20, 18, 15, 10]
    root = create_bst_using_postorder(postorder)
