
def create_bst_using_sorted_array(array, bst):
    # print("array is ", array)
    if len(array) == 0:
        return bst
    mid = len(array) // 2
    bst.insert_node(array[mid])
    create_bst_using_sorted_array(array[:mid], bst)
    create_bst_using_sorted_array(array[mid+1:], bst)
    return bst


class Node:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class Bst:
    def __init__(self):
        self.root = None

    def inorder(self, root=None):
        if not root:
            root= self.root

        if root.left:
            self.inorder(root.left)

        if root.right:
            self.inorder(root.right)
        print(root.data, end=' ')

    def inorder_with_return_list(self, root, li):
        if root.left:
            self.inorder_with_return_list(root.left, li)

        li.append(root.data)

        if root.right:
            self.inorder_with_return_list(root.right, li)

        return li

    def insert_node(self, data):
        if self.root == None:
            self.root = Node(data)
            return self.root

        head = self.root
        node = Node(data)
        while head != None:
            if data > head.data:
                if not head.right:
                    head.right = node
                    break
                head = head.right
            else:
                if not head.left:
                    head.left = node
                    break
                head = head.left

        return node

    def change_data_of_one_node(self, root, data, new_value):
        if root.left:
            self.change_data_of_one_node(root.left, data, new_value)

        if root.data == data:
            root.data = new_value
            return

        if root.right:
            self.change_data_of_one_node(root.right, data, new_value)

    def bst_validatity(self, root):
        li = []
        li = self.inorder_with_return_list(root, li)
        return all(li[i] < li[i+1] for i in range(len(li)-1))

    def find_faulty_node(self, root):
        li = []
        li = self.inorder_with_return_list(root, li)
        for i in range(len(li) - 1):
            if li[i] > li[i+1]:
                return li[i]
        return False



if __name__ == "__main__":
    bst = Bst()
    root = bst.insert_node(10)
    bst.insert_node(5)
    bst.insert_node(15)
    bst.insert_node(1)
    bst.insert_node(7)
    bst.insert_node(11)
    bst.insert_node(18)
    bst.insert_node(6)
    bst.insert_node(13)
    bst.insert_node(20)
    bst.insert_node(0)

    # In Order traversal
    bst.inorder(root)
    print("")
    # Change one node data with another
    # bst.change_data_of_one_node(root, 18, 32)
    # # Checking whether bst is valid or not
    # print("BST is valid - ", bst.bst_validatity(root))
    # # Returning what is the faulty node
    # print("faulty node is ", bst.find_faulty_node(root))
    # #
    # # print("---------")
    # li = [1,5,6,7,10,11,13,15,18,20]
    # bst2 = Bst()
    # bst2 = create_bst_using_sorted_array(li, bst2)
    # bst2.inorder()

