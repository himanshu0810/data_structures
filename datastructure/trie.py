

# Todo you need to add these strings in TRie
# Todo - 1. ab 2.aba 3.abc 4.ad 5. ba 6. bad 7. bag

class TrieNode:
    def __init__(self):
        self.end_of_word = False
        self.children = {}


class Trie:
    def __init__(self):
        self.root = TrieNode()

    def insert(self, word):
        current = self.root
        for i in range(len(word)):
            char = word[i]
            node = current.children.get(char)
            if not node:
                node = TrieNode()
                current.children[char] = node
            current = node
        current.end_of_word = True

    def search(self, word):
        current = self.root
        for i in range(len(word)):
            char = word[i]
            node = current.children.get(char)
            if not node:
                return False
            current = node
        return current.end_of_word

    def prefix_exist_or_not(self, prefix):
        current = self.root
        for i in range(len(prefix)):
            char = prefix[i]
            node = current.children.get(char)
            if not node:
                return False
            current = node
        return True


if __name__ == "__main__":
    trie_struct = Trie()
    trie_struct.insert("ab")
    trie_struct.insert("aba")
    trie_struct.insert("abc")
    trie_struct.insert("ad")
    trie_struct.insert("ba")
    trie_struct.insert("bad")
    trie_struct.insert("bag")
    trie_struct.insert("himanshu")

    result = trie_struct.search("himanshu")
    print("result is - ", result)

    prefix_result = trie_struct.prefix_exist_or_not("ba")
    print("prefix result or not - ", prefix_result)

