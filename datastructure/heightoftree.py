

class Node:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BT:

    def __init__(self):
        self.root = None
        self.diameter = 0

    def insert_node(self, data):
        if self.root == None:
            self.root = Node(data)
            return self.root

        this_level = [self.root]
        while this_level:
            current_node = this_level.pop(0)
            if current_node.left and current_node.right:
                this_level.append(current_node.left)
                this_level.append(current_node.right)
            elif current_node.left:
                current_node.right = Node(data)
                break
            else:
                current_node.left = Node(data)
                break
        return

    def height_of_tree(self, root):
        if root == None:
            return 0
        height_left = self.height_of_tree(root.left)
        height_right = self.height_of_tree(root.right)
        if height_left > height_right:
            height = height_left
        else:
            height = height_right
        return height + 1

    def find_max_diameter(self, root):
        if root == None:
            return 0
        height_left = self.find_max_diameter(root.left)
        height_right = self.find_max_diameter(root.right)
        height = height_left
        if height < height_right:
            height = height_right
        current_diameter = height_left + height_right
        if current_diameter > self.diameter:
            self.diameter = current_diameter
        return height + 1



if __name__ == "__main__":
    bt = BT()
    root = bt.insert_node(5)
    bt.insert_node(3)
    bt.insert_node(15)
    bt.insert_node(2)
    bt.insert_node(7)
    bt.insert_node(13)
    bt.insert_node(9)
    bt.insert_node(4)
    bt.insert_node(14)
    bt.insert_node(4)
    bt.insert_node(14)

    print(bt.height_of_tree(root) - 1)
    print(bt.find_max_diameter(root))
    print(bt.diameter)
