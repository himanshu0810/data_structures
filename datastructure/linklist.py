

class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:
    def __init__(self):
        self.head = None

    def insert(self, data):
        node = Node(data)
        if self.head == None:
            self.head = node
            return self.head

        root = self.head
        while root.next != None:
            root = root.next

        root.next = node
        return node

    def print(self):
        root = self.head
        while root != None:
            print(root.data, end=' ')
            root = root.next


def recursivesumbackwards(root, root1, count, remainder):
    if root == None and root1 == None:
        return

    if root1.next:
        count = count + recursivesumbackwards(root, root1.next)




if __name__ == "__main__":

    a = [2, 4, 3]
    b = [5, 6, 4]
    li = LinkedList()
    root = li.insert(2)
    li.insert(4)
    li.insert(3)

    li.print()
    print("")
    li1 = LinkedList()
    root1 = li1.insert(5)
    li1.insert(6)
    li1.insert(4)
    li1.print()
    print("")


