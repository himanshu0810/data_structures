

class Node:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BT:

    def __init__(self):
        self.root = None

    def insert_node(self, data):
        if self.root == None:
            self.root = Node(data)
            return self.root

        this_level = [self.root]
        while this_level:
            current_node = this_level.pop(0)
            if current_node.left and current_node.right:
                this_level.append(current_node.left)
                this_level.append(current_node.right)
            elif current_node.left:
                current_node.right = Node(data)
                break
            else:
                current_node.left = Node(data)
                break
        return

    def inorder(self, root):
        if root.left:
            self.inorder(root.left)
        print(root.data, end=' ')
        if root.right:
            self.inorder(root.right)

    def printroottoleaf(self, root):
        if root == None:
            return
        paths = []
        self.printroottoleafrecursion(root, paths, 0)

    def printroottoleafrecursion(self, root, paths, pathlen):
        if root == None:
            return

        if len(paths) > pathlen:
            paths[pathlen] = root.data
        else:
            paths.append(root.data)

        pathlen = pathlen + 1
        if root.left == None and root.right == None:
            print(paths)
        else:
            self.printroottoleafrecursion(root.left, paths, pathlen)
            self.printroottoleafrecursion(root.right, paths, pathlen)


if __name__ == "__main__":
    bt = BT()
    root = bt.insert_node(5)
    bt.insert_node(3)
    bt.insert_node(15)
    bt.insert_node(2)
    bt.insert_node(7)
    bt.insert_node(10)

    bt.inorder(root)
    print("")
    bt.printroottoleaf(root)

