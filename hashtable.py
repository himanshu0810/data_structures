
class Node:
    def __init__(self, key, value):
        self.key = key
        self.value = value
        self.next = None

    def __repr__(self):
        return self.value


class HashTable:
    def __init__(self):
        self.capacity = 8
        self.datas = [None] * self.capacity

    def get_hash(self, key):
        hashsum = 0
        length = len(key)
        for idx, ch in enumerate(key):
            hashsum += (idx + length) ^ ord(ch)
            hashsum = hashsum % self.capacity
        return hashsum

    def insert(self, key, value):
        hash_value = self.get_hash(key)
        first_node = self.datas[hash_value]
        if not first_node:
            self.datas[hash_value] = Node(key, value)
            return
        while first_node != None:
            prev = first_node
            first_node = first_node.next
        prev.next = Node(key, value)

    def find(self, key):
        hash_value = self.get_hash(key)
        first_node = self.datas[hash_value]
        while first_node is not None and first_node.key != key:
            first_node = first_node.next
        if first_node is None:
            return
        return first_node.value

    def remove(self, key):
        hash = self.get_hash(key)
        node = self.datas[hash]
        prev = None
        while node is not None and node.key != key:
            prev = node
            node = node.next
        if node is None:
            return
        result = node.value
        if prev is None:
            node = None
            return result
        prev.next = prev.next.next
        return result


if __name__ == "__main__":
    hashtable = HashTable()

    hashtable.insert("10", "10")
    hashtable.insert("120", "120")
    hashtable.insert("1201", "1201")
    hashtable.insert("1202", "1202")
    hashtable.insert("12", "12")
    hashtable.insert("121", "121") # will check later
    hashtable.insert("1212", "1212") # will check later

    print(hashtable.datas)
    # print(hashtable.find("10000"))
    # print(hashtable.find("1202"))
    # print(hashtable.find("121"))
    # print(hashtable.find("1212"))

    # print(hashtable.find("12123"))
    print(hashtable.remove("1202"))
    print(hashtable.datas)
