

if __name__ == "__main__":

    as1 = [1, 2, 3, 4, 1, 2, 3, 4, 2, 3]
    a = {}

    for each in as1:
        if each in a:
            a[each] = a[each] + 1
        else:
            a[each] = 1

    min_key, min_value = None, None

    for each in a.items():
        if min_value is None:
            min_key, min_value = each
            continue
        if each[1] > min_value or (each[1] == min_value and each[0] < min_key):
            min_key, min_value = each

    print(min_key, min_value)
