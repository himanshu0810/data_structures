
# Return positions with O(m*O(n-m+1))


def return_pattern_found_and_index(n, m):
    positions = []
    if len(m) > len(n):
        return positions

    small_length = len(m)

    for idx in range(len(n)-len(m)):

        big_idx = idx

        for small_idx, m_ch in enumerate(m):
            if m_ch != n[big_idx]:
                small_idx = small_idx - 1
                break
            big_idx = big_idx + 1

        if (small_length - 1) == small_idx:
            positions.append(idx)

    return positions


if __name__ == "__main__":
    n = "AAAAABAAABA"
    m = "AAAA"
    pattern_found = return_pattern_found_and_index(n, m)
    print("Pattern found are ",  pattern_found)
