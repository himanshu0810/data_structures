

def create_pattern_array(string):
    pattern = []
    j = 0
    for i, char in enumerate(string):
        if i == 0:
            pattern.append(0)
        elif string[j] == char:
            j = j + 1
            pattern.append(j)
        elif j == 0:
            pattern.append(0)
        else:
            while True:
                j = pattern[j-1]
                j_char = string[j]
                if j_char == char:
                    j = j + 1
                    pattern.append(j)
                    break
                elif j == 0:
                    pattern.append(0)
                    break
    return pattern


def return_pattern_starting_index(string, pattern, pattern_array):
    j = 0
    for i in range(len(string)):
        if j == len(pattern):
            return i
        char = string[i]
        if j == len(pattern):
            return i
        if char == pattern[j]:
            j = j + 1
        elif j == 0:
            continue
        else:
            j = pattern_array[j-1]
            j_char = pattern[j]
            if j_char == char:
                j = j + 1
    if j == len(pattern):
        return i - len(pattern) + 1
    return None

if __name__ == "__main__":
    string = "ABABDABACDABABCABAB"
    pattern = "ABABCABAB"
    pattern_array = create_pattern_array(pattern)
    print("pattern array is ", pattern_array)
    find_pattern = return_pattern_starting_index(string, pattern, pattern_array)
    print("find pattern is ", find_pattern)