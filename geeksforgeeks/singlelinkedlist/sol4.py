
# Create a linked list - learning how to insert into linked list


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:
    def __init__(self):
        self.head = None

    def insert(self, data):
        if not self.head:
            self.head = Node(data)
            return self.head

        node = self.head
        while node.next:
            node = node.next
        node.next = Node(data)
        return

    def print(self):
        if not self.head:
            return
        node = self.head
        while node:
            print(node.data, end=' ')
            node = node.next
        print()

    def delete_with_key(self, key):
        if not self.head:
            return

        if self.head.data == key:
            self.head = self.head.next
            return

        node = self.head
        while node.next and node.next.data != key:
            node = node.next

        if node.next is None:
            return

        node.next = node.next.next
        return


if __name__ == "__main__":
    linked_list = LinkedList()
    linked_list.insert(10)
    linked_list.insert(1)
    linked_list.insert(3)
    linked_list.insert(4)
    linked_list.insert(5)
    linked_list.insert(6)
    linked_list.print()
    linked_list.delete_with_key(10)
    linked_list.delete_with_key(3)
    linked_list.delete_with_key(6)
    linked_list.print()
