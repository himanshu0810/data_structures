
# Length of linked list iteratively and recursively


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:
    def __init__(self):
        self.head = None

    def print(self):
        node = self.head
        while node:
            print(node.data, end=' ')
            node = node.next

    def insert(self, data):
        node = Node(data)
        if not self.head:
            self.head = node
            return

        root = self.head
        while root.next:
            root = root.next

        root.next = node

    def return_length(self):
        count = 0
        root = self.head
        while root:
            count = count + 1
            root = root.next
        return count

    def return_length_recursively(self, head):
        if not head:
            return 0

        return 1 + self.return_length_recursively(head.next)


if __name__ == "__main__":
    linked_list = LinkedList()
    linked_list.insert(8)
    linked_list.insert(2)
    linked_list.insert(3)
    linked_list.insert(1)
    linked_list.insert(7)
    linked_list.print()
    print()
    print(linked_list.return_length())
    print(linked_list.return_length_recursively(linked_list.head))
