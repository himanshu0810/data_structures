
# Complete - Merge two sorted list


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:
    def __init__(self):
        self.head = None

    def insert(self, data):
        if not self.head:
            self.head = Node(data)
            return self.head

        node = self.head
        while node.next:
            node = node.next
        node.next = Node(data)
        return

    def print(self):
        if not self.head:
            return
        node = self.head
        while node:
            print(node.data, end=' ')
            node = node.next
        print()


def sort_two_merged_lists(ll1, ll2):
    ll3 = LinkedList()

    if ll1.head is None and ll2.head is None:
        return ll3

    head_1, head_2 = ll1.head, ll2.head
    last = None

    while head_1 is not None and head_2 is not None:
        if head_1.data < head_2.data:
            node = Node(head_1.data)
            if ll3.head is None:
                ll3.head = node
            else:
                last.next = node
            last = node
            head_1 = head_1.next
        else:
            node = Node(head_2.data)
            if ll3.head is None:
                ll3.head = node
            else:
                last.next = node
            last = node
            head_2 = head_2.next

    if head_1 is None and head_2 is None:
        return ll3

    if head_1 is None:
        head = head_2
    else:
        head = head_1

    while head is not None:
        node = Node(head.data)
        if last is None:
            ll3.head = node
        else:
            last.next = node
        last = node
        head = head.next

    return ll3


if __name__ == "__main__":
    ll1 = LinkedList()
    ll1.insert(5)
    ll1.insert(10)
    ll1.insert(15)
    ll1.print()

    ll2 = LinkedList()
    ll2.insert(2)
    ll2.insert(3)
    ll2.insert(20)
    ll2.print()

    ll3 = sort_two_merged_lists(ll1, ll2)
    ll3.print()
