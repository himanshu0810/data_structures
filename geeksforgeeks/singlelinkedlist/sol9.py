
# Complete - Swap two nodes without swapping data


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:
    def __init__(self):
        self.head = None

    def print(self):
        root = self.head
        while root:
            print(root.data, end=' ')
            root = root.next
        print()

    def swap_two_nodes(self, data1, data2):
        prev1 = prev2 = None
        node1 = node2 = self.head

        while node1.data != data1:
            prev1 = node1
            node1 = node1.next

        while node2.data != data2:
            prev2 = node2
            node2 = node2.next

        if prev1 is None:
            self.head = node2
        else:
            prev1.next = node2

        if prev2 is None:
            self.head = node1
        else:
            prev2.next = node1

        next_node2 = node2.next
        node2.next = node1.next
        node1.next = next_node2


if __name__ == "__main__":
    linked_list = LinkedList()
    linked_list.head = Node(10)
    linked_list.head.next = Node(20)
    linked_list.head.next.next = Node(3)
    linked_list.head.next.next.next = Node(5)
    linked_list.head.next.next.next.next = Node(8)
    linked_list.head.next.next.next.next.next = Node(1)

    linked_list.print()
    linked_list.swap_two_nodes(10, 20)
    print("Printing after swap two nodes ")
    linked_list.print()
