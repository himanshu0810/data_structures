

class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

    def __repr__(self):
        return str(self.data)


class LinkedList:
    def __init__(self):
        self.head = None

    def insert(self, data):
        head = self.head
        node = Node(data)
        if not head:
            self.head = node
            return node

        while head.next is not None:
            head = head.next

        head.next = node
        return node


def print_list(node):
    while node is not None:
        print(node.data, end=' ')
        node = node.next
    print()


def merge_both_lists_recursively(head1, head2):

    if not head1:
        return head2

    if not head2:
        return head1

    if head1.data < head2.data:
        temp = head1
        temp.next = merge_both_lists_recursively(head1.next, head2)
    else:
        temp = head2
        temp.next = merge_both_lists_recursively(head1, head2.next)

    return temp


def return_sort(head):

    temp = head
    if head is not None:
        slow = head
        fast = head
        if slow.next is None:
            return slow

        while fast.next is not None:
            fast = fast.next
            if fast.next is None:
                break
            fast = fast.next
            slow = slow.next

        head1 = slow.next
        slow.next = None

        left = return_sort(head)
        right = return_sort(head1)
        temp = merge_both_lists_recursively(left, right)

    return temp


if __name__ == "__main__":
    ll = LinkedList()
    ll1 = LinkedList()

    ll.insert(5)
    ll.insert(4)
    ll.insert(1)
    ll.insert(10)
    ll.insert(9)
    ll.insert(8)

    print_list(ll.head)

    temp = return_sort(ll.head)

    print("Sorted list is ")
    print_list(temp)
