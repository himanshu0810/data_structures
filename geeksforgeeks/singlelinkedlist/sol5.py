
# Create a linked list with the position


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:
    def __init__(self):
        self.head = None

    def insert(self, data):
        if not self.head:
            self.head = Node(data)
            return self.head

        node = self.head
        while node.next:
            node = node.next
        node.next = Node(data)
        return

    def print(self):
        if not self.head:
            return
        node = self.head
        while node:
            print(node.data, end=' ')
            node = node.next
        print()

    def delete_with_position(self, position):
        if not self.head:
            return
        elif position == 0:
            self.head = self.head.next
            return

        node = self.head
        prev = None
        while node.next and position > 0:
            prev = node
            node = node.next
            position = position - 1

        if position > 0:
            return

        prev.next = node.next
        return


if __name__ == "__main__":
    linked_list = LinkedList()
    linked_list.insert(10)
    linked_list.insert(1)
    linked_list.insert(3)
    linked_list.insert(4)
    linked_list.insert(5)
    linked_list.insert(6)
    linked_list.print()
    linked_list.delete_with_position(4)
    linked_list.print()
