
# Complete - Detect and Remove loop in linked list


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

    def __repr__(self):
        return str(self.data)


class LinkedList:
    def __init__(self):
        self.head = None

    def insert(self, data):
        if not self.head:
            self.head = Node(data)
            return self.head

        node = self.head
        while node.next:
            node = node.next
        node.next = Node(data)
        return node.next

    def print(self):
        if not self.head:
            return
        node = self.head
        while node:
            print(node.data, end=' ')
            node = node.next
        print()

    def detect_and_remove_loop(self):
        node1 = self.head
        node2 = self.head
        while node1 is not None and node2 is not None:
            node1 = node1.next
            node2 = node2.next
            node2 = node2.next

            if node1 == node2:
                node1, prev2 = self.head, None
                while node1 != node2:
                    node1 = node1.next
                    prev2 = node2
                    node2 = node2.next

                prev2.next = None
                return True

        return False

    def detect_and_remove_loop_1(self):
        node1 = self.head
        map = {}
        prev = None
        while node1 is not None:
            if node1.data in map:
                prev.next = None
                return True
            map[node1.data] = node1
            prev = node1
            node1 = node1.next
        return False


if __name__ == "__main__":
    ll = LinkedList()
    ll.insert(1)
    ll.insert(2)
    node1 = ll.insert(3)
    ll.insert(4)
    ll.insert(5)
    ll.insert(6)
    ll.insert(7)
    ll.insert(8)
    node2 = ll.insert(9)

    node2.next = node1

    ll.detect_and_remove_loop_1()
    ll.print()
