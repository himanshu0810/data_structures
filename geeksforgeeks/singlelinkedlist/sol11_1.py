

class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

    def __repr__(self):
        return str(self.data)


class LinkedList:
    def __init__(self):
        self.head = None

    def insert(self, data):
        head = self.head
        node = Node(data)
        if not head:
            self.head = node
            return node

        while head.next is not None:
            head = head.next

        head.next = node
        return node


def print_list(node):
    while node is not None:
        print(node.data, end=' ')
        node = node.next
    print()


def merge_both_lists_iteratively(head1, head2):

    dummy = Node(0)
    tail = dummy
    while True:

        if not head2:
            tail.next = head1
            break
        elif not head1:
            tail.next = head2
            break
        elif head1.data < head2.data:
            tail.next = head1
            tail = head1
            head1 = head1.next
        else:
            tail.next = head2
            tail = head2
            head2 = head2.next
    return dummy.next


def merge_both_lists_iteratively_1(head1, head2):

    head = dummy = None

    while True:
        if not head2:
            if not head:
                return head1
            head.next = head1
            break
        elif not head1:
            if not head:
                return head2
            head.next = head2
            break
        elif head1.data < head2.data:
            if not head:
                head = head1
                dummy = head1
            else:
                head.next = head1
                head = head1
            head1 = head1.next
        else:
            if not head:
                head = head2
                dummy = head2
            else:
                head.next = head2
                head = head2
            head2 = head2.next
    return dummy


def merge_both_lists_recursively(head1, head2):

    if not head1:
        return head2

    if not head2:
        return head1

    if head1.data < head2.data:
        temp = head1
        temp.next = merge_both_lists_recursively(head1.next, head2)
    else:
        temp = head2
        temp.next = merge_both_lists_recursively(head1, head2.next)

    return temp


if __name__ == "__main__":
    ll = LinkedList()
    ll1 = LinkedList()

    ll.insert(5)
    ll.insert(10)
    ll.insert(15)

    ll1.insert(2)
    ll1.insert(3)
    ll1.insert(20)

    # np = merge_both_lists_iteratively(ll.head, ll1.head)
    # np = merge_both_lists_iteratively_1(ll.head, ll1.head)
    np = merge_both_lists_recursively(ll.head, ll1.head)
    print_list(ll.head)
    print_list(ll1.head)
    print("After merging both lists")
    print_list(np)
