
# Complete - Add two numbers represented by linked lists


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

    def __repr__(self):
        return str(self.data)


class LinkedList:
    def __init__(self):
        self.head = None

    def insert(self, data):
        if not self.head:
            self.head = Node(data)
            return self.head

        node = self.head
        while node.next:
            node = node.next
        node.next = Node(data)
        return node.next

    def print(self):
        if not self.head:
            return
        node = self.head
        while node:
            print(node.data, end=' ')
            node = node.next
        print()


def return_add_two_numbers(ll, ll1):
    carry = 0
    ll3 = LinkedList()
    prev_node = None
    node1, node2 = ll.head, ll1.head
    while node1 is not None or node2 is not None:
        data1 = node1.data if node1 is not None else 0
        data2 = node2.data if node2 is not None else 0
        sum_of_nodes = data1 + data2 + carry
        carry, digit = sum_of_nodes // 10, sum_of_nodes % 10

        node = Node(digit)
        if ll3.head is None:
            ll3.head = node
        else:
            prev_node.next = node

        prev_node = node
        node1 = node1.next if node1 is not None else None
        node2 = node2.next if node2 is not None else None

    if node1 is None and node2 is None:
        if carry != 0:
            node = Node(carry)
            prev_node.next = node
            return ll3

    return ll3


if __name__ == "__main__":
    ll = LinkedList()
    ll.insert(9)
    ll.insert(9)
    ll.insert(9)
    ll.insert(3)
    ll.insert(1)
    ll.insert(3)

    ll1 = LinkedList()
    ll1.insert(9)
    ll1.insert(9)
    ll1.insert(9)

    ll3 = return_add_two_numbers(ll, ll1)
    ll3.print()
