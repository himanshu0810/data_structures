
# Reverse a linked list in groups of given size


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

    def __repr__(self):
        return str(self.data)


class LinkedList:
    def __init__(self):
        self.head = None

    def insert(self, data):
        if not self.head:
            self.head = Node(data)
            return self.head

        node = self.head
        while node.next:
            node = node.next
        node.next = Node(data)
        return

    def print(self):
        if not self.head:
            return
        node = self.head
        while node:
            print(node.data, end=' ')
            node = node.next
        print()

    def reverse_linked_list_with_group_size_util(self, node, k):
        if node is None:
            return node, None

        if k == 1:
            return node, node.next

        head, new_head = self.reverse_linked_list_with_group_size_util(node.next, k-1)
        if node.next is None:
            return node, new_head
        node.next.next = node
        node.next = None
        return head, new_head

    def reverse_linked_list_with_group_size(self, k):
        head = self.head
        new_head = tail = None
        while head is not None:
            head_1, head_2 = self.reverse_linked_list_with_group_size_util(head, k)
            if new_head is None:
                new_head = head_1
            if tail is not None:
                tail.next = head_1
            tail = head
            head = head_2

        return new_head


if __name__ == "__main__":
    ll = LinkedList()
    ll.insert(1)
    ll.insert(2)
    ll.insert(3)
    ll.insert(4)
    ll.insert(5)
    ll.insert(6)
    ll.insert(7)
    ll.insert(8)

    k = 3
    new_head = ll.reverse_linked_list_with_group_size(k)
    ll.head = new_head
    ll.print()
