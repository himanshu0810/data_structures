
# Complete - Rotate a linked list after k constants


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

    def __repr__(self):
        return str(self.data)


class LinkedList:
    def __init__(self):
        self.head = None

    def insert(self, data):
        if not self.head:
            self.head = Node(data)
            return self.head

        node = self.head
        while node.next:
            node = node.next
        node.next = Node(data)
        return node.next

    def print(self):
        if not self.head:
            return
        node = self.head
        while node:
            print(node.data, end=' ')
            node = node.next
        print()

    def rotate_linked_list_iteratively(self, k):

        head_1 = head_2 = self.head

        while k > 1:
            head_1 = head_1.next
            k = k - 1

        node = head_1.next
        self.head = node
        head_1.next = None
        while node.next is not None:
            node = node.next
        node.next = head_2


if __name__ == "__main__":
    ll = LinkedList()
    ll.insert(10)
    ll.insert(20)
    ll.insert(30)
    ll.insert(40)
    ll.insert(50)
    ll.insert(60)

    ll.rotate_linked_list_iteratively(3)
    ll.print()
