

class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:
    def __init__(self):
        self.head = None

    def print(self):
        if not self.head:
            return

        node = self.head

        while node is not None:
            print(node.data, end=' ')
            node = node.next

    def insert(self, data):
        if not self.head:
            self.head = Node(data)
            return

        node = self.head
        while node.next is not None:
            node = node.next

        new_node = Node(data)
        node.next = new_node
        return new_node

    def reverse_linked_list_util(self, node):
        if node is None:
            return

        if node.next is None:
            return node

        head = self.reverse_linked_list_util(node.next)
        node.next.next = node
        node.next = None
        return head

    def reverse_linked_list(self):
        self.head = self.reverse_linked_list_util(self.head)
        return

    def reverse_linked_list_iteratively(self):
        node = self.head
        if node.next is None:
            return node

        prev = None
        curr = node
        next = node.next
        while next is not None:
            curr.next = prev
            prev = curr
            temp = next.next
            next.next = curr
            curr = next
            next = temp

        self.head = curr
        return


if __name__ == "__main__":
    linked_list = LinkedList()
    linked_list.insert(10)
    linked_list.insert(1)
    linked_list.insert(3)
    linked_list.insert(9)
    linked_list.insert(5)
    linked_list.insert(6)

    linked_list.print()

    linked_list.reverse_linked_list()

    print()
    print("Linked list after reversing ")
    linked_list.print()

    linked_list.reverse_linked_list_iteratively()
    print()
    print("Linked list after reversing again ")
    linked_list.print()
