
# Complete - Reverse a linked list iteratively and recursively


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:
    def __init__(self):
        self.head = None

    def print(self):
        node = self.head
        while node:
            print(node.data, end=' ')
            node = node.next

    def insert(self, data):
        node = Node(data)
        if not self.head:
            self.head = node
            return

        root = self.head
        while root.next:
            root = root.next

        root.next = node

    def reverse_link_list_iteratively(self):
        prev = None
        root = self.head

        if root is None or root.next is None:
            return

        while root:
            self.head = root
            curr = root.next
            root.next = prev
            prev = root
            root = curr

        return

    def reverse_link_list_recursively(self, node):
        if node is None:
            return

        head = self.reverse_link_list_recursively(node.next)
        if node.next is None:
            return node

        node.next.next = node
        node.next = None
        return head


if __name__ == "__main__":
    linked_list = LinkedList()
    linked_list.insert(8)
    linked_list.insert(2)
    linked_list.insert(3)
    linked_list.insert(1)
    linked_list.insert(7)
    linked_list.print()
    print()
    linked_list.reverse_link_list_iteratively()

    linked_list.print()
    print()
    head = linked_list.reverse_link_list_recursively(linked_list.head)
    linked_list.head = head
    linked_list.print()
    print()