import math
from functools import reduce


def return_primeFactors(n):

    li = []

    while n % 2 == 0:
        li.append(2)
        n = n // 2

    s1 = int(math.sqrt(n)) + 1
    # print(s1)
    for i in range(3, s1, 2):
        if n == 1.0:
            break

        while n % i == 0:
            li.append(i)
            n = n // i

    if n > 2:
        li.append(n)

    return li


if __name__ == "__main__":
    n1 = 36
    n2 = 0

    n1_factors = return_primeFactors(n1)
    n2_factors = return_primeFactors(n2)

    prime_factors2 = []
    for each in n1_factors:
        if each in n2_factors:
            n2_factors.remove(each)
        prime_factors2.append(each)

    prime_factors2 = prime_factors2 + n2_factors
    print(reduce(lambda a, b: a*b, prime_factors2))

