
# Completed - depth first search
from collections import defaultdict


class Graph:
    def __init__(self):
        self.graph = defaultdict(list)

    def add_edge(self, src, dst):
        self.graph[src].append(dst)

    def print_graph(self):
        g = self.graph
        for key, value in g.items():
            print(key, "-->", value)

    def depth_first_search(self, vertex):

        visited = []
        self.depth_first_search_util(vertex, visited)

    def depth_first_search_util(self, vertex, visited):

        print(vertex, end=' ')
        visited.append(vertex)
        for child in self.graph[vertex]:
            if child in visited:
                continue

            visited.append(child)
            self.depth_first_search_util(child, visited)


if __name__ == "__main__":
    graph = Graph()
    graph.add_edge(0, 1)
    graph.add_edge(0, 2)
    graph.add_edge(1, 0)
    graph.add_edge(2, 0)

    graph.add_edge(1, 3)
    graph.add_edge(1, 4)
    graph.add_edge(3, 1)
    graph.add_edge(4, 1)

    graph.add_edge(2, 4)
    graph.add_edge(4, 2)

    graph.add_edge(3, 4)
    graph.add_edge(3, 5)

    graph.add_edge(5, 3)
    graph.add_edge(5, 4)

    graph.add_edge(4, 5)
    graph.add_edge(5, 4)

    graph.print_graph()
    graph.depth_first_search(0)
