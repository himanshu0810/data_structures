
# Complete - Implement stack using array and Linked List


class Stack_Array:
    def __init__(self):
        self.top = -1
        self.elements = []

    def peek(self):
        if self.isEmpty():
            return
        return self.elements[self.top]

    def isEmpty(self):
        return self.top == -1

    def pop(self):
        if self.isEmpty():
            return

        self.top = self.top - 1
        return self.elements.pop()

    def push(self, element):
        self.top = self.top + 1
        self.elements.append(element)
        return element

# using linked list


class StackNode:
    def __init__(self, data):
        self.data = data
        self.next = None

class Stack_LinkedList:
    def __init__(self):
        self.root = None

    def is_empty(self):
        return self.root is None

    def push(self, data):
        node = StackNode(data)
        node.next = self.root
        self.root = node
        return node

    def pop(self):
        if self.is_empty():
            return
        data = self.root.data
        self.root = self.root.next
        return data

    def peek(self):
        if self.is_empty():
            return
        return self.root.data
