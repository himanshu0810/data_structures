
# Complete


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None
        self.prev = None


class LinkedList:
    def __init__(self):
        self.head = None
        self.middle_element = None
        self.size = 0

    def insert(self, element):
        if not self.head:
            self.head = Node(element)
            self.middle_element = self.head
            self.size = self.size + 1
            return

        node = self.head
        while node.next is not None:
            node = node.next

        self.size = self.size + 1
        nod = Node(element)
        node.next = nod
        nod.prev = node

        if self.size % 2 != 0:
            self.middle_element = self.middle_element.next

        return nod

    def find_middle(self):
        middle_element = self.middle_element.data
        print("Middle element is ", middle_element)
        return middle_element

    def print(self):
        node = self.head
        while node is not None:
            print(node.data, end=' ')
            node = node.next

    def delete_middle(self):
        if self.size < 2:
            self.head = None
            self.middle_element = None
            return

        if self.size == 2:
            self.head = self.middle_element.next
            self.middle_element = self.head
            return

        self.size = self.size - 1
        if self.size % 2 == 0:
            new_middle = self.middle_element.prev
        else:
            new_middle = self.middle_element.next

        self.middle_element.prev.next = self.middle_element.next
        self.middle_element.next.prev = self.middle_element.prev
        self.middle_element = new_middle
        return


if __name__ == "__main__":
    linked_list = LinkedList()
    linked_list.insert(10)
    linked_list.insert(5)
    linked_list.insert(3)

    linked_list.delete_middle()
    linked_list.find_middle()

    linked_list.insert(6)
    linked_list.insert(2)

    linked_list.insert(9)
    linked_list.find_middle()
    linked_list.delete_middle()
    linked_list.find_middle()

    linked_list.print()
