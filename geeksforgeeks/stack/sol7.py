
# Complete - Next greater element in an array


class Stack:
    def __init__(self):
        self.top = - 1
        self.elements = []

    def pop(self):
        if self.top == -1:
            return

        element = self.elements.pop()
        self.top = self.top - 1
        return element

    def is_empty(self):
        return self.top == -1

    def push(self, element):
        self.elements.append(element)
        self.top = self.top + 1
        return element

    def peek(self):
        if self.top == -1:
            return
        return self.elements[self.top]


def print_next_greater_elements_of_each_element(ary):
    st = Stack()
    for each in ary:
        if st.top == -1:
            st.push(each)
        elif st.peek() < each:
            while not st.is_empty() and st.peek() < each:
                print(st.pop(), "->", each)
            st.push(each)
        else:
            st.push(each)

    while not st.is_empty():
        print(st.pop(), "->", -1)


if __name__ == "__main__":
    ary = [13, 9, 8, 3]
    print_next_greater_elements_of_each_element(ary)
