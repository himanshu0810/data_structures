# Complete


class Queue:
    def __init__(self):
        self.top = -1
        self.elements = []

    def __repr__(self):
        return str(self.elements)

    def is_empty(self):
        return self.top == -1

    def enqueue(self, element):
        self.top = self.top + 1
        self.elements.append(element)

    def deque(self):
        if self.top == -1:
            return

        self.top = self.top - 1
        element = self.elements.pop(0)
        return element


class Stack:
    def __init__(self):
        self.q1 = Queue()
        self.q2 = Queue()

    def push(self, element):
        self.q2.enqueue(element)
        while not self.q1.is_empty():
            element = self.q1.deque()
            self.q2.enqueue(element)

        temp = self.q1
        self.q1 = self.q2
        self.q2 = temp
        return element

    def pop(self):
        return self.q1.deque()

    def size(self):
        return self.q1.top + 1


if __name__ == "__main__":
    st = Stack()
    ary = [1, 2, 5, 6, 3]

    for each in ary:
        st.push(each)

    print(st.pop())
    print(st.size())
