
# Complete - Evaluating postfix expression


class StackArray:
    def __init__(self):
        self.top = -1
        self.elements = []

    def peek(self):
        if self.is_empty():
            return
        return self.elements[self.top]

    def is_empty(self):
        return self.top == -1

    def pop(self):
        if self.is_empty():
            return

        self.top = self.top - 1
        return self.elements.pop()

    def push(self, element):
        self.top = self.top + 1
        self.elements.append(element)
        return element

    def evaluate_postfix_expression(self, exp):
        for i in exp:
            if i.isdigit():
                self.push(i)
            else:
                element = self.pop()
                element1 = self.pop()
                self.push(str(eval(str(element1 + i + element))))
        return int(self.pop())

    def evaluate_postfix_expression_with_spaces(self, exp):
        for i in exp.split():
            if i.isdigit():
                self.push(i)
            else:
                element = self.pop()
                element1 = self.pop()
                self.push(str(eval(str(element1 + i + element))))
        return float(self.pop())


if __name__ == "__main__":
    exp = "231*+9-"
    stack = StackArray()
    result = stack.evaluate_postfix_expression(exp)
    print("Postfix result is ", result)

    stack = StackArray()
    exp = "100 200 + 2 / 5 * 7 +"
    result = stack.evaluate_postfix_expression_with_spaces(exp)
    print("Postfix result is ", result)
