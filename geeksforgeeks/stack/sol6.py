
# Complete - Check for balanced paranthesis


class StackArray:
    def __init__(self):
        self.top = -1
        self.elements = []

    def peek(self):
        if self.is_empty():
            return
        return self.elements[self.top]

    def is_empty(self):
        return self.top == -1

    def pop(self):
        if self.is_empty():
            return

        self.top = self.top - 1
        return self.elements.pop()

    def push(self, element):
        self.top = self.top + 1
        self.elements.append(element)
        return element

    def check_for_balanced_paranthesis(self, exp):
        for ch in exp:
            if ch in ["(", "{", "["]:
                self.push(ch)
            else:
                elem = self.pop()
                if (ch == ")" and elem != "(") or (ch == "]" and elem != "[") or \
                    (ch == "}" and elem != "{"):
                    return False
        return self.is_empty()


if __name__ == "__main__":
    exp = "[()]"

    stack = StackArray()

    result = stack.check_for_balanced_paranthesis(exp)
    print("Reversed string is ", result)
