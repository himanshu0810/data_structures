
# Complete - Sort a stack using recursion


class Stack:
    def __init__(self):
        self.top = -1
        self.elements = []

    def push(self, element):
        self.top = self.top + 1
        self.elements.append(element)

    def is_empty(self):
        return self.top == -1

    def pop(self):
        if self.top == -1:
            return

        element = self.elements.pop()
        self.top = self.top - 1
        return element

    def return_top(self):
        if self.top == -1:
            return
        return self.elements[self.top]


def insert_stack(st, element):
    if st.is_empty() or st.return_top() < element:
        st.push(element)
    else:
        temp = st.pop()
        insert_stack(st, element)
        st.push(temp)


def sort_stack(st):
    if not st.is_empty():
        element = st.pop()
        sort_stack(st)
        insert_stack(st, element)


if __name__ == "__main__":
    ary = [30, -5, 18, 14, -3]
    st = Stack()

    for each in ary:
        st.push(each)

    sort_stack(st)
    print(st.elements)
