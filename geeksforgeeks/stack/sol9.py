
# Complete - Reverse a string using stack


class StackArray:
    def __init__(self):
        self.top = -1
        self.elements = []

    def peek(self):
        if self.is_empty():
            return
        return self.elements[self.top]

    def is_empty(self):
        return self.top == -1

    def pop(self):
        if self.is_empty():
            return

        self.top = self.top - 1
        return self.elements.pop()

    def push(self, element):
        self.top = self.top + 1
        self.elements.append(element)
        return element

    def insert_at_correct_position(self, element):
        if self.top == -1 or self.peek() < element:
            self.push(element)
            return

        item = self.pop()
        self.insert_at_correct_position(element)
        self.push(item)

    def return_sorted_stack(self):
        if self.top == -1:
            return
        element = self.pop()
        self.return_sorted_stack()
        self.insert_at_correct_position(element)


if __name__ == "__main__":
    stack = StackArray()
    stack.push(2)
    stack.push(5)
    stack.push(1)
    stack.push(7)
    stack.push(8)

    stack.return_sorted_stack()
    print("Sorted stack is ", stack.elements)
