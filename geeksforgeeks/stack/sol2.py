
# Complete


class StackArray:
    def __init__(self):
        self.size = -1
        self.elements = []
        self.precedence = {"+": 1, "-": 1, "*": 2, "/": 2, "^": 3}

    def is_empty(self):
        return self.size == -1

    def peek(self):
        return self.elements[self.size]

    def push(self, element):
        self.elements.append(element)
        self.size = self.size + 1
        return element

    def pop(self):
        if self.is_empty():
            return

        element = self.elements.pop()
        self.size = self.size - 1
        return element

    def is_operand(self, element):
        return 65 <= ord(element) <= 90 or 97 <= ord(element) <= 122

    def not_greater(self, i):
        try:
            a = self.precedence[i]
            b = self.precedence[self.peek()]
            return True if a <= b else False
        except KeyError:
            return False

    def infix_to_postfix(self, exp):
        output = []
        for i in exp:
            if self.is_operand(i):
                output.append(i)

            elif i == "(":
                self.push(i)

            elif i == ")":
                while (not self.is_empty()) and self.peek() != "(":
                    a = self.pop()
                    output.append(a)

                if (not self.is_empty()) and self.peek() != '(':
                    return -1
                else:
                    self.pop()
            else:
                while (not self.is_empty()) and self.not_greater(i):
                    element = self.pop()
                    output.append(element)
                self.push(i)

        while not self.is_empty():
            output.append(self.pop())

        return output


if __name__ == "__main__":
    exp = "a+b*(c^d-e)^(f+g*h)-i"
    stack = StackArray()
    output = stack.infix_to_postfix(exp)
    print("Output is ", ''.join(output))
