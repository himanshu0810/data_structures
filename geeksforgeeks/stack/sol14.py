
# Complete - Implement k stacks in a single array


class NStack:
    def __init__(self, k, array_length):
        self.k = k
        self.array_length = array_length
        self.elements = [None for _ in range(self.array_length)]
        self.top = [-1 for _ in range(self.k)]
        self.next = [-1 if i == self.array_length - 1 else i + 1 for i in range(self.array_length)]
        self.free_slot = 0

    def push(self, element, nk):
        i = self.free_slot
        if i == -1:
            raise OverflowError

        top = self.top[nk]

        self.free_slot = self.next[i]
        self.next[i] = top

        self.elements[i] = element
        self.top[nk] = i
        return

    def pop(self, nk):
        if self.top[nk] == - 1:
            return

        top = self.top[nk]

        self.top[nk] = self.next[top]

        self.next[top] = self.free_slot

        self.free_slot = top
        element = self.elements[top]
        self.elements[top] = None
        return element

    def print_elements(self):
        print(self.elements)
        print(self.top)
        print(self.next)


if __name__ == "__main__":
    k = 3
    array_length = 8

    ks = NStack(k, array_length)
    ks.push(15, 2)
    ks.push(45, 1)

    ks.push(17, 1)
    ks.push(49, 0)
    ks.push(39, 0)

    ks.push(11, 0)
    ks.push(9, 2)
    ks.push(7, 1)

    ks.print_elements()
    ks.pop(2)
    ks.pop(2)
    ks.pop(1)
