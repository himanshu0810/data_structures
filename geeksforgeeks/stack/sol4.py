
# Complete - Reverse a string using stack


class StackArray:
    def __init__(self):
        self.top = -1
        self.elements = []

    def peek(self):
        if self.is_empty():
            return
        return self.elements[self.top]

    def is_empty(self):
        return self.top == -1

    def pop(self):
        if self.is_empty():
            return

        self.top = self.top - 1
        return self.elements.pop()

    def push(self, element):
        self.top = self.top + 1
        self.elements.append(element)
        return element

    def reverse_string(self, string):
        for ch in string:
            self.push(ch)

        reversed_str = ""
        while self.is_empty() is not True:
            reversed_str = reversed_str + self.pop()
        return reversed_str


if __name__ == "__main__":
    exp = "231*+9-"
    print(exp[::-1])
    stack = StackArray()

    result = stack.reverse_string(exp)
    print("Reversed string is ", result)
