
# Complete - Design and Implement Special Stack Data Structure


class Stack:
    def __init__(self, st):
        self.top = -1
        self.elements = []
        self.st = st

    def peek(self):
        if self.is_empty():
            return
        return self.elements[self.top]

    def is_empty(self):
        return self.top == -1

    def pop(self):
        if self.is_empty():
            return
        self.top = self.top - 1
        return self.elements.pop()

    def mod_pop(self):
        if self.is_empty():
            return

        self.st.pop()
        self.top = self.top - 1
        return self.elements.pop()

    def push(self, element):
        self.top = self.top + 1
        self.elements.append(element)
        return element

    def mod_push(self, element):
        if self.st.is_empty():
            self.st.push(element)
        else:
            min_element = self.st.peek()
            if min_element > element:
                min_element = element

            self.st.push(min_element)

        self.top = self.top + 1
        self.elements.append(element)
        return element


if __name__ == "__main__":
    special_stack = Stack(None)
    stack = Stack(special_stack)
    elements = [4, 5, 2, 25, 10, 1]
    for each in elements:
        stack.mod_push(each)

    print(stack.elements)
    print(special_stack.elements)
    stack.mod_pop()
    print(stack.elements)
    print(special_stack.elements)
    stack.mod_pop()
    print(stack.elements)
    print(special_stack.elements)
    stack.mod_pop()
