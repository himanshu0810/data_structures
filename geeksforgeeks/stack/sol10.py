
# Complete - Stock Span Algorithm


class Stack:
    def __init__(self):
        self.top = -1
        self.elements = []

    def peek(self):
        if self.is_empty():
            return
        return self.elements[self.top]

    def is_empty(self):
        return self.top == -1

    def pop(self):
        if self.is_empty():
            return

        self.top = self.top - 1
        return self.elements.pop()

    def push(self, element):
        self.top = self.top + 1
        self.elements.append(element)
        return element

    def return_stock_span_stack(self, elements):
        output = []
        for idx, element in enumerate(elements):
            if self.is_empty() or element < elements[self.peek()]:
                self.push(idx)
                output.append(1)
            elif elements[self.peek()] < element:
                prev_element = None
                while (not self.is_empty()) and elements[self.peek()] < element:
                    prev_element = self.pop()
                prev_idx = elements.index(elements[prev_element])
                output.append(idx - prev_idx + 1)
        return output


if __name__ == "__main__":
    stack = Stack()
    input_elements = [100, 80, 60, 70, 60, 75, 85]
    output_elements = stack.return_stock_span_stack(input_elements)
    print("Output is ", output_elements)
