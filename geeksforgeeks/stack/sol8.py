
# Complete - Reverse stack using recursion


class StackArray:
    def __init__(self):
        self.top = -1
        self.elements = []

    def peek(self):
        if self.is_empty():
            return
        return self.elements[self.top]

    def is_empty(self):
        if self.top == -1:
            return True
        return

    def pop(self):
        if self.is_empty():
            return

        self.top = self.top - 1
        return self.elements.pop()

    def push(self, element):
        self.top = self.top + 1
        self.elements.append(element)
        return element

    def insert_at_bottom(self, element):
        if self.top == -1:
            self.push(element)
        else:
            prev_elem = self.pop()
            self.insert_at_bottom(element)
            self.push(prev_elem)

    def reverse_stack_using_recursion(self):
        if self.top == -1:
            return
        element = self.pop()
        self.reverse_stack_using_recursion()
        self.insert_at_bottom(element)


if __name__ == "__main__":
    stack = StackArray()
    stack.push(2)
    stack.push(5)
    stack.push(1)
    stack.push(7)
    stack.push(8)
    print(stack.elements)
    stack.reverse_stack_using_recursion()
    print("Reversed stack is ", stack.elements)
