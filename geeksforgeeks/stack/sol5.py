
# Complete - Implementing two stacks in an array


class StackArray:
    def __init__(self):
        self.range = 6
        self.top_1 = -1
        self.top_2 = self.range
        self.elements = [None for _ in range(self.range)]
        print(self.elements)

    def push1(self, element):
        if self.top_1 + 1 == self.top_2:
            self.resize_and_move()
        self.elements[self.top_1 + 1] = element
        self.top_1 = self.top_1 + 1
        return element

    def push2(self, element):
        if self.top_1 + 1 == self.top_2:
            self.resize_and_move()
        self.elements[self.top_2 - 1] = element
        self.top_2 = self.top_2 - 1
        return element

    def resize_and_move(self):
        new_range = self.range * 2
        new_element_stack = [None for _ in range(new_range)]
        new_element_stack[0:self.top_1+1] = self.elements[0:self.top_1 + 1]
        new_top_2 = new_range - (self.range - self.top_2)
        new_element_stack[new_top_2:new_range] = self.elements[self.top_2:self.range]
        self.elements = new_element_stack
        self.top_2 = new_range - (self.range - self.top_2)
        self.range = new_range
        return

    def pop1(self):
        if self.top_1 == -1:
            return
        element = self.elements[self.top_1]
        self.elements[self.top_1] = None
        self.top_1 = self.top_1 - 1
        return element

    def pop2(self):
        if self.top_2 - 1 == self.top_1 or self.top_2 + 1 >= self.range:
            return
        element = self.elements[self.top_2]
        self.elements[self.top_2] = None
        self.top_2 = self.top_2 + 1
        return element


if __name__ == "__main__":
    stack = StackArray()

    stack.push1(5)
    stack.push1(10)
    stack.push1(15)
    stack.push1(11)

    stack.push1(5)
    stack.push1(10)
    stack.push1(15)
    stack.push1(11)

    stack.push2(7)

    stack.push2(17)
    stack.push2(29)

    stack.push2(7)
    stack.push2(17)
    stack.push2(29)

    print("Full array is ", stack.elements)
