
# Reverse linked list in k pairs


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

    def __repr__(self):
        return str(self.data)



class LinkedList:
    def __init__(self):
        self.head = None

    def insert(self, data):
        new_node = Node(data)
        if not self.head:
            self.head = new_node
            return new_node

        node = self.head
        while node.next is not None:
            node = node.next

        node.next = new_node
        return new_node

    def reverse_linked_list(self, node, current, k):
        if current == k-1:
            return None, None

        if node is None:
            return None, None

        head, last_node = self.reverse_linked_list(node.next, current+1, k)
        if node.next is None or current == k-2:
            return node, node.next

        node.next.next = node
        node.next = last_node
        return head, last_node

    def reverse_linked_list_in_k_pairs(self, k):
        node = self.head
        new_head = None
        reverse = True
        while node is not None:
            if reverse:
                new_head_1, last_node = self.reverse_linked_list(node, 0, k)
                if new_head is None:
                    new_head = new_head_1
                    self.head = new_head_1
                node = last_node
            else:
                k1 = 0
                while k1 < k:
                    node = node.next
            reverse = not reverse




    def print(self):
        node = self.head
        while node is not None:
            print(node.data, end=' ')
            node = node.next



if __name__ == "__main__":
    linked_list = LinkedList()
    linked_list.insert(10)
    linked_list.insert(3)
    linked_list.insert(1)
    linked_list.insert(5)
    linked_list.insert(6)
    linked_list.insert(7)

    linked_list.print()
    print()
    linked_list.reverse_linked_list_in_k_pairs(3)
    print()
    linked_list.print()
