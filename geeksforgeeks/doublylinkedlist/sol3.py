
# Complete - Delete the node from doubly linked list in three scenarios
# 1. From the front
# 2. From the middle
# 3. From the end


class Node:
    def __init__(self, data):
        self.data = data
        self.previous = None
        self.next = None


class DoubleLinkedList:
    def __init__(self):
        self.head = None

    def append(self, data):
        if not self.head:
            self.head = Node(data)
            return
        node = self.head
        while node.next:
            node = node.next
        new_node = Node(data)
        new_node.previous = node
        new_node.next = node.next
        node.next = new_node
        return new_node

    def print(self):
        if not self.head:
            return
        node = self.head
        while node:
            print(node.data, end=' ')
            node = node.next
        print()
        return

    def delete(self, data):
        if not self.head:
            return
        elif self.head.data == data:
            self.head = self.head.next
            self.head.previous = None
            return

        node = self.head
        while node and node.data != data:
            node = node.next

        node.previous.next = node.next
        if node.next is not None:
            node.next.previous = node.previous


if __name__ == "__main__":
    doubly_linked_list = DoubleLinkedList()
    doubly_linked_list.append(10)
    doubly_linked_list.append(4)
    doubly_linked_list.append(3)
    doubly_linked_list.append(5)
    doubly_linked_list.append(2)
    doubly_linked_list.print()
    doubly_linked_list.delete(3)
    doubly_linked_list.print()
