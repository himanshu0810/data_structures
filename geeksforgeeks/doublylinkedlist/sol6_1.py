
class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.data)


class BinaryTree:
    def __init__(self):
        self.root = None

    def inorder(self, root, bi):
        if root:
            self.inorder(root.left, bi)
            bi.append(root.data)
            self.inorder(root.right, bi)
        return bi

    def preorder(self, root, bi):
        if root:
            bi.append(root.data)
            self.preorder(root.left, bi)
            self.preorder(root.right, bi)
        return bi


class CDLinkedList:
    def __init__(self):
        self.head = None

    def print_list(self):
        temp = self.head
        while True:
            print(temp.data, end=' ')
            temp = temp.right
            if temp == self.head:
                break


def merge_left_and_node_circular_linked_list(left_list, node):
    left_list_last = left_list.left
    left_list.left = node
    node.right = left_list
    left_list_last.right = node
    node.left = left_list_last
    node = left_list
    return node


def merge_left_right_circular_linked_list(node, right_list):
    right_list = right_list
    right_list_last = right_list.left

    node_last = node.left
    node = node

    node.left = right_list_last
    right_list_last.right = node

    node_last.right = right_list
    right_list.left = node_last
    return node


def convert_tree_into_list_util(node):
    if node.left is None and node.right is None:
        node.left = node
        node.right = node
        return node

    right_node = node.right
    if node.left:
        left_list = convert_tree_into_list_util(node.left)
        node = merge_left_and_node_circular_linked_list(left_list, node)

    if right_node:
        right_list = convert_tree_into_list_util(right_node)
        node = merge_left_right_circular_linked_list(node, right_list)

    return node


def convert_tree_into_list(b1):
    llist = CDLinkedList()
    llist.head = convert_tree_into_list_util(b1.root)
    return llist


if __name__ == "__main__":
    b1 = BinaryTree()
    b1.root = Node(100)
    b1.root.left = Node(50)
    b1.root.right = Node(300)
    b1.root.right.left = Node(200)
    b1.root.left.left = Node(20)
    b1.root.left.right = Node(70)
    print(b1.inorder(b1.root, []))
    print(b1.preorder(b1.root, []))

    convert_tree_into_list(b1)
