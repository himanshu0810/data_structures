
# Complete - Reverse the doubly linked list


class Node:
    def __init__(self, data):
        self.data = data
        self.previous = None
        self.next = None


class DoubleLinkedList:
    def __init__(self):
        self.head = None

    def append(self, data):
        if not self.head:
            self.head = Node(data)
            return
        node = self.head
        while node.next:
            node = node.next
        new_node = Node(data)
        new_node.previous = node
        new_node.next = node.next
        node.next = new_node
        return new_node

    def print(self):
        if not self.head:
            return
        node = self.head
        while node:
            print(node.data, end=' ')
            node = node.next
        print()
        return

    def reverse_linked_list(self):
        if not self.head:
            return
        node = self.head
        while node.next:
            node.previous, node.next = node.next, node.previous
            node = node.previous
        node.previous, node.next = node.next, node.previous
        self.head = node
        return self.head


if __name__ == "__main__":
    doubly_linked_list = DoubleLinkedList()
    doubly_linked_list.append(10)
    doubly_linked_list.append(4)
    doubly_linked_list.append(3)
    doubly_linked_list.append(5)
    doubly_linked_list.append(2)
    doubly_linked_list.print()
    doubly_linked_list.reverse_linked_list()
    doubly_linked_list.print()
