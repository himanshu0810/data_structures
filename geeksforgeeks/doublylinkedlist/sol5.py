

class Node:
    def __init__(self, data):
        self.data = data
        self.prev = None
        self.next = None

    def __repr__(self):
        return str(self.data)


class LinkedList:
    def __init__(self):
        self.head = None

    def __repr__(self):
        head = self.head
        c = ""
        while head.next is not None:
            c = str(head.data) + " "
            head = head.next
        return c

    def print_list(self):
        head = self.head
        while head is not None:
            print(head.data, end=' ')
            head = head.next
        print()

    def partition(self, low, high):
        l = low
        pivot = high.data

        h = high.prev

        while l != h:
            if h.data > pivot:
                h = h.prev
            elif l.data < pivot:
                l = l.next
            else:
                l.data, h.data = h.data, l.data

        if l.data < pivot:
            l = l.next
            l.data, high.data = high.data, l.data
        else:
            l.data, high.data = high.data, l.data
        return l

    def quick_sort(self, low, high):
        if low is not None and high is not None and low != high and high.next is not low:
            p = self.partition(low, high)
            self.quick_sort(low, p.prev)
            self.quick_sort(p.next, high)

    def sort(self):
        head = self.head
        while head.next is not None:
            head = head.next

        self.quick_sort(self.head, head)


if __name__ == "__main__":
    linked_list = LinkedList()
    linked_list.head = Node(31)

    node_1 = Node(1)
    node_1.prev = linked_list.head
    linked_list.head.next = node_1

    node_2 = Node(14)
    node_1.next = node_2
    node_2.prev = node_1

    node_3 = Node(53)
    node_2.next = node_3
    node_3.prev = node_2

    node_4 = Node(10)
    node_3.next = node_4
    node_4.prev = node_3

    linked_list.print_list()

    linked_list.sort()

    print("Sorted linked list is ")
    linked_list.print_list()
