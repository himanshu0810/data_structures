
# Complete - Insertion in doubly linked list with four scenarios:-
# 1. At the front
# 2. At the end
# 3. After some node
# 4. Before some node


class Node:
    def __init__(self, data):
        self.data = data
        self.previous = None
        self.next = None


class DoubleLinkedList:
    def __init__(self):
        self.head = None

    def push(self, data):
        if not self.head:
            self.head = Node(data)
            return
        node = Node(data)
        node.next = self.head
        self.head.previous = node
        self.head = node
        return

    def append(self, data, key=None):
        if key is not None:
            node = self.head
            while node.data != key:
                node = node.next
        else:
            if not self.head:
                self.head = Node(data)
                return
            node = self.head
            while node.next:
                node = node.next
        new_node = Node(data)
        new_node.previous = node
        new_node.next = node.next
        node.next = new_node
        return new_node

    def before(self, data, key):
        new_node = Node(data)
        node = self.head
        while node.data != key:
            node = node.next
        if node.previous:
            node.previous.next = new_node

        new_node.next = node
        new_node.previous = node.previous
        node.previous = new_node
        return new_node

    def print(self):
        if not self.head:
            return
        node = self.head
        while node:
            print(node.data, end=' ')
            node = node.next
        return


if __name__ == "__main__":
    doubly_linked_list = DoubleLinkedList()
    doubly_linked_list.push(3)
    doubly_linked_list.push(4)
    doubly_linked_list.push(1)
    doubly_linked_list.append(1)
    doubly_linked_list.append(1, key=3)
    doubly_linked_list.append(1, key=4)

    doubly_linked_list.before(14, key=4)
    doubly_linked_list.print()
