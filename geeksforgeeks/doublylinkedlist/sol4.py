

class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.data)


def concatenate(left_list, right_list):
    if right_list is None:
        return left_list

    if left_list is None:
        return right_list

    left_last = left_list.left
    right_last = right_list.left

    left_last.right = right_list
    right_list.left = left_last

    right_last.right = left_list
    left_list.left = right_last

    return left_list


def make_it_to_list(node):
    if node is None:
        return

    left = make_it_to_list(node.left)
    right = make_it_to_list(node.right)

    node.left = node.right = node

    return concatenate(concatenate(left, node), right)


class BinaryTree:
    def __init__(self):
        self.head = None

    def print_tree(self, node):
        if node:
            self.print_tree(node.left)
            print(node.data, end=' ')
            self.print_tree(node.right)


if __name__ == "__main__":
    bt = BinaryTree()
    root = Node(10)
    root.left = Node(5)
    root.left.left = Node(1)
    root.left.right = Node(8)

    root.right = Node(15)
    root.right.left = Node(13)

    bt.print_tree(root)
    print()

    t = make_it_to_list(root)

    head = t
    while True:
        print(head.data, end=' ')
        head = head.left
        if t == head:
            break
