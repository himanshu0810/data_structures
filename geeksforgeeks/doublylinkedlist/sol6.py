

class Node:
    def __init__(self, data):
        self.data = data
        self.prev = None
        self.next = None


class LinkedList:
    def __init__(self):
        self.head = None

    def print_list_rev(self):
        head = self.head
        while head.right is not None:
            head = head.right

        while head is not None:
            print(head.data, end=' ')
            head = head.left
        print()


def print_list(head):
    temp = None
    while head is not None:
        print(head.data, end=' ')
        temp = head
        head = head.next

    print(" reversing - ", end=' ')
    while temp is not None:
        print(temp.data, end=' ')
        temp = temp.prev
    print()


def return_sorted_array(head):
    temp = head
    if head is not None:
        if head.next is None:
            return head

        slow = head
        fast = head

        while fast.next is not None:
            fast = fast.next
            if fast.next is None:
                break
            fast = fast.next
            slow = slow.next

        head2 = slow.next
        head2.prev = None

        slow.next = None
        head1 = head

        left = return_sorted_array(head1)
        right = return_sorted_array(head2)
        temp = merge_double_linked_list(left, right)

    return temp


def merge_double_linked_list(head, head1):

    if head is None:
        return head1

    if head1 is None:
        return head

    if head.data < head1.data:
        temp = head
        node = merge_double_linked_list(head.right, head1)
    else:
        temp = head1
        node = merge_double_linked_list(head, head1.right)

    node.left = temp
    temp.right = node
    return temp


if __name__ == "__main__":
    linked_list1 = LinkedList()
    linked_list1.head = Node(3)

    node_1 = Node(1)
    node_1.prev = linked_list1.head
    linked_list1.head.next = node_1

    node_2 = Node(4)
    node_1.next = node_2
    node_2.prev = node_1

    node_3 = Node(5)
    node_2.next = node_3
    node_3.prev = node_2

    node_4 = Node(2)
    node_3.next = node_4
    node_4.prev = node_3

    node_5 = Node(8)
    node_4.next = node_5
    node_5.prev = node_4

    print_list(linked_list1.head)

    print("Sorted linked list is ")
    new_head = return_sorted_array(linked_list1.head)
    print_list(new_head)
