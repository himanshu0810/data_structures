import sys


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.data)


class Tree:
    def __init__(self):
        self.root = None

    def inorder(self, node):
        if node:
            self.inorder(node.left)
            print(node.data, end=' ')
            self.inorder(node.right)

    def check_bst(self):
        node = self.root
        print("Perfect bst or not ", self.check_bst_util(node, -1, sys.maxsize))

    def check_bst_util(self, node, min, max):
        if node is None:
            return True

        if not (min < node.data < max):
            return False

        if self.check_bst_util(node.left, min, node.data):
            if self.check_bst_util(node.right, node.data, max):
                return True

        return False


if __name__ == "__main__":
    tree = Tree()
    tree.root = Node(6)

    tree.root.left = Node(2)
    tree.root.right = Node(9)

    tree.root.left.left = Node(1)
    tree.root.left.right = Node(4)

    tree.inorder(tree.root)

    print()
    tree.check_bst()
