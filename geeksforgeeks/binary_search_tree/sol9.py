
# TODO


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.data)


class BinaryTree:
    def __init__(self):
        self.root = None

    def inorder(self, root):
        if root:
            self.inorder(root.left)
            print(root.data, end=' ')
            self.inorder(root.right)


def merge_sorted_binary_tree(node, node1):
    if node is None and node1 is None:
        return None
    if node is None:
        return node
    if node1 is None:
        return node1

    if node.left:
        merge_sorted_binary_tree(node.left, node1)
    if node1.left:
        merge_sorted_binary_tree(node, node1.left)

    if node.data < node1.data:
        print(node.data, end=' ')
        if node.right:
            merge_sorted_binary_tree(node.right, node1)
    else:
        print(node1.data, end=' ')
        if node1.right:
            merge_sorted_binary_tree(node, node1.right)
    return


if __name__ == "__main__":
    binary_tree = BinaryTree()
    binary_tree.root = Node(3)
    binary_tree.root.left = Node(1)
    binary_tree.root.right = Node(5)

    # binary_tree.inorder(binary_tree.root)

    binary_tree1 = BinaryTree()
    binary_tree1.root = Node(4)
    binary_tree1.root.left = Node(2)
    binary_tree1.root.right = Node(6)

    # binary_tree1.inorder(binary_tree.root)

    binary_tree2 = BinaryTree()
    binary_tree.root = merge_sorted_binary_tree(binary_tree.root, binary_tree1.root)
