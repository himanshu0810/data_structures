
# Complete - Find inorder successor of BST


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __str__(self):
        return str(self.data)


def insert_recursively(root, data):
    if root is None:
        return Node(data)

    if root.data > data:
        root.left = insert_recursively(root.left, data)
    else:
        root.right = insert_recursively(root.right, data)

    return root


def inorder(root):
    if root:
        inorder(root.left)
        print(root.data, end=' ')
        inorder(root.right)


def return_inorder_successor(data, root, successor):
    if root is None:
        return successor

    if data < root.data:
        successor = root
        return return_inorder_successor(data, root.left, successor)
    elif data > root.data:
        return return_inorder_successor(data, root.right, successor)
    else:
        node1 = root.right
        while node1 and node1.left:
            node1 = node1.left
        if node1 is not None:
            successor = node1

    return successor


if __name__ == "__main__":
    root_key = insert_recursively(None, 20)

    insert_recursively(root_key, 8)
    insert_recursively(root_key, 22)
    insert_recursively(root_key, 4)
    insert_recursively(root_key, 12)
    insert_recursively(root_key, 10)
    insert_recursively(root_key, 14)

    inorder(root_key)
    print()
    success = return_inorder_successor(10, root_key, None)
    print("successor is ", success)
