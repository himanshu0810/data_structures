
# Complete - Find the inorder predecessor and successor for a given key in BST


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BST:
    def __init__(self):
        self.root = None

    def insert_recursively_util(self, node, data):
        if node is None:
            return Node(data)

        if node.data > data:
            if node.left:
                self.insert_recursively_util(node.left, data)
            else:
                node.left = self.insert_recursively_util(node.left, data)
        else:
            if node.right:
                self.insert_recursively_util(node.right, data)
            else:
                node.right = self.insert_recursively_util(node.right, data)

        return node

    def insert_recursively(self, data):
        if not self.root:
            self.root = Node(data)
            return self.root
        self.insert_recursively_util(self.root, data)
        return

    def inorder(self, node):
        if node:
            self.inorder(node.left)
            print(node.data, end=' ')
            self.inorder(node.right)

    def return_pred_succ(self, data, root_key, pred, succ):
        if root_key is None:
            return pred, succ

        if root_key.data < data:
            pred = root_key
            return self.return_pred_succ(data, root_key.right, pred, succ)
        elif root_key.data > data:
            succ = root_key
            return self.return_pred_succ(data, root_key.left, pred, succ)
        else:
            node = root_key

            node1 = node.left
            while node1 and node1.right:
                node1 = node1.right
            if node1 is not None:
                pred = node1

            node2 = node.right
            while node2 and node2.left:
                node2 = node2.left
            if node2 is not None:
                succ = node2

        return pred, succ


if __name__ == "__main__":
    bst = BST()
    root_key = bst.insert_recursively(4)

    bst.insert_recursively(2)
    bst.insert_recursively(9)
    bst.insert_recursively(1)
    bst.insert_recursively(3)
    bst.insert_recursively(6)
    bst.insert_recursively(10)
    bst.insert_recursively(7)

    bst.inorder(root_key)

    a, b= bst.return_pred_succ(-1, root_key, None, None)
    print()
    if a is not None:
        print(a.data, end=' ')
    else:
        print(None, end=' ')
    if b is not None:
        print(b.data, end=' ')
    else:
        print(None, end=' ')
