
# Complete - Delete in a Binary Tree


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.data)


def insert_recursively(root, data):
    if root is None:
        return Node(data)

    if root.data > data:
        root.left = insert_recursively(root.left, data)
    else:
        root.right = insert_recursively(root.right, data)

    return root


def inorder(root):
    if root:
        inorder(root.left)
        print(root.data, end=' ')
        inorder(root.right)

def preorder(root):
    if root:
        print(root.data, end=' ')
        preorder(root.left)
        preorder(root.right)


def return_inorder_successor(root):
    node = root.right
    prev = None
    while node.left is not None:
        prev = node
        node = node.left

    if prev:
        prev.left = node.right
    return node


def delete_a_node(root, data):
    if root is None:
        return

    if root.data == data:
        if root.left is None and root.right is None:
            return
        elif root.left is None:
            return root.right
        elif root.right is None:
            return root.left
        else:
            node = return_inorder_successor(root)
            if node.right is not None:
                node.right = root.right
            node.left = root.left
            return node

    if root.data < data:
        root.right = delete_a_node(root.right, data)
    else:
        root.left = delete_a_node(root.left, data)

    return root

if __name__ == "__main__":
    root_key = insert_recursively(None, 4)

    insert_recursively(root_key, 2)
    insert_recursively(root_key, 9)
    insert_recursively(root_key, 1)
    insert_recursively(root_key, 3)
    insert_recursively(root_key, 6)
    insert_recursively(root_key, 10)
    insert_recursively(root_key, 7)

    print("inorder")
    inorder(root_key)
    print()

    print("preorder")
    preorder(root_key)
    print()

    root_key = delete_a_node(root_key, 4)

    print("inorder")
    inorder(root_key)
    print()

    print("preorder")
    preorder(root_key)
    print()
