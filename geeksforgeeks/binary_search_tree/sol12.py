
# Complete - Convert DLL into Binary Tree


class Node:
    def __init__(self, data):
        self.data = data
        self.prev = None
        self.next = None

    def __repr__(self):
        return str(self.data)


class CBinaryTree:
    def __init__(self):
        self.root = None

    def convert_dll_into_linked_list_1(self, dll):
        if dll.head is None:
            return
        elif dll.head.right is None:
            return dll.head

        slow = fast = dll.head
        while fast and fast.right is not None:
            fast = fast.right
            if fast:
                fast = fast.right
            slow = slow.right

        middle_head = slow
        prev_node = middle_head.left
        if prev_node:
            d1 = DLL()
            d1.head = dll.head
            prev_node.right = None
            middle_head.left = None
            middle_head.left = self.convert_dll_into_linked_list_1(d1)

        next_node = middle_head.right
        if next_node:
            d2 = DLL()
            next_node.left = None
            middle_head.right = None
            d2.head = next_node
            middle_head.right = self.convert_dll_into_linked_list_1(d2)

        return middle_head

    def inorder(self, root):
        if root:
            self.inorder(root.left)
            print(root.data, end=' ')
            self.inorder(root.right)


class DLL:
    def __init__(self):
        self.head = None
        self.count = 0

    def print(self):
        head = self.head
        if not self.head:
            return

        prev = None
        while head is not None:
            print(head.data, end=' ')
            prev = head
            head = head.next

        head = prev
        print()
        while head is not None:
            print(head.data, end=' ')
            head = head.prev

    def insert(self, element):
        head = self.head
        self.count = self.count + 1
        if head is None:
            self.head = Node(element)
            return

        while head.next is not None:
            head = head.next

        node = Node(element)
        head.next = node
        node.prev = head
        return node


if __name__ == "__main__":
    dll = DLL()
    ary = [1, 2, 3, 4, 5, 6, 7]
    for each in ary:
        dll.insert(each)

    # dll.print()

    bt = CBinaryTree()
    bt.root = bt.convert_dll_into_linked_list_1(dll)
    bt.inorder(bt.root)
    print()
    print(bt.root.data)
