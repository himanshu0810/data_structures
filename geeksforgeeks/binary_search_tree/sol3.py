
# Complete - Return minimum value in binary search tree


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __str__(self):
        return str(self.data)


def insert_recursively(root, data):
    if root is None:
        return Node(data)

    if root.data > data:
        root.left = insert_recursively(root.left, data)
    else:
        root.right = insert_recursively(root.right, data)

    return root


def insert_iteratively(root, data):
    if root is None:
        return Node(data)

    while True:
        if root.data > data:
            if root.left:
                root = root.left
            else:
                root.left = Node(data)
                break
        else:
            if root.right:
                root = root.right
            else:
                root.right = Node(data)
                break


def inorder(root):
    if root:
        inorder(root.left)
        print(root.data, end=' ')
        inorder(root.right)


def return_minimum_value(root):
    if root is None:
        return None

    if root.left:
        return return_minimum_value(root.left)

    return root


def return_minimum_value_iteratively(root):
    if root is None:
        return

    while root.left:
        root = root.left

    print("minim element is ", root.data)
    return root


if __name__ == "__main__":
    root = insert_recursively(None, 4)

    insert_recursively(root, 2)
    insert_recursively(root, 9)
    insert_recursively(root, 1)
    insert_recursively(root, 3)
    insert_recursively(root, 6)
    insert_recursively(root, 10)
    insert_recursively(root, 7)

    inorder(root)
    print()
    print("printing inorder of a tree")

    print()
    find_minimum = return_minimum_value(root)
    print("minimum value is ", find_minimum)


    print()
    find_minimum = return_minimum_value_iteratively(root)
    print("minimum value is ", find_minimum)
