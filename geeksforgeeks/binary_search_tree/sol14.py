
# Complete - Total number of possible Binary Search Trees with n keys
from pprint import pprint


def return_number_possible_bst(n):
    li = [0 for _ in range(n+1)]
    li[0] = 1
    li[1] = 1

    if n == 1:
        return li[n]
    elif n == 2:
        return li[n]
    else:
        print("for - ", n)
        for i in range(2, n+1):
            for j in range(0, i):
                x = i-j-1
                print((x, j), end=' ')
                li[i] = li[i] + li[x]*li[j]

        print()
    pprint(li)


if __name__ == "__main__":
    n = 5
    return_number_possible_bst(3)
    return_number_possible_bst(4)
    return_number_possible_bst(5)
    return_number_possible_bst(6)
