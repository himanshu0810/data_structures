
# Complete - Find Floor ceil value of binary search tree


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __str__(self):
        return str(self.data)


def insert_recursively(root, data):
    if root is None:
        return Node(data)

    if root.data > data:
        root.left = insert_recursively(root.left, data)
    else:
        root.right = insert_recursively(root.right, data)

    return root


def inorder(root):
    if root:
        inorder(root.left)
        print(root.data, end=' ')
        inorder(root.right)


def return_ceil_value(root, key):
    if root is None:
        return -1

    if root.data == key:
        return root.data

    if root.data < key:
        return return_ceil_value(root.right, key)

    val = return_ceil_value(root.left, key)
    return val if val >= key else root.data


def return_floor_ceil_value(root, key):
    if root is None:
        return -1, -1

    if root.data == key:
        return root.data, root.data

    if root.data < key:
        floor, ceil = return_floor_ceil_value(root.right, key)
    else:
        floor, ceil = return_floor_ceil_value(root.left, key)

    if floor == -1 and root.data < key:
        floor = root.data

    if ceil == -1 and root.data > key:
        ceil = root.data
    return floor, ceil


if __name__ == "__main__":
    root = insert_recursively(None, 8)

    insert_recursively(root, 4)
    insert_recursively(root, 12)
    insert_recursively(root, 2)
    insert_recursively(root, 10)
    insert_recursively(root, 6)
    insert_recursively(root, 14)

    inorder(root)
    print()
    print("printing inorder of a tree")

    for i in range(16):
        floor_value, ceil_value = return_floor_ceil_value(root, i)
        print("ceil value is of ", i, " -> ", floor_value, ceil_value)
