
# Complete - Merge two balanced binary tree

from geeksforgeeks.binary_search_tree.sol12 import CBinaryTree
from geeksforgeeks.doublylinkedlist.sol6 import merge_double_linked_list, LinkedList
from geeksforgeeks.doublylinkedlist.sol6_1 import convert_tree_into_list


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.data)


class BinaryTree:
    def __init__(self):
        self.root = None

    def inorder(self, root, bi):
        if root:
            self.inorder(root.left, bi)
            bi.append(root.data)
            self.inorder(root.right, bi)
        return bi

    def preorder(self, root, bi):
        if root:
            bi.append(root.data)
            self.preorder(root.left, bi)
            self.preorder(root.right, bi)
        return bi


def merge_two_arrays(b1, b2):

    len_b1 = len(b1)
    len_b2 = len(b2)

    s_b1 = s_b2 = 0
    merged_array = []
    while s_b1 < len_b1 and s_b2 < len_b2:
        if b1[s_b1] < b2[s_b2]:
            merged_array.append(b1[s_b1])
            s_b1 = s_b1 + 1
        else:
            merged_array.append(b2[s_b2])
            s_b2 = s_b2 + 1

    while s_b1 < len_b1:
        merged_array.append(b1[s_b1])
        s_b1 = s_b1 + 1

    while s_b2 < len_b2:
        merged_array.append(b2[s_b2])
        s_b2 = s_b2 + 1

    return merged_array


def return_merge_two_balanced_binary_tree(b1, b2):
    # Inorder list of first binary tree
    b1_inorder = b1.inorder(b1.root, [])

    # Inorder list of second binary tree
    b2_inorder = b2.inorder(b2.root, [])

    # Merged array of two inorder lists
    merged_array = merge_two_arrays(b1_inorder, b2_inorder)

    # Convert array into binary tree
    b3 = convert_list_to_binary_tree(merged_array)
    return b3


def convert_list_to_binary_tree_util(ar):
    if not ar:
        return
    elif len(ar) == 1:
        return Node(ar[0])
    elif len(ar) == 2:
        node_1 = Node(ar[0])
        node_2 = Node(ar[1])
        node_2.left = node_1
        return node_2

    middle_element = (len(ar) - 1) // 2
    node = Node(ar[middle_element])
    node.left = convert_list_to_binary_tree_util(ar[:middle_element])
    node.right = convert_list_to_binary_tree_util(ar[middle_element+1:])
    return node


def convert_list_to_binary_tree(ar):
    b3 = BinaryTree()
    b3.root = convert_list_to_binary_tree_util(ar)
    return b3


def return_merge_two_balanced_binary_tree_1(b1, b2):
    l1 = convert_tree_into_list(b1)

    l2 = convert_tree_into_list(b2)

    l1 = l1.head
    l1_last = l1.left
    l1.left = None
    l1_last.right = None

    l2 = l2.head
    l2_last = l2.left
    l2.left = None
    l2_last.right = None

    l3 = LinkedList()
    l3.head = merge_double_linked_list(l1, l2)
    l3.print_list_rev()

    bt = CBinaryTree()
    bt.root = bt.convert_dll_into_linked_list_1(l3)
    return bt


if __name__ == "__main__":
    b1 = BinaryTree()
    b1.root = Node(100)
    b1.root.left = Node(50)
    b1.root.right = Node(300)
    b1.root.left.left = Node(20)
    b1.root.left.right = Node(70)

    b2 = BinaryTree()
    b2.root = Node(80)
    b2.root.left = Node(40)
    b2.root.right = Node(120)

    b3 = return_merge_two_balanced_binary_tree(b1, b2)
    print(b3.preorder(b3.root, []))

    bt = return_merge_two_balanced_binary_tree_1(b1, b2)
    bt.inorder(bt.root)
    print()
    print(bt.root.data)
