
# Complete - Find least common ancestor of two nodes


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __str__(self):
        return str(self.data)


def insert_recursively(root, data):
    if root is None:
        return Node(data)

    if root.data > data:
        root.left = insert_recursively(root.left, data)
    else:
        root.right = insert_recursively(root.right, data)

    return root


def inorder(root):
    if root:
        inorder(root.left)
        print(root.data, end=' ')
        inorder(root.right)


def return_least_common_ancestor_mode_iteratively(root, left, right):
    if root is None:
        return

    while root is not None:
        if left < root.data and right < root.data:
            root = root.left

        elif right > root.data and left > root.data:
            root = root.right
        else:
            break

    return root

def return_least_common_ancestor_mode_recursively(root, left, right):
    if root is None:
        return

    if left < root.data and right < root.data:
        return return_least_common_ancestor_mode_recursively(root.left, left, right)
    elif right > root.data and left > root.data:
        return return_least_common_ancestor_mode_recursively(root.right, left, right)

    return root

if __name__ == "__main__":
    root = insert_recursively(None, 20)

    insert_recursively(root, 8)
    insert_recursively(root, 22)
    insert_recursively(root, 4)
    insert_recursively(root, 12)
    insert_recursively(root, 10)
    insert_recursively(root, 14)

    a = inorder(root)
    print()
    print("printing inorder of a tree")

    lca = return_least_common_ancestor_mode_iteratively(root, 10, 22)
    print()
    print("least common ancestor is ", lca)

    lca = return_least_common_ancestor_mode_recursively(root, 10, 14)
    print()
    print("least common ancestor is ", lca)
