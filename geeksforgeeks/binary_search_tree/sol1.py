
# Complete - Insert and Search in Binary Search Tree


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


def insert_recursively(root, data):
    if root is None:
        return Node(data)

    if root.data > data:
        root.left = insert_recursively(root.left, data)
    else:
        root.right = insert_recursively(root.right, data)

    return root


def insert_iteratively(root, data):
    if root is None:
        return Node(data)

    while True:
        if root.data > data:
            if root.left:
                root = root.left
            else:
                root.left = Node(data)
                break
        else:
            if root.right:
                root = root.right
            else:
                root.right = Node(data)
                break


def search_recursively(root, data):
    if root is None or root.data == data:
        return root

    if root.data > data:
        return search_recursively(root.left, data)

    return search_recursively(root.right, data)


def search_iteratively(root, data):
    if root is None:
        return

    while True:
        if root.data == data:
            return True

        if root.data > data:
            if root.left:
                root = root.left
            else:
                return
        else:
            if root.right:
                root = root.right
            else:
                return


def inorder(root):
    if root:
        inorder(root.left)
        print(root.data, end=' ')
        inorder(root.right)


if __name__ == "__main__":
    root_key = insert_recursively(None, 4)

    insert_recursively(root_key, 2)
    insert_recursively(root_key, 9)
    insert_recursively(root_key, 1)
    insert_recursively(root_key, 3)
    insert_recursively(root_key, 6)
    insert_recursively(root_key, 10)
    insert_recursively(root_key, 7)

    inorder(root_key)
    print()
    print("printing inorder of a tree")

    print()
    search_key = search_recursively(root_key, -2)
    print("searching a key in recursive method ", search_key)

    print()
    search_key = search_recursively(root_key, 3)
    print("searching a key in an iterative method ", search_key.data)
