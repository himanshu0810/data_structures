
# Complete - Two nodes of a BST are swapped, correct the BST
import sys


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.data)


def insert_recursively(root, data):
    if root is None:
        return Node(data)

    if root.data > data:
        root.left = insert_recursively(root.left, data)
    else:
        root.right = insert_recursively(root.right, data)

    return root


def inorder(root):
    if root:
        inorder(root.left)
        print(root.data, end=' ')
        inorder(root.right)


def return_wrong_nodes_util(node, min, max, node1, node2):
    if node1 and node2:
        return node1, node2

    if node.left:
        node1, node2 = return_wrong_nodes_util(node.left, min, node.data, node1, node2)

    if not (min < node.data < max):
        if not node1:
            node1 = node
        else:
            node2 = node
        return node1, node2

    if node.right:
        node1, node2 = return_wrong_nodes_util(node.right, node.data, max, node1, node2)

    return node1, node2


def fix_binary_search_tree(root):
    node1 = node2 = None
    node1, node2 = return_wrong_nodes_util(root, 0, sys.maxsize, node1, node2)
    node1.data, node2.data = node2.data, node1.data


if __name__ == "__main__":
    root = insert_recursively(None, 10)
    root.left = Node(5)
    root.right = Node(8)
    root.left.left = Node(2)
    root.left.right = Node(20)
    # root.right.right = Node(12)
    # root.right.left = Node(7)

    inorder(root)
    print()
    fix_binary_search_tree(root)
    inorder(root)
