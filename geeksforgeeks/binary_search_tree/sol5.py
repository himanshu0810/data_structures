
# Complete - Check whether a tree is bst or not


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __str__(self):
        return str(self.data)


def insert_recursively(root, data):
    if root is None:
        return Node(data)

    if root.data > data:
        root.left = insert_recursively(root.left, data)
    else:
        root.right = insert_recursively(root.right, data)

    return root


def inorder_with_array(root, a):
    if root:
        inorder_with_array(root.left, a)
        a.append(root.data)
        inorder_with_array(root.right, a)
    return a


def inorder_with_global_storage_helper(root):
    global prev

    if root is None:
        return True

    if inorder_with_global_storage_helper(root.left) is False:
        return False

    if prev is not None and prev.data > root.data:
        return False

    prev = root
    return inorder_with_global_storage_helper(root.right)


def inorder_with_global_storage(root):
    global prev
    prev = None
    return inorder_with_global_storage_helper(root)


def check_sorted_array(a):
    prev = None
    for each in a:
        if prev is not None and prev > each:
            return False
        prev = each

    return True


def is_tree_bst_util(root, left, right):
    if root is None:
        return True

    if left is not None and root.data < left.data:
        return False

    if right is not None and root.data > right.data:
        return False

    return is_tree_bst_util(root.left, left, root) and is_tree_bst_util(root.right, root, right)


def is_tree_bst(root):
    return is_tree_bst_util(root, None, None)


if __name__ == "__main__":
    root = insert_recursively(None, 4)

    insert_recursively(root, 2)
    insert_recursively(root, 9)
    insert_recursively(root, 1)
    insert_recursively(root, 3)
    insert_recursively(root, 6)
    insert_recursively(root, 10)
    insert_recursively(root, 7)

    a = inorder_with_array(root, [])
    flag = check_sorted_array(a)
    print()
    print("printing inorder of a tree", a)
    print("tree is bst or not ", flag)

    print()
    flag = inorder_with_global_storage(root)
    print('checking tree bst or not with global pointer', flag)

    print()
    flag = is_tree_bst(root)
    print("checking tree whether it is bst or not ", flag)
