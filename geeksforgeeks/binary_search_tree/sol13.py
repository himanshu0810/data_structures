
# Complete - Find a pair with given sum in balanced bst


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __str__(self):
        return str(self.data)


def insert_recursively(root, data):
    if root is None:
        return Node(data)

    if root.data > data:
        root.left = insert_recursively(root.left, data)
    else:
        root.right = insert_recursively(root.right, data)

    return root


def inorder(root, order):
    if root:
        inorder(root.left, order)
        order.append(root.data)
        inorder(root.right, order)
    return order


def rev_inorder(root, order):
    if root:
        rev_inorder(root.right, order)
        order.append(root.data)
        rev_inorder(root.left, order)
    return order


def find_a_pair_with_given_sum(order, rev_order, sum):
    l = r = 0
    while l < len(order) and r < len(order):
        left, right = order[l], rev_order[r]
        if left + right == sum:
            return left, right
        elif left + right < sum:
            l = l + 1
        else:
            r = r + 1
    return False


if __name__ == "__main__":
    root = insert_recursively(None, 8)

    insert_recursively(root, 4)
    insert_recursively(root, 12)
    insert_recursively(root, 2)
    insert_recursively(root, 10)
    insert_recursively(root, 6)
    insert_recursively(root, 14)

    order = []
    order = inorder(root, order)
    rev_order = []
    rev_order = rev_inorder(root, rev_order)

    sum = 8
    pair = find_a_pair_with_given_sum(order, rev_order, sum)
    print("Pair is ", pair)
