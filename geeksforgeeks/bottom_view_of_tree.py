
# Completed - Bottom view of tree


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.data)


class Tree:
    def __init__(self):
        self.root = None

    def print(self, node):
        if node:
            self.print(node.left)
            # print(node.data, end=' ')
            self.print(node.right)

    def print_bottom_view_of_tree(self):
        node = tree.root
        hash = self.print_bottom_view_of_tree_util(node, 0, {}, 0)

        for each in hash.values():
            print(each[0], end=' ')

        print()

    def print_bottom_view_of_tree_util(self, node, level, hash, depth):
        if node is None:
            return

        if level in hash:
            node_data, node_depth = hash[level]
            if node_depth <= depth:
                hash[level] = [node.data, depth]
        else:
            hash[level] = [node.data, depth]

        if node.left:
            self.print_bottom_view_of_tree_util(node.left, level-1, hash, depth+1)

        if node.right:
            self.print_bottom_view_of_tree_util(node.right, level+1, hash, depth+1)

        return hash


if __name__ == "__main__":
    tree = Tree()
    tree.root = Node(20)

    tree.root.left = Node(8)
    tree.root.right = Node(22)

    tree.root.left.left = Node(5)
    tree.root.left.right = Node(3)

    tree.root.right.left = Node(4)
    tree.root.right.right = Node(25)

    tree.root.left.right.left = Node(10)
    tree.root.left.right.right = Node(14)

    tree.print(tree.root)
    print()

    tree.print_bottom_view_of_tree()
