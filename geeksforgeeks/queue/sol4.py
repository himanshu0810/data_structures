
# Priority queue implementation using max-heap
# Insert - O(logn)
# Delete - O(logn)
# Extract Max - O(1)


class PriorityQueue:
    def __init__(self):
        self.array = []
        self.len = 0

    def get_element(self, index):
        return self.array[index]

    def swap_elements(self, index, index1):
        self.array[index], self.array[index1] = self.array[index1], self.array[index]

    @staticmethod
    def has_parent(child_index):
        return int((child_index - 1) / 2) > -1

    def has_left_child(self, parent_index):
        return 2 * parent_index + 1 < self.len

    def has_right_child(self, parent_index):
        return 2 * parent_index + 2 < self.len

    @staticmethod
    def get_parent_index(child_index):
        return int((child_index - 1) / 2)

    @staticmethod
    def get_left_child_index(parent_index):
        return 2 * parent_index + 1

    @staticmethod
    def get_right_child_index(parent_index):
        return 2 * parent_index + 2

    def get_parent(self, child_index):
        if not PriorityQueue.has_parent(child_index):
            return
        return self.array[PriorityQueue.get_parent_index(child_index)]

    def get_left_child(self, parent_index):
        if not self.has_left_child(parent_index):
            return
        return self.get_element(self.get_left_child_index(parent_index))

    def get_right_child(self, parent_index):
        if not self.has_right_child(parent_index):
            return
        return self.get_element(self.get_right_child_index(parent_index))

    def insert(self, data):
        if self.len == 0:
            self.len = self.len + 1
            return self.array.append(data)

        self.len = self.len + 1
        self.array.append(data)
        self.heapify_up()
        return

    def get_highest_priority(self):
        if not self.array:
            return
        return self.array[0]

    def delete_highest_priority(self):
        if not self.array:
            return

        if self.len == 1:
            self.len = self.len - 1
            return self.array.pop()

        self.len = self.len - 1
        self.array[0] = self.array.pop()
        self.heapify_down()
        return

    def print(self):
        print(self.array)

    def heapify_up(self):
        last_index = self.len - 1
        while self.has_parent(last_index):
            parent_index = self.get_parent_index(last_index)
            if self.get_element(last_index) > self.get_element(parent_index):
                self.swap_elements(last_index, parent_index)
                last_index = parent_index
                continue
            break

    def heapify_down(self):
        parent_index = 0
        while self.has_left_child(parent_index):

            max_child_index = self.get_left_child_index(parent_index)
            if self.has_right_child(parent_index) and \
                    self.get_element(self.get_right_child_index(parent_index)) > \
                    self.get_element(self.get_left_child_index(parent_index)):
                    max_child_index = self.get_right_child_index(parent_index)
            if self.get_element(parent_index) > self.get_element(max_child_index):
                break
            self.swap_elements(max_child_index, parent_index)
            parent_index = max_child_index


if __name__ == "__main__":
    pq = PriorityQueue()
    pq.insert(20)
    pq.insert(3)
    pq.insert(1)
    pq.insert(10)
    pq.insert(16)
    pq.insert(12)
    pq.print()
    print(pq.get_highest_priority())
    pq.delete_highest_priority()
    pq.print()
    pq.delete_highest_priority()
    pq.print()
