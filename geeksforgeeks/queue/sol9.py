
# Complete - Maximum of all sub-arrays of size k


class Queue:
    def __init__(self):
        self.elements = []
        self.top = -1

    def __repr__(self):
        return str(self.elements)

    def insert(self, element):
        self.elements.append(element)
        self.top = self.top + 1

    def print_queue(self):
        print(self.elements)

    def delete(self):
        if self.top == -1:
            return

        self.top = self.top - 1
        element = self.elements.pop(0)
        return element


if __name__ == "__main__":
    arr = [8, 5, 10, 7, 9, 4, 15, 12, 90, 13]
    k = 4
    max_elements = []
    queue = Queue()
    for idx, each in enumerate(arr):
        if idx < k - 1:
            queue.insert(each)
            continue
        queue.insert(each)
        max_elements.append(max(queue.elements))
        queue.delete()

    print("Max Elements are ", max_elements)
