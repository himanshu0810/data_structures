
# Complete - Find the first circular tour that visits all petrol pumps


if __name__ == "__main__":
    pumps = [(6, 5), (4, 6), (4, 5), (7, 3)]

    number_of_pumps = 4

    current_idx = 0
    start_idx = None
    sum1 = 0

    while current_idx != start_idx:
        if current_idx == len(pumps):
            if start_idx is None:
                break
            current_idx = 0
        pump_number, distance = pumps[current_idx]
        if sum1 - distance + pump_number <= 0:
            start_idx = None
        else:
            if start_idx is None:
                start_idx = current_idx
            sum1 = sum1 - distance + pump_number
        current_idx = current_idx + 1

    print("start index is ", start_idx)
