
# Complete - Queue implementation using array


class Queue:
    def __init__(self, capacity):
        self.capacity = capacity
        self.front = -1
        self.rear = -1
        self.data = [None] * self.capacity

    def is_full(self):
        return self.rear + 1 == self.capacity

    def is_empty(self):
        return self.front == -1

    def enqueue(self, data):
        if self.is_full():
            print("Full")
            return

        if self.front == -1:
            self.front = self.front + 1

        self.data[self.rear + 1] = data
        self.rear = self.rear + 1
        return self.rear

    def dequeue(self):
        if self.is_full():
            print("Full")
            return

        data = self.rear
        self.data[self.rear] = None
        self.rear = self.rear - 1
        if self.rear == -1:
            self.front = -1

        return data

    def que_front(self):
        if self.is_empty():
            print("Empty")
            return
        print(self.data[self.front])
        return self.data[self.front]

    def que_rear(self):
        if self.is_empty():
            print("Empty")
            return

        print(self.data[self.rear])
        return self.data[self.rear]


# Driver Code
if __name__ == '__main__':
    queue = Queue(10)

    queue.enqueue(10)
    queue.enqueue(20)
    queue.enqueue(30)
    queue.enqueue(40)
    queue.dequeue()
    queue.que_front()
    queue.que_rear()
    print(queue.data)
