
# Complete - Implement Deque using circular array - python list


# Complete


class Deque:
    def __init__(self):
        self.front = -1
        self.rear = -1
        self.array = []

    def is_empty(self):
        return self.front == -1

    def insert_at_front(self, data):
        if self.front == -1:
            self.front = self.front + 1
        self.rear = self.rear + 1
        self.array.insert(0, data)

    def insert_at_end(self, data):
        if self.front == -1:
            self.front = self.front + 1
        self.array.append(data)
        self.rear = self.rear + 1

    def delete_from_front(self):
        if not self.array:
            return

        self.array.pop(0)
        if not self.array:
            self.front = self.front - 1
        self.rear = self.rear - 1
        return

    def delete_from_end(self):
        if not self.array:
            return

        self.array.pop()
        if not self.array:
            self.front = self.front - 1
        self.rear = self.rear - 1
        return

    def get_front(self):
        if not self.array:
            return
        return self.array[self.front]

    def get_rear(self):
        if not self.array:
            return
        return self.array[-1]

    def print(self):
        print("front-{} , rear-{}, and the list is-{}".format(self.front, self.rear, self.array))


if __name__ == "__main__":
    dq = Deque()
    dq.insert_at_end(5)
    dq.print()
    dq.insert_at_front(10)
    print(dq.get_rear())
    dq.delete_from_end()
    print(dq.get_rear())
    dq.print()
    dq.insert_at_front(15)
    print(dq.front)
    dq.print()
    dq.delete_from_end()
    print(dq.front)
    dq.print()
