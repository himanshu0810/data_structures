
# Complete - Implement queue using two stacks


class Stack:
    def __init__(self):
        self.elements = []
        self.top = -1

    def push(self, element):
        self.elements.append(element)
        self.top = self.top + 1

    def is_empty(self):
        return self.top == -1

    def pop(self):
        if self.top == -1:
            return

        element = self.elements.pop()
        self.top = self.top - 1
        return element


class Queue:
    def __init__(self):
        self.st1 = Stack()
        self.st2 = Stack()

    def enqueue(self, element):
        self.st1.push(element)

    def shift_elements_from_st1_to_st2(self):
        st1 = self.st1
        st2 = self.st2
        while not st1.is_empty():
            st2.push(st1.pop())

    def deque(self):
        if self.st2.is_empty():
            self.shift_elements_from_st1_to_st2()

        if self.st2.is_empty():
            return

        element = self.st2.pop()
        print("Popped element is ", element)
        return element

    def deque_recursively(self):
        if self.st1.is_empty():
            return

        if self.st1.top == 0:
            element = self.st1.pop()
            return element

        element = self.st1.pop()
        res = self.deque_recursively()
        self.st1.push(element)
        return res


if __name__ == "__main__":

    queue = Queue()
    queue.enqueue(10)
    queue.enqueue(1)
    element = queue.deque_recursively()
    print("dequeued element is ", element)
    queue.enqueue(5)
    element = queue.deque_recursively()
    print("dequeued element is ", element)
    queue.enqueue(3)
    queue.enqueue(7)
    queue.enqueue(9)
    element = queue.deque_recursively()
    print("dequeued element is ", element)
    queue.enqueue(15)
    element = queue.deque_recursively()
    print("dequeued element is ", element)
    element = queue.deque_recursively()
    print("dequeued element is ", element)
    element = queue.deque_recursively()
    print("dequeued element is ", element)
