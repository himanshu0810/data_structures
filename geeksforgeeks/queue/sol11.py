
# Complete - How to efficiently implement k Queues in a single array


class NQueues:
    def __init__(self, n, size):
        self.n = 3
        self.front = [-1 for _ in range(n)]
        self.rear = [-1 for _ in range(n)]
        self.elements = [-1 for _ in range(size)]
        self.next = [i + 1 for i in range(size)]
        self.next[size-1] = -1
        self.free_idx = 0

    def print_queues(self):
        print(self.elements)
        print(self.next)
        print(self.front)
        print(self.rear)

    def enqueue(self, element, qn):
        free_qs = self.free_idx
        if free_qs == -1:
            raise OverflowError

        if self.front[qn] == -1:
            self.front[qn] = free_qs
        else:
            self.next[self.rear[qn]] = free_qs

        self.elements[free_qs] = element
        self.rear[qn] = free_qs
        self.free_idx = self.next[free_qs]
        return

    def deque(self, qn):
        if self.front[qn] == -1:
            return

        front_idx = self.front[qn]
        element = self.elements[front_idx]

        if self.front[qn] == self.rear[qn]:
            self.front[qn] = -1
            self.rear[qn] = -1
            return element

        self.front[qn] = self.next[front_idx]
        self.next[front_idx] = self.free_idx
        self.free_idx = front_idx
        return element


if __name__ == "__main__":
    k, n = 3, 10

    ns = NQueues(k, 10)

    ns.enqueue(15, 2)
    ns.enqueue(17, 1)
    ns.enqueue(49, 1)
    ns.enqueue(45, 2)
    ns.enqueue(39, 1)
    ns.enqueue(50, 2)
    ns.enqueue(3, 1)

    print(ns.deque(2))
    print(ns.deque(2))
    print(ns.deque(1))

    ns.enqueue(39, 1)
    ns.enqueue(50, 2)
    ns.enqueue(39, 1)
    ns.enqueue(50, 2)
    ns.enqueue(50, 2)
    ns.enqueue(39, 1)
