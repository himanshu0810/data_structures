
# Complete - Queue implement using linked list


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class QueueLinkedList:
    def __init__(self):
        self.front = None
        self.rear = None

    def is_empty(self):
        return self.front is None

    def enqueue(self, data):
        node = Node(data)
        if not self.front:
            self.front = node
            self.rear = node
            return

        self.rear.next = node
        self.rear = node

    def dequeue(self):
        if self.is_empty():
            print("empty")
            return

        node = self.front.data
        self.front = self.front.next
        return node

    def que_front(self):
        if self.is_empty():
            print("empty")
            return
        print(self.front.data)
        return self.front.data

    def que_rear(self):
        if self.is_empty():
            print("empty")
            return
        print(self.rear.data)
        return self.rear.data


# Driver Code
if __name__ == '__main__':
    queue = QueueLinkedList()
    queue.enqueue(10)
    queue.enqueue(20)
    queue.enqueue(30)
    queue.enqueue(40)
    queue.dequeue()
    queue.que_front()
    queue.que_rear()
