
# Complete - Print binary number from decimal number


class Queue:
    def __init__(self):
        self.elements = []
        self.top = -1

    def __repr__(self):
        return str(self.elements)

    def insert(self, element):
        self.elements.append(element)
        self.top = self.top + 1

    def print_queue(self):
        print(self.elements)

    def pop(self):
        if self.top == -1:
            return

        self.top = self.top - 1
        element = self.elements.pop(0)
        return element


if __name__ == "__main__":
    n = 10
    queue = Queue()
    queue.insert(1)
    for i in range(n):
        s = queue.pop()
        print(s, end=' ')
        s1 = str(s)

        s2 = s1 + "0"
        queue.insert(s2)

        s3 = s1 + "1"
        queue.insert(s3)
