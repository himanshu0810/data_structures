
# Least common ancestor in a tree


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class Tree:
    def __init__(self):
        self.root = None

    def print(self, node):
        if node:
            self.print(node.left)
            print(node.data, end=' ')
            self.print(node.right)

    def return_least_common_ancestor(self, lower, higher):
        val = self.return_least_common_ancestor_util(self.root, lower, higher)
        print("Least Common Ancestor value is ", val)

    def return_least_common_ancestor_util(self, node, lower, higher):
        if node is None:
            return

        if lower < node.data and higher < node.data:
            return self.return_least_common_ancestor_util(node.left, lower, higher)
        elif lower > node.data and higher > node.data:
            return self.return_least_common_ancestor_util(node.right, lower, higher)

        return node.data


if __name__ == "__main__":
    tree = Tree()
    tree.root = Node(8)
    tree.root.left = Node(2)
    tree.root.right = Node(15)
    tree.root.right.left = Node(11)
    tree.root.right.right = Node(18)
    tree.root.right.left.left = Node(9)
    tree.root.right.left.right = Node(13)

    tree.print(tree.root)
    print()
    tree.return_least_common_ancestor(11, 9)
