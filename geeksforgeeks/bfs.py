from collections import defaultdict


class Graph:
    def __init__(self):
        self.graph = defaultdict(list)

    def add_edge(self, a, b):
        self.graph[a].append(b)
        self.graph[b].append(a)

    def print_all_edges(self):
        for src, dst in self.graph.items():
            print(src, "--->", dst)

    def bsf(self, start):
        queue = [start]
        visited = [start]

        while queue:
            node = queue.pop(0)
            print(node, end=' ')

            children = self.graph[node]
            for child in children:
                if child in visited:
                    continue
                visited.append(child)
                queue.append(child)


if __name__ == "__main__":
    graph = Graph()
    graph.add_edge(1, 2)
    graph.add_edge(1, 3)
    graph.add_edge(2, 4)
    graph.add_edge(2, 5)
    graph.add_edge(3, 5)
    graph.add_edge(4, 5)
    graph.add_edge(4, 6)
    graph.add_edge(5, 6)

    graph.print_all_edges()
    print()
    graph.bsf(1)
