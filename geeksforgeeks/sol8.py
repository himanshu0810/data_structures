
class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __str__(self):
        return str(self.data)


def insert_recursively(root, data):
    if root is None:
        return Node(data)

    if root.data > data:
        root.left = insert_recursively(root.left, data)
    else:
        root.right = insert_recursively(root.right, data)

    return root


def inorder(root):
    if root:
        inorder(root.left)
        print(root.data, end=' ')
        inorder(root.right)


def return_kth(root, k):
    global k1
    k1 = k
    return return_kth_smallest_element(root)


def return_kth_smallest_element(root):
    global k1


    if root.left:
        return_kth_smallest_element(root.left)

    k1 = k1 - 1
    if k1 == 1:
        return root.data

    if root.right:
        return_kth_smallest_element(root.right)


if __name__ == "__main__":
    root = insert_recursively(None, 20)

    insert_recursively(root, 8)
    insert_recursively(root, 22)
    insert_recursively(root, 4)
    insert_recursively(root, 12)
    insert_recursively(root, 10)
    insert_recursively(root, 14)

    a = inorder(root)
    print()
    print("printing inorder of a tree")

    kth_element = return_kth(root, 3)
    print()
    print("Kth smallest element is ", kth_element)
