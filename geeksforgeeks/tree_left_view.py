

class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class Tree:
    def __init__(self):
        self.root = None

    def inorder(self, root):
        if root:
            self.inorder(root.left)
            print(root.data, end=' ')
            self.inorder(root.right)

    def level_wise_traversal(self):
        node = self.root
        queue = [node]
        while queue:
            next_queue = []
            first_char = None
            while queue:
                element = queue.pop(0)
                if not first_char:
                    print(element.data, end=' ')
                    first_char = True
                if element.left:
                    next_queue.append(element.left)
                    if element.left.left:
                        break
                    if element.left.right:
                        break
                if element.right:
                    next_queue.append(element.right)
                    if element.right.left:
                        break
                    if element.right.right:
                        break
            queue = next_queue

    def print_left_view(self):
        self.print_left_view_util(self.root, 1, [0])

    def print_left_view_util(self, root, level, max_level):
        if root is None:
            return

        if level > max_level[0]:
            print(root.data, end=' ')
            max_level[0] = level

        self.print_left_view_util(root.left, level+1, max_level)
        self.print_left_view_util(root.right, level+1, max_level)

    def return_tree_height(self):
        print()
        print("tree height is ", self.return_tree_height_util(self.root))

    def return_tree_height_util(self, root):
        if root is None:
            return 0
        return max(self.return_tree_height_util(root.left),
                   self.return_tree_height_util(root.right)) + 1


if __name__ == "__main__":
    tree = Tree()
    tree.root = Node(4)
    tree.root.left = Node(5)
    tree.root.right = Node(2)
    tree.root.right.left = Node(3)
    tree.root.right.right = Node(1)
    tree.root.right.left.left = Node(6)
    tree.root.right.left.right = Node(7)

    tree.print_left_view()

    tree.return_tree_height()