
# Complete - Find kth largest/smallest element in an array


from geeksforgeeks.heap.sol1 import BinaryHeap


if __name__ == "__main__":
    ary = [1, 23, 12, 9, 30, 2, 50]
    k = 3

    bin_heap = BinaryHeap()
    for each in ary:
        bin_heap.insert(each)

    print(bin_heap.elements)
    element = None
    while k > 0:
        element = bin_heap.extract_max()
        k = k - 1

    print("element is ", element)
