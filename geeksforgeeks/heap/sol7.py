
# Complete - Sort a nearly or (k sorted) array


from geeksforgeeks.heap.sol1 import BinaryHeap


if __name__ == "__main__":
    ary = [6, 5, 3, 2, 8, 10, 9]
    k = 3

    ary = [10, 9, 8, 7, 4, 70, 60, 50]
    k = 4

    min_heap = BinaryHeap()
    i = None
    for i in range(k+1):
        min_heap.insert(ary[i])
        i = i + 1

    while not min_heap.is_empty():
        min_element = min_heap.extract_max()
        if i < len(ary):
            min_heap.insert(ary[i])
        ary[i-k-1] = min_element
        i = i + 1

    print(ary)
