
# Why Binary Heap is preferred over BST

# Binary Heap we do not need pointers
# Extract Max will be done in O(1) time
# Better locality of reference
# Binary Heap will be build in O(n) time
# Constants in binary heap is lower than BST
