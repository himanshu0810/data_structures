
# Complete - Heap Sort


from geeksforgeeks.heap.sol1 import BinaryHeap


if __name__ == "__main__":
    ary = [4, 10, 3, 5, 1]

    min_heap = BinaryHeap()
    for each in ary:
        min_heap.insert(each)

    print(min_heap.elements)
    for i in range(len(ary)):
        min_element = min_heap.extract_max()
        ary[i] = min_element

    print(ary)
