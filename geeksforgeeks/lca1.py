

class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.data)

class Tree:
    def __init__(self):
        self.root = None

    def inorder(self, node):
        if node:
            self.inorder(node.left)
            print(node.data, end=' ')
            self.inorder(node.right)

    def least_common_ancestor(self, l, r):
        lca = self.least_common_ancestor_util(self.root, l, r, [None], [False], [False])
        print("least common ancestor is ", lca)

    def least_common_ancestor_util(self, node, l, r, current_root, l_find, r_find):
        if node is None:
            return

        if current_root[0] is None:
            current_root[0] = node

        if l_find[0] is False and l == node.data:
            l_find[0] = True

        if r_find[0] is False and r == node.data:
            r_find[0] = True
            return current_root[0].data

        self.least_common_ancestor_util(node.left, l, r, current_root, l_find, r_find)
        if l_find[0] and r_find[0]:
            return current_root[0].data

        if l_find[0]:
            current_root[0] = node

        return self.least_common_ancestor_util(node.right, l, r, current_root, l_find, r_find)


if __name__ == "__main__":
    tree = Tree()
    tree.root = Node(1)

    tree.root.left = Node(2)
    tree.root.right = Node(3)

    tree.root.left.left = Node(4)
    tree.root.left.right = Node(5)

    tree.root.right.left = Node(6)
    tree.root.right.right = Node(7)

    tree.inorder(tree.root)
    print()
    # tree.least_common_ancestor(4, 5)
    # tree.least_common_ancestor(4, 6)
    # tree.least_common_ancestor(3, 4)
    tree.least_common_ancestor(2, 4)

