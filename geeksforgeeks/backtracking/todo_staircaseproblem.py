from pprint import pprint


def find_all_paths_to_end_util(all_paths, paths, steps):
    if not paths:
        return all_paths

    path = paths.pop()
    for step in steps:
        if path == "4":
            all_paths.append(path)
            break
        last_step = path[-1]
        next_step = int(last_step) + step
        if next_step > 4:
            continue
        current_path = path + str(next_step)
        if next_step < 4:
            paths.append(current_path)
        else:
            all_paths.append(current_path)
    find_all_paths_to_end_util(all_paths, paths, steps)
    return all_paths


def find_all_paths_to_end(steps):
    paths = ["1", "2"]
    steps = [1, 2]
    all_paths = []
    all_paths = find_all_paths_to_end_util(all_paths, paths, steps)
    pprint(all_paths)


if __name__ == "__main__":
    print("ji")
    paths = find_all_paths_to_end(10)
