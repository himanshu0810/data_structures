
# Complete - Return Subset Sum

import copy
from pprint import pprint


def return_subset_sum_util(ary, sum1, paths, index, current_path):
    if index == len(ary):
        return

    if sum(current_path) > sum1:
        return

    current_path.append(ary[index])
    return_subset_sum_util(ary, sum1, paths, index+1, current_path)

    if current_path and (sum1 == sum(current_path)):
        paths.append(copy.deepcopy(current_path))

    current_path.remove(ary[index])
    return_subset_sum_util(ary, sum1, paths, index+1, current_path)


def return_subset_sum(ary, sum1):
    paths = []
    current_path = []
    return_subset_sum_util(ary, sum1, paths, 0, current_path)
    pprint(paths)


if __name__ == "__main__":
    ary = [10, 7, 5, 18, 12, 20, 15]
    sum1 = 35
    return_subset_sum(ary, sum1)
