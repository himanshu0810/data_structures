
# Complete - Soduku


from pprint import pprint


def check_number_to_add_in_grid(problem, number, row, col):
    init_row, init_col = row - row % 3, col - col % 3
    for i in range(3):
        for j in range(3):
            if number == problem[init_row + i][init_col + j]:
                return False
    return True


def return_number_belong_to_row_or_col(problem, number, row_number, col_number):
    for i in range(9):
        if problem[row_number][i] == number or problem[i][col_number] == number:
            return False
    return True


def is_safe(problem, number, row, col):
    if not check_number_to_add_in_grid(problem, number, row, col):
        return False
    if not return_number_belong_to_row_or_col(problem, number, row, col):
        return False
    return True


def return_filled_soduku_util(problem, row, col):
    if row == len(problem):
        return problem

    if col == 9:
        return return_filled_soduku_util(problem, row + 1, 0)

    if problem[row][col] is not None:
        return return_filled_soduku_util(problem, row, col + 1)

    for choice in range(1, 10):
        if is_safe(problem, choice, row, col):
            problem[row][col] = choice
            if return_filled_soduku_util(problem, row, col + 1):
                return problem
            else:
                problem[row][col] = None
    return


def return_filled_soduku(problem):
    return_filled_soduku_util(problem, 0, 0)
    pprint(problem)


if __name__ == "__main__":
    problem = [
        [3, None, 6, 5, None, 8, 4, None, None],
        [5, 2, None, None, None, None, None, None, None],
        [None, 8, 7, None, None, None, None, 3, 1],
        [None, None, 3, None, 1, None, None, 8, None],
        [9, None, None, 8, 6, 3, None, None, 5],
        [None, 5, None, None, 9, None, 6, None, None],
        [1, 3, None, None, None, None, 2, 5, None],
        [None, None, None, None, None, None, None, 7, 4],
        [None, None, 5, 2, None, 6, 3, None, None]
    ]
    return_filled_soduku(problem)
