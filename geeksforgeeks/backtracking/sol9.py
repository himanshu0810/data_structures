
# Complete - Tug Of War

import sys


class TugOfWar(object):
    def __init__(self):
        self.min_diff = sys.maxsize + 1

    def tug_of_war(self, arr):
        n = len(arr)
        current_elements = [False for _ in range(n)]
        soln = [False for _ in range(n)]

        sum1 = sum(arr)
        self.tug_of_war_util(arr, n, current_elements, 0, soln, sum1, 0, 0)

        print("The first subset is ")
        for idx, each in enumerate(soln):
            if each:
                print(arr[idx], end=' ')

        print()
        print("The second subset is ")
        for idx, each in enumerate(soln):
            if not each:
                print(arr[idx], end=' ')

    def tug_of_war_util(self, arr, n, current_elements, no_of_selected_elements, soln, sum1,
                        curr_sum, curr_position):
        if curr_position == n:
            return

        if ((n/2) - no_of_selected_elements) > (n - curr_position):
            return

        if no_of_selected_elements == n // 2:

            if abs(sum1 / 2 - curr_sum) < self.min_diff:
                self.min_diff = abs(sum1 / 2 - curr_sum)
                for i in range(n):
                    soln[i] = current_elements[i]

        no_of_selected_elements = no_of_selected_elements + 1
        curr_sum = curr_sum + arr[curr_position]
        current_elements[curr_position] = True

        self.tug_of_war_util(arr, n, current_elements, no_of_selected_elements, soln, sum1,
                             curr_sum, curr_position + 1)

        no_of_selected_elements = no_of_selected_elements - 1
        curr_sum = curr_sum - arr[curr_position]
        current_elements[curr_position] = False

        self.tug_of_war_util(arr, n, current_elements, no_of_selected_elements, soln, sum1,
                             curr_sum, curr_position + 1)


if __name__ == "__main__":
    arr1 = [23, 45, -34, 12, 0, 98, -99, 4, 189, -1, 4]
    a = TugOfWar()
    a.tug_of_war(arr1)
