
# Complete - Check hamiltonion cycle is present or not and return path if yes


from collections import defaultdict


class Graph:
    def __init__(self, V):
        self.V = V
        self.graph = defaultdict(list)
        self.vertices = set()

    def insert_edge(self, src, dst, weight=0):
        self.graph[src].append(dst)
        self.graph[dst].append(src)
        self.vertices.add(src)
        self.vertices.add(dst)

    def print_graph(self):
        for k, v in self.graph.items():
            print(k, "-->", v)

    def check_hamiltonian_cycle_with_path_util(self, source_vertex, paths, vertices, init_vertex):
        if vertices is None:
            return

        vertices.remove(source_vertex)
        paths.append(source_vertex)
        dst_vertices = self.graph[source_vertex]
        for dst in dst_vertices:
            if dst not in vertices:
                continue

            flag = self.check_hamiltonian_cycle_with_path_util(dst, paths, vertices, init_vertex)
            if flag:
                return paths
            else:
                vertices.add(dst)

        if not vertices and init_vertex in self.graph[source_vertex]:
            return paths

        paths.remove(source_vertex)
        return False

    def check_hamiltonian_cycle_with_path(self):
        vertices = self.vertices.copy()
        for each in vertices:
            result = self.check_hamiltonian_cycle_with_path_util(each, [], vertices, each)
            if result:
                print("result is ", result)
                return result
        print("No Hamiltonian cycle is present")


if __name__ == "__main__":
    gp = Graph(5)
    gp.insert_edge(0, 1)
    gp.insert_edge(1, 2)
    gp.insert_edge(2, 3)
    gp.insert_edge(3, 4)
    gp.insert_edge(4, 5)
    gp.insert_edge(5, 6)
    gp.insert_edge(6, 7)
    gp.insert_edge(7, 8)
    gp.insert_edge(8, 9)
    gp.insert_edge(9, 0)
    gp.insert_edge(9, 5)
    gp.insert_edge(0, 4)
    gp.insert_edge(3, 6)
    gp.insert_edge(1, 8)
    gp.insert_edge(2, 7)

    print(gp.check_hamiltonian_cycle_with_path())
