
# Complete - N queens problem


from pprint import pprint


def check_for_attack(result, row, col):
    for i in range(row):
        for j in range(len(result[row])):
            value = result[i][j]
            if value is None:
                continue
            if j == col or abs((col-j)/(row-i)) == 1.0:
                return True
    return False


def no_of_queens_util(queens, result, row):
    if row == queens:
        return result

    for j in range(queens):
        if not check_for_attack(result, row, j):
            result[row][j] = 1
            if no_of_queens_util(queens, result, row+1):
                return True
            result[row][j] = None

    return False


def no_of_queens(queens):
    result = [[None for _ in range(queens)] for _ in range(queens)]
    no_of_queens_util(queens, result, 0)
    return result


if __name__ == "__main__":
    matrix = no_of_queens(8)
    pprint(matrix)
