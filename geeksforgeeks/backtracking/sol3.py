
# Complete - Rat in a maze


from pprint import pprint


def find_next_step(maze, row, col):
    next_row = row + 1
    next_col = col + 1

    moves = []
    if next_col < len(maze) and maze[row][next_col] == 1:
        moves.append((row, next_col))
    if next_row < len(maze) and maze[next_row][col] == 1:
        moves.append((next_row, col))

    return moves


def fill_result_with_a_way(maze, result, row, col):
    if row == len(maze)-1 and col == len(maze)-1:
        return result

    moves = find_next_step(maze, row, col)
    for next_row, next_col in moves:

        result[next_row][next_col] = 1
        flag = fill_result_with_a_way(maze, result, next_row, next_col)
        if flag:
            return result
        else:
            result[next_row][next_col] = 0

    return False


if __name__ == "__main__":
    maze = [
        [1, 0, 0, 0],
        [1, 1, 0, 1],
        [0, 1, 0, 0],
        [0, 1, 1, 1]
    ]
    result = [
        [1, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ]
    fill_result_with_a_way(maze, result, 0, 0)
    print("Results are ")
    pprint(result)
