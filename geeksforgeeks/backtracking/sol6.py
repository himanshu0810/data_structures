
# Complete - m Coloring Problem

from pprint import pprint


def return_adjacent_edges(graph, vertex):
    adjacent_edges = []

    for idx, each in enumerate(graph[vertex]):
        if idx == vertex:
            continue

        if each == 1:
            adjacent_edges.append(idx)

    return adjacent_edges


def return_color_choices(graph, colors, vertex, results):

    colors_copy = colors.copy()
    adjacent_edges = return_adjacent_edges(graph, vertex)
    for each in adjacent_edges:
        if each not in results:
            continue

        color_assigned = results[each]
        colors_copy.remove(color_assigned)

    if not colors:
        return False
    return adjacent_edges, colors_copy


def return_graph_colors(graph, no_of_vertices, colors_list, no, results):
    if no == no_of_vertices:
        return results

    adjacent_edges, color_choices = return_color_choices(graph, colors_list, no, results)
    for color in color_choices:
        results[no] = color
        flag = return_graph_colors(graph, no_of_vertices, colors_list, no+1, results)
        if flag:
            return results
        else:
            results.pop(no)
    return False


if __name__ == "__main__":
    graph = [
        [0, 1, 1, 1],
        [1, 0, 1, 0],
        [1, 1, 0, 1],
        [1, 0, 1, 0]
    ]
    no_of_colors = 2

    colors = [each for each in range(no_of_colors)]
    result = return_graph_colors(graph, len(graph), colors, 0, {})
    pprint(result)
