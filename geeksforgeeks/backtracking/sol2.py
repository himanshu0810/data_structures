
# Complete - The knight's tour problem


def is_safe(x, y, sol):
    N = 8
    return 0 <= x < N and 0 <= y < N and sol[x][y] is None


def return_knight_tour_path_util(total_squares, matrix, count, row, col, xmove, ymove):
    if total_squares == count:
        return matrix

    for j in range(len(matrix)):
        new_row = row + xmove[j]
        new_col = col + ymove[j]
        if is_safe(new_row, new_col, matrix):
            matrix[new_row][new_col] = count
            if return_knight_tour_path_util(total_squares, matrix, count+1, new_row, new_col,
                                              xmove, ymove):
                return True
            matrix[new_row][new_col] = None
    return False


def return_knight_tour_path(matrix):
    count = 1
    total_squares = 64
    matrix[0][0] = 0
    xMove = [2, 1, -1, -2, -2, -1, 1, 2]
    yMove = [1, 2, 2, 1, -1, -2, -2, -1]
    return_knight_tour_path_util(total_squares, matrix, count, 0, 0, xMove, yMove)
    print(matrix)


if __name__ == "__main__":
    rows = 8
    matrix = [[None for _ in range(8)] for _ in range(8)]
    return_knight_tour_path(matrix)
