

# Complete - Print all permutations of a string


def permute(string1, start, end):
    if start == end:
        print("".join(string1))
    else:
        for i in range(start, end+1):
            string1[start], string1[i] = string1[i], string1[start]
            permute(string1, start + 1, end)
            string1[start], string1[i] = string1[i], string1[start]


def print_all_string(string):
    n = len(string)
    a = list(string)
    permute(a, 0, n-1)


if __name__ == "__main__":
    stringk = "ABCn"
    print_all_string(stringk)
