# Complete Staircase problem


from pprint import pprint


def return_all_paths_util(all_paths, paths, max_step, steps):

    if not paths:
        for each in steps:
            if each < max_step:
                paths.append([each])
                return_all_paths_util(all_paths, paths, max_step, steps)
    else:
        new_paths = []
        for idx, each in enumerate(paths):
            if not each:
                continue
            sum_of_steps = sum(each)
            for each_step in steps:
                if sum_of_steps + each_step < max_step:
                    each_copy = each.copy()
                    each_copy.append(each_step)
                    new_paths.append(each_copy)
                    return_all_paths_util(all_paths, new_paths, max_step, steps)
                elif sum_of_steps + each_step == max_step:
                    each_copy = each.copy()
                    each_copy.append(each_step)
                    all_paths.append(each_copy)
            paths[idx] = []
    return all_paths


def return_all_paths(max_step, steps):
    paths = []
    all_paths = []
    all_paths = return_all_paths_util(all_paths, paths, max_step, steps)
    return all_paths


if __name__ == "__main__":
    max_step = 4
    steps = [1, 2, 3]
    paths = return_all_paths(max_step, steps)
    pprint(paths)
