
# Least common ancestor in a tree


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.data)


class Tree:
    def __init__(self):
        self.root = None

    def print(self, node):
        if node:
            self.print(node.left)
            print(node.data, end=' ')
            self.print(node.right)

    def diameter(self):
        max_diameter = self.diameter_util(tree.root, 0)
        print("Max Diameter is ", max_diameter)

    def height_of_node(self, node):
        if node is None:
            return 0

        return max(self.height_of_node(node.left), self.height_of_node(node.right)) + 1

    def diameter_util(self, node, max_diameter):
        if node is None:
            return 0

        left_diameter = self.diameter_util(node.left, max_diameter)
        right_diameter = self.diameter_util(node.right, max_diameter)

        left_height = self.height_of_node(node.left)
        right_height = self.height_of_node(node.right)

        return max(left_diameter, right_diameter, left_height+right_height+1)


if __name__ == "__main__":
    tree = Tree()
    tree.root = Node(8)
    tree.root.left = Node(2)
    tree.root.right = Node(15)
    tree.root.right.left = Node(11)
    tree.root.right.right = Node(18)
    tree.root.right.left.left = Node(9)
    tree.root.right.left.right = Node(13)

    tree.print(tree.root)
    print()
    print("Diameter of tree is ", tree.diameter())
