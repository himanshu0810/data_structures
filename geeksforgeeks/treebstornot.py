import sys


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.data)


class Tree:
    def __init__(self):
        self.root = None

    def inorder(self, root):
        if root:
            self.inorder(root.left)
            print(root.data, end=' ')
            self.inorder(root.right)

    def is_tree_bst_or_not(self):
        node = self.root
        print()
        print("Tree is BST ", self.is_tree_bst_or_not_util(node, -sys.maxsize, sys.maxsize))

    def is_tree_bst_or_not_util(self, node, min, max):
        if node is None:
            return True

        if node.data < min or node.data > max:
            return False

        if not self.is_tree_bst_or_not_util(node.left, min, node.data):
            return False

        if not self.is_tree_bst_or_not_util(node.right, node.data, max):
            return False

        return True


if __name__ == "__main__":
    tree = Tree()
    tree.root = Node(4)
    tree.root.left = Node(2)
    tree.root.right = Node(5)
    tree.root.left.left = Node(1)
    tree.root.left.right = Node(3)

    tree.inorder(tree.root)
    tree.is_tree_bst_or_not()
    # tree.root.right.left.left = Node(6)
    # tree.root.right.left.right = Node(7)
