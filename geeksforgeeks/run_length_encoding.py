
# run length encoding


def return_modified_s(s):
    count = None
    prev_ch = None

    idx = 0
    s_length = len(s)
    actual_idx = 0
    while idx < s_length:
        if prev_ch is None:
            prev_ch = s[idx]
            count = 1
            actual_idx = 1
        else:
            curr_ch = s[idx]
            if curr_ch == prev_ch:
                count = count + 1
            else:
                s = s[:actual_idx] + str(count) + s[actual_idx+count-1:]
                actual_idx = actual_idx + 2
                count = 1
                prev_ch = curr_ch


        idx = idx + 1
    return s


if __name__ == "__main__":
    s = "wwwwaaadexxxxxx"

    # return like w4a3d1e1x6
    modified_s = return_modified_s(s)
    print("Modified string is ", s)
