

class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None



class Tree:
    def __init__(self):
        self.root = None


    def print(self, node):
        if node:
            self.print(node.left)
            print(node.data, end=' ')
            self.print(node.right)


    def height_of_tree(self, node):
        if node is None:
            return 0
        return max(self.height_of_tree(node.left), self.height_of_tree(node.right)) + 1



if __name__ == "__main__":
    tree = Tree()
    tree.root = Node(10)
    tree.root.left= Node(1)
    tree.root.right = Node(2)
    tree.root.right.left = Node(5)
    tree.root.right.right = Node(6)
    tree.root.right.left.right = Node(7)
    tree.root.right.left.left = Node(8)

    tree.print(tree.root)
    print()
    height = tree.height_of_tree(tree.root)
    print("Height of tree is ", height)
