class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

def return_binary_tree_util(inorder, preorder, min, max):

    root_ch = preorder[0]

    head = Node(root_ch)
    root_in_inorder = inorder.index(root_ch)
    preorder_last_node = preorder[root_in_inorder-1]
    head.left = return_binary_tree(inorder[:root_in_inorder], preorder[1:preorder.index()])

    return head


def return_binary_tree(inorder, preorder):


    return node


def print_binary_tree(root):
    if root:
        print_binary_tree(root.left)
        print(root.data)
        print_binary_tree(root.right)


if __name__ == "__main__":
    inorder = "bdeafc"
    preorder = "abdecf"
    node = return_binary_tree(inorder, preorder)
    print_binary_tree(node)
