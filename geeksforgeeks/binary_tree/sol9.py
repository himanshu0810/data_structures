# breadth first traversal of a tree


class Node:
    def __init__(self, val):
        self.value = val
        self.left = None
        self.right = None


def breadth_first_traversal(root):
    if not root:
        return

    list = [root]
    while list:
        node = list.pop(0)
        print(node.value, end=' ')
        if node.left:
            list.append(node.left)
        if node.right:
            list.append(node.right)


def print_left_view(root):
    if not root:
        return

    list = [root]
    while list:
        node = list.pop(0)
        print(node.value, end=' ')
        if node.left:
            list.append(node.left)


def print_level_view(root, level):
    if root is None:
        return

    if level == 0:
        print(root.value, end=' ')
    else:
        print_level_view(root.left, level-1)
        print_level_view(root.right, level-1)


def print_all_level(root):
    for level in range(0, height(root)):
        print_level_view(root, level)
        print()


def height(root):
    if not root:
        return 0

    return 1 + max(height(root.left), height(root.right))


class Tree:
    def __init__(self):
        self.root = None


if __name__ == "__main__":
    root = Node(1)
    root.left = Node(2)
    root.right = Node(3)

    root.right.left = Node(10)

    root.left.left = Node(4)
    root.left.right = Node(5)

    root.left.right.left = Node(6)
    root.left.right.right = Node(7)
    root.left.right.right.right = Node(8)

    print()
    print()
    print("breadth first traversal")
    breadth_first_traversal(root)

    print()
    print()
    print_left_view(root)

    print()
    print()
    print("print level view")
    print_level_view(root, 3)

    print()
    print()
    print("height of the tree is")
    print(height(root))

    print()
    print()
    print("print all level view is")
    print_all_level(root)
