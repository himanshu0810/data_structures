# height of a tree


class Node:
    def __init__(self, val):
        self.value = val
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.value)


def return_height_of_tree(root):
    if root is None:
        return 0

    return 1 + max(return_height_of_tree(root.left), return_height_of_tree(root.right))


def return_height_of_tree_iteratively(root):

    height = 0
    current_list = [root]
    while current_list:
        new_list = list()
        height = height + 1
        for each in current_list:
            if each.left:
                new_list.append(each.left)
            if each.right:
                new_list.append(each.right)
        current_list = new_list
    return height

if __name__ == "__main__":
    root = Node(1)

    root.left = Node(2)
    root.right = Node(3)

    root.left.left = Node(4)
    root.left.right = Node(5)

    root.left.right.left = Node(6)
    height = return_height_of_tree(root)
    print("height of tree is ", height)

    print()
    height = return_height_of_tree_iteratively(root)
    print("height of tree iteratively ", height)