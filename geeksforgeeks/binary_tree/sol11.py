# inorder without stack

class Node:
    def __init__(self, val):
        self.value = val
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.value)


def print_inorder(root):
    if root.left:
        print_inorder(root.left)
    print(root.value, end=' ')
    if root.right:
        print_inorder(root.right)


def print_inorder_without_stack(root):
    if not root:
        return

    current, stack = root, []

    while True:
        if current is not None:
            stack.append(current)
            current = current.left
            continue

        if stack:
            current = stack.pop()
            print(current.value, end=' ')
            current = current.right
            continue

        break


if __name__ == "__main__":
    root = Node(1)

    root.left = Node(2)
    root.right = Node(3)

    root.left.left = Node(4)
    root.left.right = Node(5)

    print()
    print()
    print("printing inorder ")
    print_inorder(root)

    print()
    print()
    print("printing inorder without stack ")
    print_inorder_without_stack(root)
