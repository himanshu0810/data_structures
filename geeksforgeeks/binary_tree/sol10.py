# Diameter of a tree


class Node:
    def __init__(self, val):
        self.value = val
        self.left = None
        self.right = None


def height(root):
    if not root:
        return 0

    return 1 + max(height(root.left), height(root.right))


def diameter_of_tree(root, height):
    if root is None:
        return 0, 0

    diameter_left, lheight = diameter_of_tree(root.left, height)
    diameter_right, rheight = diameter_of_tree(root.right, height)
    return max(lheight + rheight + 1, diameter_left, diameter_right), max(lheight, rheight) + 1


if __name__ == "__main__":
    root = Node(1)

    root.left = Node(2)
    root.right = Node(3)

    root.left.left = Node(4)
    root.left.right = Node(5)

    print()
    print()
    print("Diameter of a tree ")
    print(diameter_of_tree(root, 0)[0])