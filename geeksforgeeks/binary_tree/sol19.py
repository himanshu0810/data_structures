
# Print the number of nodes at kth level


class Node:
    def __init__(self, val):
        self.data = val
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.data)


def print_tree(root):
    if root:
        print_tree(root.left)
        print(root.data)
        print_tree(root.right)


def k_nodes_recursively_util(root, k, output):
    if k == 0:
        output.append(root.data)
        return output

    if k < 0:
        return output

    if root.left:
        k_nodes_recursively_util(root.left, k-1, output)
    if root.right:
        k_nodes_recursively_util(root.right, k-1, output)
    return output


def k_nodes_recursively(root, k):
    output = []
    k_nodes_recursively_util(root, k, output)
    print("output is ", output)
    return output


def k_nodes_iteratively(root, k):
    queue = [root]
    if k == 0:
        return queue
    while True:
        new_queue = []
        if k == 0:
            return queue
        elif k < 0:
            return None
        k = k - 1
        while queue:
            root = queue.pop(0)
            if root.left:
                new_queue.append(root.left)
            if root.right:
                new_queue.append(root.right)
        queue = new_queue


if __name__ == "__main__":
    root = Node(1)

    root.left = Node(2)
    root.right = Node(3)

    root.left.left = Node(4)
    root.left.right = Node(5)

    root.right.left = Node(8)
    k_nodes_recursively(root, 1)
    queue = k_nodes_iteratively(root, 1)
    print("Output is ", queue)
