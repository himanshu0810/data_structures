

# Check whether the two trees are symmetric or not


class Node:
    def __init__(self, val):
        self.data = val
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.data)


def return_both_trees_util(node, root1):
    if root1 is None and node is None:
        return True

    if node.data != root1.data:
        return False

    if node.data == root1.data and return_both_trees_util(node.left, root1.left) and \
            return_both_trees_util(node.right, root1.right):
        return True

    return False


def print_tree(root):
    if root:
        print_tree(root.left)
        print(root.data, end=' ')
        print_tree(root.right)


def return_both_trees(root, root1):
    queue = [root1]

    flag = False
    while queue:
        new_queue = []
        node_count = len(queue)
        while node_count > 0:
            node_count = node_count - 1
            node = queue.pop(0)
            if node.data == root.data:
                flag = True
                new_queue = []
                break
            if node.left:
                new_queue.append(node.left)
            if node.right:
                new_queue.append(node.right)
        queue = new_queue

    if not flag:
        return

    bool = return_both_trees_util(node, root)
    return bool


if __name__ == "__main__":
    root = Node(10)
    root.left = Node(4)
    root.right = Node(6)
    root.left.right = Node(30)

    print("First Tree")
    print_tree(root)

    root1 = Node(26)
    root1.left = Node(10)
    root1.right = Node(3)
    root1.left.left = Node(4)
    root1.left.right = Node(6)
    root1.right.right = Node(32)
    root1.left.left.right = Node(30)

    print()
    print("Second Tree")
    print_tree(root1)

    print()
    bool = return_both_trees(root, root1)
    print("Treees are symmeteric ", bool)
