

class Node:
    def __init__(self, val):
        self.value = val
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.value)


def swap_if_max(max_width, current_width):
    if current_width > max_width:
        return current_width
    return max_width


def return_max_width_of_tree(root):

    max_width = 0
    current_level = [root]
    while True:
        node_count = len(current_level)
        max_width = swap_if_max(max_width, node_count)
        if node_count == 0:
            break

        while node_count > 0:
            node = current_level.pop(0)
            if node.left:
                current_level.append(node.left)
            if node.right:
                current_level.append(node.right)
            node_count = node_count - 1
    return max_width


def height_of_tree(root):
    if not root:
        return 0

    return 1 + max(height_of_tree(root.left), height_of_tree(root.right))


def print_all_levels(root):
    height = height_of_tree(root)
    max_width = 0
    for each in range(0, height):
        width = print_at_level(root, each)
        max_width = swap_if_max(max_width, width)

    return max_width


def print_at_level(root, level):
    if not root:
        return 0

    if level == 0:
        return 1

    return print_at_level(root.left, level-1) + print_at_level(root.right, level-1)


if __name__ == "__main__":
    root = Node(1)

    root.left = Node(2)
    root.right = Node(3)

    root.left.left = Node(4)
    root.left.right = Node(5)

    root.right.right = Node(8)

    root.right.right.left = Node(6)
    root.right.right.right = Node(7)

    max_width = return_max_width_of_tree(root)
    print()
    print("Max Width of tree is ", max_width)

    print()
    print('print at level 2')
    print_at_level(root, 2)

    print()
    print('height of tree is ')
    print(height_of_tree(root))

    print()
    max_width = print_all_levels(root)
    print("max width by another method is ", max_width)
