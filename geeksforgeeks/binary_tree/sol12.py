# inorder traversal without stack and without recursion


class Node:
    def __init__(self, val):
        self.value = val
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.value)


def print_inorder(root):
    if root.left:
        print_inorder(root.left)
    print(root.value, end=' ')
    if root.right:
        print_inorder(root.right)


def morris_traversal(root):

    current = root
    while current is not None:
        if current.left is None:
            print(current.value, end=' ')
            current = current.right
        else:
            predecessor = current.left
            while predecessor.right != current and predecessor.right is not None:
                predecessor = predecessor.right

            if predecessor.right is None:
                predecessor.right = current
                current = current.left

            else:
                predecessor.right = None
                print(current.value, end=' ')
                current = current.right


if __name__ == "__main__":
    root = Node(1)

    root.left = Node(2)
    root.right = Node(3)

    root.left.left = Node(4)
    root.left.right = Node(5)

    print()
    print()
    print("printing inorder ")
    print_inorder(root)

    print()
    print()
    print("priting inorder without stack without recursion ")
    print(morris_traversal(root))
