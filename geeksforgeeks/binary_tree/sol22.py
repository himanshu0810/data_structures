# Connect the nodes at the same level and form the linked lists
import random


class Node:
    def __init__(self, val):
        self.data = val
        self.left = None
        self.right = None
        self.next_right = random.random()

    def __repr__(self):
        return str(self.data)


def print_tree(root):
    if root:
        print_tree(root.left)
        print(root.data, end=' ')
        print_tree(root.right)


def return_level_wise_connected_tree(root):
    if root is None:
        return
    root.next_right = None
    queue = [root]
    while queue:
        node_count = len(queue)
        new_queue = []
        prev_node = None
        while node_count > 0:
            node = queue.pop(0)
            node.next_right = None
            node_count = node_count - 1

            if prev_node:
                prev_node.next_right = node

            if node.left:
                new_queue.append(node.left)
            if node.right:
                new_queue.append(node.right)
            prev_node = node

        queue = new_queue
    return root


if __name__ == "__main__":
    root = Node(10)
    root.left = Node(4)
    root.right = Node(6)
    root.left.right = Node(30)
    root.right.right = Node(100)

    root = return_level_wise_connected_tree(root)
    print_tree(root)
    print()
    print("Next right of root", root.next_right)
    print(root.left.next_right.data)
    print(root.right.next_right)
    print(root.left.right.next_right)
