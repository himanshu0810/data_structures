# Preorder, inorder and postorder of a tree


class Node:
    def __init__(self, val):
        self.value = val
        self.left = None
        self.right = None


def print_preorder(root):
    print(root.value, end=' ')
    if root.left:
        print_preorder(root.left)
    if root.right:
        print_preorder(root.right)


def print_inorder(root):
    if root.left:
        print_inorder(root.left)
    print(root.value, end=' ')
    if root.right:
        print_inorder(root.right)


def print_postorder(root):
    if root.left:
        print_postorder(root.left)
    if root.right:
        print_postorder(root.right)
    print(root.value, end=' ')


if __name__ == "__main__":
    root = Node(1)
    root.left = Node(2)
    root.right = Node(3)

    root.right.left = Node(10)

    root.left.left = Node(4)
    root.left.right = Node(5)

    root.left.right.left = Node(6)
    root.left.right.right = Node(7)
    root.left.right.right.right = Node(8)

    print_inorder(root)

    print()
    print()
    print_postorder(root)

    print()
    print()
    print_preorder(root)
