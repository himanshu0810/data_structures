


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BinaryTree:
    def __init__(self):
        self.root = None

    def inorder(self, node):
        if node:
            self.inorder(node.left)
            print(node.data, end=' ')
            self.inorder(node.right)

    def recreate(self, node, binary_tree_1):
        if node:
            new_node = Node(node.data)
            if binary_tree_1.root is None:
                binary_tree_1.root = new_node

            new_node.left = self.recreate(node.left, binary_tree_1)
            new_node.right = self.recreate(node.right, binary_tree_1)
            return new_node
        return


if __name__ == "__main__":
    binary_tree = BinaryTree()
    binary_tree.root = Node(10)

    binary_tree.root.left = Node(5)
    binary_tree.root.right = Node(6)

    binary_tree.root.left.left = Node(1)
    binary_tree.root.left.right = Node(2)

    # binary_tree.inorder(binary_tree.root)

    binary_tree1 = BinaryTree()
    binary_tree.recreate(binary_tree.root, binary_tree1)
    print()
    binary_tree1.inorder(binary_tree1.root)
