
# Complete - Print all elements in sorted order from row and column wise sorted matrix


class Element:
    def __init__(self, data, row, col):
        self.data = data
        self.row = row
        self.col = col

    def __repr__(self):
        return str(self.data)


class BinaryHeap:
    def __init__(self):
        self.elements = []

    def is_empty(self):
        return len(self.elements) == 0

    def get_parent_index(self, child_index):
        return (child_index - 1) // 2

    def get_left_index(self, parent_index):
        return 2 * parent_index + 1

    def get_right_index(self, parent_index):
        return 2 * parent_index + 2

    def has_parent(self, child_index):
        return self.get_parent_index(child_index) > -1

    def has_left_child(self, parent_index):
        return self.get_left_index(parent_index) < len(self.elements)

    def has_right_child(self, parent_index):
        return self.get_right_index(parent_index) < len(self.elements)

    def get_parent(self, child_index):
        if self.has_parent(child_index):
            return self.elements[self.get_parent_index(child_index)]
        return

    def get_left_child(self, parent_index):
        if self.has_left_child(parent_index):
            return self.elements[self.get_left_index(parent_index)]
        return

    def get_right_child(self, parent_index):
        if self.has_right_child(parent_index):
            return self.elements[self.get_right_index(parent_index)]
        return

    def insert(self, element):
        self.elements.append(element)
        self.heapify_up()

    def extract_max(self):
        element = self.elements.pop(0)
        self.heapify_down(0)
        return element

    def get_max(self):
        if not self.elements:
            return
        return self.elements[0]

    def swap(self, idx, idx1):
        self.elements[idx], self.elements[idx1] = self.elements[idx1], self.elements[idx]

    def decrease_key(self, i, new_value):
        self.elements[i] = new_value
        self.heapify_down(i)

    def delete_key(self, i):
        element = self.elements.pop()
        element_1 = self.elements[i]
        self.elements[i] = element
        self.heapify_down(i)
        return element_1

    def heapify_up(self):
        idx = len(self.elements) - 1
        while self.has_parent(idx) and self.get_parent(idx).data > self.elements[idx].data:
            self.swap(idx, self.get_parent_index(idx))
            idx = self.get_parent_index(idx)

    def heapify_down(self, idx):

        while self.has_left_child(idx):

            bigger_child_index = self.get_left_index(idx)

            has_right_child = self.has_right_child(idx)
            right_child = self.get_right_child(idx)

            if has_right_child and \
                    self.elements[bigger_child_index].data > right_child.data:
                bigger_child_index = self.get_right_index(idx)

            if self.elements[bigger_child_index].data < self.elements[idx].data:
                self.swap(idx, bigger_child_index)
                idx = bigger_child_index
                continue
            break


def print_row_column_wise_sorted_matrix(matrix):
    min_heap = BinaryHeap()

    # initialize min_heap
    for i in range(len(matrix)):
        element = Element(matrix[i][0], i, 0)
        min_heap.insert(element)

    print("Printing sorted elements ")
    while not min_heap.is_empty():
        element = min_heap.extract_max()
        data, row, col = element.data, element.row, element.col
        print(data, end=' ')
        if col == len(matrix) - 1:
            continue
        min_heap.insert(Element(matrix[row][col+1], row, col+1))


if __name__ == "__main__":
    matrix = [
        [10, 20, 30, 40],
        [15, 25, 35, 45],
        [27, 29, 37, 48],
        [32, 33, 39, 50]
    ]
    print_row_column_wise_sorted_matrix(matrix)
