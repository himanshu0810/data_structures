
# Complete - Maximum size square sub-matrix with all 1s

import sys


def return_max_size_sib_matrix(matrix1):
    temp = []
    max_size = -sys.maxsize + 1
    coordinates = None
    for row in range(len(matrix1)):
        li = []
        for col in range(len(matrix1[0])):
            element = matrix1[row][col]
            if row == 0 or col == 0 or element == 0:
                li.append(element)
            else:
                element = min(temp[row-1][col], temp[row-1][col-1], li[col-1]) + 1
                li.append(element)

            if element > max_size:
                max_size = element
                coordinates = (row, col)

        temp.append(li)

    return max_size, coordinates


if __name__ == "__main__":
    matrix = [
        [0, 1, 1, 0, 1],
        [1, 1, 0, 1, 0],
        [0, 1, 1, 1, 0],
        [1, 1, 1, 1, 0],
        [1, 1, 1, 1, 1],
        [0, 0, 0, 0, 0],
    ]
    print(return_max_size_sib_matrix(matrix))
