
# Complete - Maximum sum rectangle in a 2D matrix | DP-27

import sys


def kadane_algorithm(array):
    max_so_far = 0
    max_ending_here = 0
    left = right = reset = True
    for idx, each in enumerate(array):
        max_ending_here = max_ending_here + each
        if max_ending_here < 0:
            max_ending_here = 0
            reset = True
        if max_so_far < max_ending_here:
            max_so_far = max_ending_here
            if reset:
                left = idx
                reset = False
            right = idx

    return max_so_far, left, right


def return_maximum_sum_with_coordinates(matrix):
    aleft = aright = atop = abottom = None
    max_sum = -sys.maxsize

    for left in range(len(matrix[0])):
        temp = [0 for _ in range(len(matrix))]

        for right in range(left, len(matrix[0])):

            for row in range(len(matrix)):
                temp[row] = temp[row] + matrix[row][right]

            sum1, rleft, rright = kadane_algorithm(temp)
            if sum1 > max_sum:
                max_sum = sum1
                atop = rleft
                abottom = rright
                aleft = left
                aright = right

    print(max_sum, (atop, aleft), (abottom, aright))


if __name__ == "__main__":
    # matrix = [[1, 2, -1, -4], [-8, -3, 4, 2], [3, 8, 10, 1], [-4, -1, 1, 7]]

    matrix = [
        [0, -2, -7, 0],
        [9, 2, -6, 2],
        [-4, 1, -4, 1],
        [-1, 8, 0, -2]
    ]
    return_maximum_sum_with_coordinates(matrix)

    # array = [-2, -3, 4, -1, -2, 1, 5, -3]
    # kadane_algorithm(array)
