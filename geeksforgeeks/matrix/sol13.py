
# Completed - Count number of islands where every island is row-wise
# and column-wise separated


def count_number_of_islands(matrix):
    count = 0
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            ch = matrix[i][j]
            if ch == 'O':
                continue

            if i == 0:
                if j == 0:
                    count = count + 1
                elif matrix[i][j-1] == 'O':
                    count = count + 1
            elif j == 0:
                if matrix[i-1][j] == 'O':
                    count = count + 1
            else:
                if matrix[i-1][j] == 'O' and matrix[i][j-1] == 'O':
                    count = count + 1
    return count


if __name__ == "__main__":
    matrix = [
        ['O', 'O', 'O'],
        ['X', 'X', 'O'],
        ['X', 'X', 'O'],
        ['O', 'O', 'X'],
        ['O', 'O', 'X'],
        ['X', 'X', 'O']
    ]
    print(count_number_of_islands(matrix))

    matrix1 = [
        ['X', 'O', 'O', 'O', 'O', 'O'],
        ['X', 'O', 'X', 'X', 'X', 'X'],
        ['O', 'O', 'O', 'O', 'O', 'O'],
        ['X', 'X', 'X', 'O', 'X', 'X'],
        ['X', 'X', 'X', 'O', 'X', 'X'],
        ['O', 'O', 'O', 'O', 'X', 'X'],
    ]
    print(count_number_of_islands(matrix1))
