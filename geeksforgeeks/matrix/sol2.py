
# Complete - Printing matrix in spiral order


def print_in_spiral_order(ref_matrix):
    top_index = 0
    left_index = 0
    right_index = len(ref_matrix[0]) - 1
    bottom_index = len(ref_matrix) - 1

    a = []
    while left_index <= right_index and top_index <= bottom_index:

        for i in range(left_index, right_index + 1):
            print(ref_matrix[top_index][i], end=' ')
            a.append(ref_matrix[top_index][i])

        for i in range(top_index + 1, bottom_index + 1):
            print(ref_matrix[i][right_index], end=' ')
            a.append(ref_matrix[i][right_index])

        if top_index < bottom_index:
            for i in range(right_index - 1, left_index - 1, -1):
                print(ref_matrix[bottom_index][i], end=' ')
                a.append(ref_matrix[bottom_index][i])

        for i in range(bottom_index - 1, top_index, -1):
            print(ref_matrix[i][left_index], end=' ')
            a.append(ref_matrix[i][left_index])

        left_index = left_index + 1
        right_index = right_index - 1
        top_index = top_index + 1
        bottom_index = bottom_index - 1


if __name__ == "__main__":
    matrix = [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 10, 11, 12],
        [13, 14, 15, 16]
    ]
    matrix_1 = [
        [1, 2, 3, 4, 5, 6],
        [7, 8, 9, 10, 11, 12],
        [13, 14, 15, 16, 17, 18]
    ]
    print("Printing matrix in spiral order")
    print_in_spiral_order(matrix_1)
