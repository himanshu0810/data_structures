
# Complete - A Boolean matrix question


def nullify_rows(matrix, row):
    for col in range(len(matrix[0])):
        matrix[row][col] = 1


def nullify_cols(matrix, col):
    for row in range(len(matrix)):
        matrix[row][col] = 1


def return_resultant_matrix(matrix):
    cols_to_nullify = set()
    for row in range(len(matrix)):
        row_to_nullify = False
        for col in range(len(matrix[0])):
            if matrix[row][col] == 1:
                cols_to_nullify.add(col)
                row_to_nullify = True

        if row_to_nullify:
            nullify_rows(matrix, row)

    for col in cols_to_nullify:
        nullify_cols(matrix, col)
    return


if __name__ == "__main__":
    matrix = [
        [1, 0],
        [0, 0]
    ]
    matrix = [
        [0, 0, 0],
        [0, 0, 1]
    ]
    matrix = [
        [1, 0, 0, 1],
        [0, 0, 1, 0],
        [0, 0, 0, 0]
    ]
    return_resultant_matrix(matrix)
    print("resultant matrix is ", matrix)
