
# Complete - Return the position of a number in sorted row-wise column-wise matrix


def return_position_in_matrix(ref_matrix, ref_number):
    row, col = 0, len(ref_matrix[0]) - 1
    max_row = len(ref_matrix)
    while row < max_row and col > -1:
        element = ref_matrix[row][col]
        if element == ref_number:
            return row, col

        if ref_number < element:
            col = col - 1
        else:
            row = row + 1
    return None


if __name__ == "__main__":
    matrix = [
        [10, 20, 30, 40],
        [15, 25, 35, 45],
        [27, 29, 37, 48],
        [32, 33, 39, 50]
    ]
    number = 50
    return_position = return_position_in_matrix(matrix, number)
    print("Position is ", return_position)
