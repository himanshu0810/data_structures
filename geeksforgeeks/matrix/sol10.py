
# Complete - Create a matrix with alternating rectangles of O and X

from pprint import pprint


def write_toggeled_zero_x(matrix):
    left_index = top_index = 0
    bottom_index = len(matrix) - 1
    right_index = len(matrix[0]) - 1

    ch = 'X'
    while left_index <= right_index and top_index <= bottom_index:

        for i in range(left_index, right_index + 1):
            matrix[top_index][i] = ch
            # print(matrix[top_index][i], end=' ')

        top_index = top_index + 1

        for i in range(top_index, bottom_index + 1):
            matrix[i][right_index] = ch
            # print(matrix[i][right_index], end=' ')

        right_index = right_index - 1

        for i in range(right_index, left_index-1, -1):
            matrix[bottom_index][i] = ch

            # print(matrix[bottom_index][right_index], end=' ')

        bottom_index = bottom_index - 1

        for i in range(bottom_index, top_index-1, -1):
            matrix[i][left_index] = ch
            # print(matrix[i][left_index], end=' ')

        left_index = left_index + 1
        if ch is 'X':
            ch = 'O'
        else:
            ch = 'X'


if __name__ == "__main__":
    m = 4
    n = 5
    matrix = [[1 for _ in range(0, n)] for _ in range(0, m)]
    write_toggeled_zero_x(matrix)
    pprint(matrix)
