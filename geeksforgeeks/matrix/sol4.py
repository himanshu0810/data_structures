
# Complete - Printing unique rows in matrix


def print_unique_rows_in_matrix(ref_matrix):
    hash_map = {}

    for row in range(len(ref_matrix)):
        string = ''
        hash = None
        for col in range(len(ref_matrix[0])):
            string = string + str(matrix[row][col])
            hash = int(string, 2)
        if hash not in hash_map:
            print(string)
            hash_map[hash] = row


if __name__ == "__main__":
    matrix = [
        [0, 1, 0, 0, 1],
        [1, 0, 1, 1, 0],
        [0, 1, 0, 0, 1],
        [1, 1, 1, 0, 0]
    ]
    print_unique_rows_in_matrix(matrix)
