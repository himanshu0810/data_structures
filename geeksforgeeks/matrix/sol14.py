
# Complete - Find a common element in all rows of a given row-wise sorted matrix


def return_common_element(matrix):
    cols = [len(matrix[0])-1 for _ in range(len(matrix))]
    for i in range(len(matrix[0])-1, -1, -1):
        common_element = matrix[0][i]
        for row in range(1, len(matrix)):
            col = cols[row]
            found = False
            while col > -1:
                el = matrix[row][col]
                if el == common_element:
                    cols[row] = col
                    found = True
                    break
                elif el > common_element:
                    cols[row] = col
                    col = col - 1
                    continue
                found = False
                break
            if found and (row == len(matrix) - 1):
                return common_element
            if not found:
                break

    return


if __name__ == "__main__":
    matrix = [
        [1, 2, 3, 4, 9],
        [2, 3, 5, 8, 10],
        [3, 5, 7, 9, 11],
        [1, 3, 5, 7, 9]
    ]
    print(return_common_element(matrix))
