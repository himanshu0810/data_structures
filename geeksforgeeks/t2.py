import six
from abc import ABCMeta


@six.add_metaclass(ABCMeta)
class Abstract_Coffee(object):

    def get_cost(self):
        pass

    def get_ingredients(self):
        pass

    def get_tax(self):
        return 0.1 * self.get_cost()


class Concrete_Coffee(Abstract_Coffee):

    def get_cost(self):
        return 1.00

    def get_ingredients(self):
        return 'coffee'


class Sugar(Concrete_Coffee):

    def __init__(self):
        Concrete_Coffee.__init__(self)

    def get_cost(self):
        return super(Sugar, self).get_cost()

    def get_ingredients(self):
        return super(Sugar, self).get_ingredients() + ', sugar'


class Milk(Concrete_Coffee):

    def __init__(self):
        Concrete_Coffee.__init__(self)

    def get_cost(self):
        return super(Milk, self).get_cost() + 0.25

    def get_ingredients(self):
        return super(Milk, self).get_ingredients() + ', milk'


class Vanilla(Concrete_Coffee):

    def __init__(self,):
        Concrete_Coffee.__init__(self)

    def get_cost(self):
        return super(Vanilla, self).get_cost() + 0.75

    def get_ingredients(self):
        return super(Vanilla, self).get_ingredients() + ', vanilla'


myCoffee = Concrete_Coffee()
print('Ingredients: ' + myCoffee.get_ingredients() +
      '; Cost: ' + str(myCoffee.get_cost()) + '; sales tax = ' + str(myCoffee.get_tax()))

myCoffee = Milk()
print('Ingredients: ' + myCoffee.get_ingredients() + '; Cost: ' +
      str(myCoffee.get_cost()) + '; sales tax = ' + str(myCoffee.get_tax()))

myCoffee = Vanilla()
print('Ingredients: ' + myCoffee.get_ingredients() + '; Cost: ' + str(myCoffee.get_cost()) +
      '; sales tax = ' + str(myCoffee.get_tax()))

myCoffee = Sugar()
print('Ingredients: ' + myCoffee.get_ingredients() + '; Cost: '
      + str(myCoffee.get_cost()) + '; sales tax = ' + str(myCoffee.get_tax()))
#