
class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class CircularLinkedList:
    def __init__(self):
        self.tail = None

    def insert(self, data):
        if not self.tail:
            node = Node(data)
            self.tail = node
            self.tail.next = self.tail
            return node

        node = Node(data)
        node.next = self.tail.next
        self.tail.next = node
        return self.tail

    def traverse_list(self):
        if not self.tail:
            return

        temp = self.tail
        while True:
            temp = temp.next
            print(temp.data, end=' ')
            if temp == self.tail:
                break


if __name__ == "__main__":
    circular_linked_list = CircularLinkedList()
    circular_linked_list.insert(11)
    circular_linked_list.insert(2)
    circular_linked_list.insert(15)
    circular_linked_list.insert(50)
    circular_linked_list.traverse_list()
