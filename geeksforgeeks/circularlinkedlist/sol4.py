
class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class CircularLinkedList:
    def __init__(self):
        self.tail = None

    def insert(self, data):
        if not self.tail:
            node = Node(data)
            self.tail = node
            self.tail.next = self.tail
            return node

        node = Node(data)
        node.next = self.tail.next
        self.tail.next = node
        return self.tail

    def traverse_list(self):
        if not self.tail:
            return

        temp = self.tail
        while True:
            temp = temp.next
            print(temp.data, end=' ')
            if temp == self.tail:
                break

    def split_list_into_two_halves(self):
        head = mid = self.tail.next
        while True:
            if mid.next != self.tail:
                mid = mid.next
            else:
                break
            if mid.next != self.tail:
                mid = mid.next
            else:
                break
            if head.next != self.tail:
                head = head.next
            else:
                break
        temp = self.tail
        self.tail.next = mid
        return mid, temp.next



if __name__ == "__main__":
    circular_linked_list = CircularLinkedList()
    circular_linked_list.insert(4)
    circular_linked_list.insert(3)
    circular_linked_list.insert(2)
    circular_linked_list.insert(1)
    circular_linked_list.traverse_list()
    circular_linked_list.split_list_into_two_halves()
    print("-")
    circular_linked_list.traverse_list()

