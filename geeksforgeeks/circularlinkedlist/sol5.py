
# Complete - Insert data in circular linked list in all the conditions


class Node:
    def __init__(self, data):
        self.data = data
        self.next = self


class CircularLinkedList:
    def __init__(self):
        self.head = None

    def print(self):
        node = self.head
        print(node.data, end=' ')
        node = node.next
        while node is not self.head:
            print(node.data, end=' ')
            node = node.next
        return

    def insert(self, data):
        node = Node(data)
        if not self.head:
            self.head = node

        elif self.head.next is self.head:
            if self.head.data > data:
                self.head = node
            self.head.next = node
            node.next = self.head

        else:
            prev = None
            head = self.head
            while head.data < data and head.next is not self.head:
                prev = head
                head = head.next

            next = head
            if head.next is self.head:
                prev = head
                next = self.head
            elif prev is None and next is self.head:
                head = self.head
                while head.next is not self.head:
                    head = head.next
                prev = head
                self.head = node

            node.next = next
            prev.next = node
        return node


if __name__ == "__main__":
    cl = CircularLinkedList()
    cl.insert(12)
    cl.insert(56)
    cl.insert(2)
    cl.insert(11)
    cl.insert(1)
    cl.insert(90)

    cl.print()
