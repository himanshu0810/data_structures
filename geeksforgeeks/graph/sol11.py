
# Complete - Bipartite graph for connected and non-connected graph


from collections import defaultdict


class Graph:
    def __init__(self, V):
        self.V = V
        self.graph = defaultdict(list)
        self.vertices = set()

    def add_edge(self, src, dst):
        self.graph[src].append(dst)
        self.graph[dst].append(src)
        self.vertices.add(src)
        self.vertices.add(dst)
        return

    def return_vertices(self):
        return self.vertices

    def print_graph(self):
        for v in self.vertices:
            print(v, "->", self.graph[v])

    def is_bipartite_util(self, v):
        color_arr = [-1] * self.V

        color_arr[v] = 1
        queue = [v]

        while queue:

            v1 = queue.pop(0)

            vertices = self.graph[v1]
            for each in vertices:
                if color_arr[each] == color_arr[v1]:
                    return False

                if color_arr[each] == -1:
                    color_arr[each] = 1 - color_arr[v1]
                    queue.append(each)

        return True

    def is_bipartite(self):
        color_arr = [-1] * self.V
        for v in self.vertices:
            if color_arr[v] == -1:
                if not self.is_bipartite_util(v):
                    return False
        return True


if __name__ == "__main__":
    gp = Graph(6)
    gp.add_edge(0, 1)
    gp.add_edge(1, 2)
    gp.add_edge(2, 3)
    gp.add_edge(3, 4)
    gp.add_edge(4, 5)
    gp.add_edge(5, 0)

    gp.print_graph()
    print(gp.is_bipartite())
