

# Complete - Topological Sort using DFS


class Vertex:
    def __init__(self, data):
        self.data = data
        self.connections = {}

    def get_connections(self):
        return self.connections.keys()

    def add_connection(self, to, weight=0):
        self.connections[to] = weight

    def get_weight(self, to):
        return self.connections[to]

    def get_id(self):
        return self.data

    def __str__(self):
        return str(self.data)   # + "-connected to-" + str([x.data for x in self.connections])


class Graph:
    def __init__(self):
        self.vertices = {}
        self.num_vertices = 0

    def add_vertex(self, data):
        self.num_vertices = self.num_vertices + 1
        vertex = Vertex(data)
        self.vertices[data] = vertex
        return vertex

    def get_vertex(self, data):
        return self.vertices[data]

    def __contains__(self, n):
        return n in self.vertices

    def get_vertices(self):
        return self.vertices.keys()

    def add_edge(self, frm, to):
        if frm not in self.vertices:
            self.vertices[frm] = self.add_vertex(frm)

        if to not in self.vertices:
            self.vertices[to] = self.add_vertex(to)

        self.vertices[frm].add_connection(self.vertices[to])

    def __iter__(self):
        return iter(self.vertices.values())

    def print_graph(self):
        for v in self:
            for w in v.get_connections():
                print("(%s, %s)" % (v.get_id(), w.get_id()))

    def topological_sort_util(self, vertex, visited, sorted):
        visited.add(vertex.get_id())
        for w in vertex.get_connections():
            if w.get_id() in visited:
                continue

            self.topological_sort_util(w, visited, sorted)

        sorted.append(vertex.get_id())

    def topological_sort(self):
        visited = set()
        sorted = []
        for vertex, vertex_obj in self.vertices.items():
            if vertex in visited:
                continue
            self.topological_sort_util(vertex_obj, visited, sorted)
        print("sorted stack is ", sorted)


if __name__ == "__main__":
    graph = Graph()
    graph.add_edge(1, 3)
    graph.add_edge(2, 3)
    graph.add_edge(2, 4)
    graph.add_edge(3, 5)
    graph.add_edge(4, 6)
    graph.add_edge(5, 6)
    graph.add_edge(5, 7)
    graph.add_edge(6, 8)

    graph.print_graph()

    print("printing topological sort is")
    graph.topological_sort()
