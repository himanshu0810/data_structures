
# Complete - Detect Cycle in a Directed Graph


class Vertex:
    def __init__(self, data):
        self.data = data
        self.connections = {}

    def get_connections(self):
        return self.connections.keys()

    def add_connection(self, to, weight=0):
        self.connections[to] = weight

    def get_weight(self, to):
        return self.connections[to]

    def get_id(self):
        return self.data

    def __str__(self):
        return str(self.data)   # + "-connected to-" + str([x.data for x in self.connections])


class Graph:
    def __init__(self):
        self.vertices = {}
        self.num_vertices = 0

    def add_vertex(self, data):
        self.num_vertices = self.num_vertices + 1
        vertex = Vertex(data)
        self.vertices[data] = vertex
        return vertex

    def get_vertex(self, data):
        return self.vertices[data]

    def __contains__(self, n):
        return n in self.vertices

    def get_vertices(self):
        return self.vertices.keys()

    def add_edge(self, frm, to):
        if frm not in self.vertices:
            self.vertices[frm] = self.add_vertex(frm)

        if to not in self.vertices:
            self.vertices[to] = self.add_vertex(to)

        self.vertices[frm].add_connection(self.vertices[to])

    def __iter__(self):
        return iter(self.vertices.values())

    def print_graph(self):
        for v in self:
            for w in v.get_connections():
                print("(%s, %s)" % (v.get_id(), w.get_id()))

    def depth_first_traversal(self, root):
        if root is None:
            return

        root.is_visited = True

        for w in root.get_connections():
            if hasattr(w, 'is_visited'):
                continue

            w.is_visited = True
            self.depth_first_traversal(w)

    def detect_cycle_util(self, root, visited, recursion_stack):
        recursion_stack.add(root.data)
        visited.add(root.get_id())
        for w in root.get_connections():
            if not w.get_id() in visited:
                if self.detect_cycle_util(w, visited, recursion_stack) is True:
                    return True
            elif w.get_id() in recursion_stack:
                return True

        recursion_stack.remove(root.data)
        return recursion_stack, False

    def detect_cycle_with_dfs(self):
        visited = set()
        recursion_stack = set()
        for v in self.vertices:
            if v not in visited:
                flag = self.detect_cycle_util(self.vertices[v], visited, recursion_stack)
                if flag:
                    return flag
        return False


if __name__ == "__main__":
    graph = Graph()
    graph.add_edge(1, 7)
    graph.add_edge(2, 6)
    graph.add_edge(3, 1)
    graph.add_edge(3, 5)
    graph.add_edge(4, 6)
    graph.add_edge(5, 2)
    graph.add_edge(5, 4)
    graph.add_edge(6, 7)
    graph.add_edge(6, 8)
    graph.add_edge(7, 2)
    graph.add_edge(7, 8)

    # graph.print_graph()
    print("Printing depth first traversal of a graph - ")
    graph.depth_first_traversal(graph.vertices[2])
    print()

    print("Find whether there is a cycle or not in a graph ")
    is_cycle_present = graph.detect_cycle_with_dfs()
    print("Cycle present -", is_cycle_present)
