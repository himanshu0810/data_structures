
# Complete - Depth first traversal in a graph - recursively and iteratively


class Vertex:
    def __init__(self, data):
        self.data = data
        self.connections = {}

    def get_connections(self):
        return self.connections.keys()

    def add_connection(self, to, weight=0):
        self.connections[to] = weight

    def get_weight(self, to):
        return self.connections[to]

    def get_id(self):
        return self.data

    def __str__(self):
        return str(self.data)   # + "-connected to-" + str([x.data for x in self.connections])


class Graph:
    def __init__(self):
        self.vertices = {}
        self.num_vertices = 0

    def add_vertex(self, data):
        self.num_vertices = self.num_vertices + 1
        vertex = Vertex(data)
        self.vertices[data] = vertex
        return vertex

    def get_vertex(self, data):
        return self.vertices[data]

    def __contains__(self, n):
        return n in self.vertices

    def get_vertices(self):
        return self.vertices.keys()

    def add_edge(self, frm, to):
        if frm not in self.vertices:
            self.vertices[frm] = self.add_vertex(frm)

        if to not in self.vertices:
            self.vertices[to] = self.add_vertex(to)

        self.vertices[frm].add_connection(self.vertices[to])

    def __iter__(self):
        return iter(self.vertices.values())

    def print_graph(self):
        for v in self:
            for w in v.get_connections():
                print("(%s, %s)" % (v.get_id(), w.get_id()))

    def depth_first_traversal(self, root):
        print(root.get_id(), end=' ')
        root.is_visited = True

        for w in root.get_connections():
            if hasattr(w, 'is_visited'):
                continue

            self.depth_first_traversal(w)

    def depth_first_traversal_iteratively(self, root):

        st = [root]
        root.is_visited = True

        while st:
            node = st[-1]
            st.pop()
            print(node.data, end=' ')

            for w in node.get_connections():
                if hasattr(w, 'is_visited'):
                    continue

                w.is_visited = True
                st.append(w)


if __name__ == "__main__":
    graph = Graph()
    graph.add_edge(0, 1)
    graph.add_edge(0, 2)
    graph.add_edge(1, 2)
    graph.add_edge(2, 0)
    graph.add_edge(2, 3)
    graph.add_edge(3, 3)

    graph.print_graph()

    print("Printing depth first traversal of a graph - ")
    graph.depth_first_traversal(graph.vertices[2])

    print("Printing depth first traversal of a graph iteratively- ")
    graph.depth_first_traversal_iteratively(graph.vertices[2])
