
# Create a graph using linked list


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class Graph:
    def __init__(self, vertices):
        self.V = vertices
        self.graph = [None] * self.V

    def add_edge(self, frm, to):
        node = Node(to)
        node.next = self.graph[frm]
        self.graph[frm] = node

        node = Node(frm)
        node.next = self.graph[to]
        self.graph[to] = node

    def print_graph(self):
        for i in range(self.V):
            node = self.graph[i]
            while node:
                print(node.data, end=' ')
                node = node.next
            print()


if __name__ == "__main__":
    V = 5
    graph = Graph(V)
    graph.add_edge(0, 1)
    graph.add_edge(0, 4)
    graph.add_edge(1, 2)
    graph.add_edge(1, 3)
    graph.add_edge(1, 4)
    graph.add_edge(2, 3)
    graph.add_edge(3, 4)

    graph.print_graph()
