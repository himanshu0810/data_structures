
# Complete - Find Cycle in an undirected graph using disjoint sets


class Vertex:
    def __init__(self, data):
        self.data = data
        self.connections = {}

    def get_connections(self):
        return self.connections.keys()

    def add_connection(self, to, weight=0):
        self.connections[to] = weight

    def get_weight(self, to):
        return self.connections[to]

    def get_id(self):
        return self.data

    def __str__(self):
        return str(self.data)   # + "-connected to-" + str([x.data for x in self.connections])


class Graph:
    def __init__(self):
        self.vertices = {}
        self.num_vertices = 0

    def add_vertex(self, data):
        self.num_vertices = self.num_vertices + 1
        vertex = Vertex(data)
        self.vertices[data] = vertex
        return vertex

    def get_vertex(self, data):
        return self.vertices[data]

    def __contains__(self, n):
        return n in self.vertices

    def get_vertices(self):
        return self.vertices.keys()

    def add_edge(self, frm, to):
        if frm not in self.vertices:
            self.vertices[frm] = self.add_vertex(frm)

        if to not in self.vertices:
            self.vertices[to] = self.add_vertex(to)

        self.vertices[frm].add_connection(self.vertices[to])
        self.vertices[to].add_connection(self.vertices[frm])

    def __iter__(self):
        return iter(self.vertices.values())

    def print_graph(self):
        for v in self:
            for w in v.get_connections():
                print("(%s, %s)" % (v.get_id(), w.get_id()))

    def find_set(self, sets_dict, vertex):
        if vertex in sets_dict[vertex]:
            return vertex, sets_dict[vertex]

        for v_id, vertex_obj in sets_dict.items():
            if vertex in vertex_obj:
                return v_id, vertex_obj
        return

    def union_sets(self, sets_dict, v_id, vertex_set, c_id, connection_set):
        union_set = vertex_set | connection_set
        sets_dict[v_id] = union_set
        sets_dict[c_id] = set()
        return sets_dict, union_set

    def disjoint_sets(self):
        sets_dict = {}
        for vertex, vertex_obj in self.vertices.items():
           sets_dict[vertex] = {vertex}

        for vertex, vertex_obj in self.vertices.items():

            v_id, vertex_set = self.find_set(sets_dict, vertex)
            for w in vertex_obj.get_connections():

                c_id, connection_set = self.find_set(sets_dict, w.get_id())

                if vertex_set and vertex_set == connection_set:
                    return True

                sets_dict, vertex_set = self.union_sets(sets_dict, v_id, vertex_set, c_id, connection_set)

        return False


if __name__ == "__main__":
    graph = Graph()
    graph.add_edge(1, 2)
    graph.add_edge(1, 6)
    graph.add_edge(2, 3)
    graph.add_edge(2, 5)
    # graph.add_edge(3, 4)
    graph.add_edge(4, 5)

    graph.print_graph()

    print("Finding cycle with disjoint sets through disjoint sets - ", graph.disjoint_sets())
