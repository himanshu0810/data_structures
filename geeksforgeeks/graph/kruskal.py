

class Vertex:
    def __init__(self, vertex_id):
        self.id = vertex_id
        self.connections = {}

    def get_connections(self):
        return self.connections.keys()

    def add_connection(self, to, weight=0):
        self.connections[to] = weight
        return

    def __str__(self):
        return self.id


class Graph:
    def __init__(self):
        self.graph = {}
        self.num_vertices = 0

    def get_or_add_vertex(self, id1):
        if id1 in self.get_vertices():
            return self.graph[id1]
        vertex = self.add_vertex(id1)
        return vertex

    def get_vertices(self):
        return self.graph.keys()

    def add_vertex(self, id1):
        self.num_vertices = self.num_vertices + 1
        vertex = Vertex(id1)
        self.graph[id1] = vertex
        return vertex

    def add_connection(self, frm, to, weight):
        frm_vertex = self.get_or_add_vertex(frm)
        to_vertex = self.get_or_add_vertex(to)
        frm_vertex.add_connection(to_vertex, weight)
        to_vertex.add_connection(frm_vertex, weight)
        return

    def get_connections(self, frm):
        frm_vertex = self.get_or_add_vertex(frm)
        return frm_vertex.get_connections()

    def print_graph(self):
        vertices = self.get_vertices()
        for vertex in vertices:
            vertex_obj = self.get_or_add_vertex(vertex)
            print(str(vertex) + "-", end=' ')
            for each in vertex_obj.get_connections():
                print(each.id, end=' ')
            print()


if __name__ == "__main__":
    graph = Graph()
    graph.add_connection(1, 2, 5)
    graph.add_connection(1, 3, 3)
    graph.add_connection(2, 4, 1)
    graph.add_connection(2, 3, 2)
    graph.add_connection(3, 4, 0)
    graph.add_connection(4, 5, 2)
    graph.print_graph()

