
# Complete - breadth first search in a graph


class Vertex:
    def __init__(self, data):
        self.data = data
        self.connections = {}

    def get_connections(self):
        return self.connections.keys()

    def add_connection(self, to, weight=0):
        self.connections[to] = weight

    def get_weight(self, to):
        return self.connections[to]

    def get_id(self):
        return self.data

    def __str__(self):
        return str(self.data)   # + "-connected to-" + str([x.data for x in self.connections])


class Graph:
    def __init__(self):
        self.vertices = {}
        self.num_vertices = 0

    def add_vertex(self, data):
        self.num_vertices = self.num_vertices + 1
        vertex = Vertex(data)
        self.vertices[data] = vertex
        return vertex

    def get_vertex(self, data):
        return self.vertices[data]

    def __contains__(self, n):
        return n in self.vertices

    def get_vertices(self):
        return self.vertices.keys()

    def add_edge(self, frm, to):
        if frm not in self.vertices:
            self.vertices[frm] = self.add_vertex(frm)

        if to not in self.vertices:
            self.vertices[to] = self.add_vertex(to)

        self.vertices[frm].add_connection(self.vertices[to])
        self.vertices[to].add_connection(self.vertices[frm])

    def __iter__(self):
        return iter(self.vertices.values())

    def print_graph(self):
        for v in self:
            for w in v.get_connections():
                print("(%s, %s)" % (v.get_id(), w.get_id()))

    def breadth_first_search(self, root_key):

        root_vertex = self.vertices[root_key]
        root_vertex.is_visited = True
        current_level = [self.vertices[root_key]]

        while current_level:
            vertex = current_level.pop(0)

            for w in vertex.get_connections():
                if hasattr(w, 'is_visited'):
                    continue

                w.is_visited = True
                current_level.append(w)

            print(vertex.get_id(), end=' ')
        print()

    def breadth_first_search_1(self, root_key):

        current_level = [self.vertices[root_key]]
        visited = [root_key]

        while current_level:
            vertex = current_level.pop(0)

            for w in vertex.get_connections():
                if w.get_id() in visited:
                    continue

                visited.append(w.get_id())
                current_level.append(w)

            print(vertex.get_id(), end=' ')
        print()


if __name__ == "__main__":
    graph = Graph()
    graph.add_edge(1, 2)
    graph.add_edge(1, 3)
    graph.add_edge(2, 4)
    graph.add_edge(2, 5)
    graph.add_edge(3, 5)
    graph.add_edge(4, 5)
    graph.add_edge(4, 6)
    graph.add_edge(5, 6)

    graph.print_graph()
    print("Printing breadth first search traversal of a tree - ")
    graph.breadth_first_search(1)
    print("Printing breadth first search traversal of a tree by another method -")
    graph.breadth_first_search_1(1)
