
# Completed - Print a binary tree in vertical order


from collections import defaultdict
from pprint import pprint


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


def inorder(root):
    if root:
        inorder(root.left)
        print(root.data, end=' ')
        inorder(root.right)


def return_vertical_order_util(vertical_map, root, position):
    if not root:
        return vertical_map

    vertical_map[position].append(root.data)
    if root.left:
        return_vertical_order_util(vertical_map, root.left, position - 1)
    if root.right:
        return_vertical_order_util(vertical_map, root.right, position + 1)
    return vertical_map


def return_vertical_order(root):
    vertical_map = defaultdict(list)
    return return_vertical_order_util(vertical_map, root, 0)


if __name__ == "__main__":
    root1 = Node(1)
    root1.left = Node(2)
    root1.right = Node(3)

    root1.left.left = Node(4)
    root1.left.right = Node(5)

    root1.right.left = Node(6)
    root1.right.right = Node(7)

    root1.right.left.right = Node(8)
    root1.right.right.right = Node(9)

    inorder(root1)
    print()
    vertical_mapping = return_vertical_order(root1)
    print("Mapping of vertical position of nodes are - ")
    pprint(vertical_mapping)
