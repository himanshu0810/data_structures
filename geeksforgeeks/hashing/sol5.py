
# Find whether the array is subset of another array
# TODO in case of duplicacy


def find_subset_in_main_array(main_array_1, subset_1):
    hash_map = {}
    for each in main_array_1:
        hash_map[each] = each
    for each in subset_1:
        if hash_map.get(each) is not each:
            return False
    return True


if __name__ == "__main__":
    subset = [11, 3, 7, 1, 44]
    main_array = [11, 1, 13, 21, 3, 7]
    print("Subset is present in main array - ", find_subset_in_main_array(main_array, subset))
