
# Completed - Find Itinerary from a given list of tickets


def return_itinerary_from_tickets(tickets):
    reverse_map = {}
    for start, end in tickets.items():
        reverse_map[end] = start

    for k, v in tickets.items():
        if k in reverse_map:
            continue
        return k
    return


if __name__ == "__main__":
    tickets = {
        "Chennai": "Banglore",
        "Bombay": "Delhi",
        "Goa": "Chennai",
        "Delhi": "Goa",
    }
    starting_point = return_itinerary_from_tickets(tickets)

    while starting_point in tickets:
        dest = tickets[starting_point]
        print(starting_point, "=>", dest, end='|')
        starting_point = dest
