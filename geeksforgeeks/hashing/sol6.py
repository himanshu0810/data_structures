
# Completed - Union and Intersection of two Linked Lists


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:
    def __init__(self):
        self.head = None

    def insert(self, data):
        if not self.head:
            self.head = Node(data)
            return self.head

        node = self.head
        while node.next:
            node = node.next
        node.next = Node(data)
        return

    def print(self):
        if not self.head:
            return
        node = self.head
        while node:
            print(node.data, end=' ')
            node = node.next
        print()


def create_intersection_two_linked_lists(map, ll1, ll2):

    ll3 = LinkedList()
    node_1, node_2 = ll1.head, ll2.head

    while node_1 is not None:
        map[node_1.data] = node_1
        node_1 = node_1.next

    prev = None
    while node_2 is not None:
        if node_2.data in map:
            node = Node(node_2.data)
            if ll3.head is None:
                ll3.head = node
                prev = node
            else:
                prev.next = node
                prev = node

        map[node_2.data] = node_2
        node_2 = node_2.next
    return ll3


def create_union_two_linked_lists(map):
    ll4 = LinkedList()
    prev = None
    for key in map:
        node = Node(key)
        if ll4.head is None:
            ll4.head = node
        else:
            prev.next = node
        prev = node
    return ll4


if __name__ == "__main__":
    ll1 = LinkedList()
    ll1.insert(10)
    ll1.insert(15)
    ll1.insert(4)
    ll1.insert(20)

    ll2 = LinkedList()
    ll2.insert(8)
    ll2.insert(4)
    ll2.insert(2)
    ll2.insert(10)

    map = {}
    ll3 = create_intersection_two_linked_lists(map, ll1, ll2)
    ll3.print()

    ll4 = create_union_two_linked_lists(map)
    ll4.print()
