
# Completed - Finding employees under each manager and total count


from collections import defaultdict
from pprint import pprint


def return_count_and_result_util(count, result, value):
    count = count + 1

    if value not in result:
        result[value] = 1
        return count

    result[value] = result[value] + 1
    return count


# def return_count_and_result(emps):
#     count = 0
#     result = {}
#     for key, value in emps.items():
#         if key != value:
#             count = return_count_and_result_util(count, result, value)
#     return count, result


def return_count_and_result(emps):
    count = 0
    result = defaultdict(list)
    for key, value in emps.items():
        if key == value:
            continue

        count = count + 1
        val = result.get(value)
        if val is None:
            result[value] = [key]
            continue

        result[value].append(key)
    return count, result


if __name__ == "__main__":
    emps = {
        "A": "C",
        "B": "C",
        "C": "F",
        "D": "E",
        "E": "F",
        "F": "F"
    }

    count, result = return_count_and_result(emps)
    print("Count is", count)
    pprint(result)
