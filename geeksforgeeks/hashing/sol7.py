
# Completed - Check for pair in A[] with sum as x


def return_pair(array, n):
    map = {}
    for idx, i in enumerate(array):
        if i in map:
            return map[i], i

        map[n - i] = i
    return False


if __name__ == "__main__":
    array = [1, 4, 45, 6, 10, 8]
    n = 41
    pair = return_pair(array, n)
    print("Pair is ", pair)
