
# Completed - duplicate elements within k distance from each other


def return_check_for_dup(array, k):
    map = {}

    for idx, element in enumerate(array):
        if element not in map:
            map[element] = idx
            continue

        prev_idx = map[element]

        if idx > prev_idx + k:
            map[element] = idx
            continue

        return False

    return True


if __name__ == "__main__":
    array = [1, 2, 3, 4, 4]
    k = 3
    check_for_dup = return_check_for_dup(array, k)
    print("All duplicates are more than k distance away", check_for_dup)
