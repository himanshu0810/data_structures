import sys

# TODO will print the path later on and recursively


def return_min_cost(costs, a, b):
    paths = [[sys.maxsize] * (a + 1 + 1) for _ in range(b + 1 + 1)]
    paths[0][0] = 0

    for row_id in range(1, a+2):
        for col_id in range(1, b+2):
            paths[row_id][col_id] = min(paths[row_id-1][col_id], paths[row_id-1][col_id-1],
                                        paths[row_id][col_id-1]
                                        ) + costs[row_id - 1][col_id - 1]

    return paths[a+1][b+1]


if __name__ == "__main__":
    matrix = [
        [1, 2, 3],
        [4, 8, 2],
        [1, 5, 3]
    ]
    print("min cost is ", return_min_cost(matrix, 1, 1))
