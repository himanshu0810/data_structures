
# TODO will do recursively and the no of paths drawn and solve using one array


def return_total_ways(N, array):

    ways = [[0 for _ in range(N+1)] for _ in range(len(array)+1)]
    for i in range(1, len(ways)):
        element = array[i-1]
        for j in range(1, len(ways[i])):
            if j < element:
                ways[i][j] = ways[i-1][j]
            elif element - j == 0:
                ways[i][j] = ways[i-1][j] + 1
            else:
                ways[i][j] = ways[i - 1][j] + ways[i][j-element]

    return ways[i][j]


if __name__ == "__main__":
    array = [1, 2, 3]
    N = 4
    min_cost = return_total_ways(N, array)
    print("min cost is ", min_cost)
