
# Dynamic Programming using Longest Increasing Subsequence
# TODO print the numbers contributing in longest increasing subsequence AND recusively
# TODO nlogn complexity



def return_longest_common_subsequence(input_list):

    subsequence_list = [1] * len(input_list)

    for idx, number in enumerate(input_list):

        for prev in range(0, idx):
            if number > input_list[prev] and subsequence_list[prev] >= \
                    subsequence_list[idx]:
                subsequence_list[idx] = subsequence_list[prev] + 1

    return max(subsequence_list), subsequence_list


if __name__ == "__main__":
    input_list = [10, 22, 9, 33, 21, 50, 41, 60, 80]
    length, longest_common_subsequence_list = return_longest_common_subsequence(input_list)
    print("Length and longest common subsequence is ", length, longest_common_subsequence_list)
