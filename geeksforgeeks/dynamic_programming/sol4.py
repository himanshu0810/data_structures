
# Longest common subsequence between two sequences
# TODO print the sequence and recursively

from pprint import pprint

import copy


def return_longest_common_subsequence(list1, list2):
    matrix = [[0] * (len(list2) + 1) for _ in range(len(list1)+1)]

    for i in range(1, len(matrix)):
        for j in range(1, len(matrix[i])):
            if list1[i-1] == list2[j-1]:
                matrix[i][j] = matrix[i-1][j-1] + 1
            else:
                matrix[i][j] = max(matrix[i-1][j], matrix[i][j-1])

    return matrix[i][j]

if __name__ == "__main__":
    list1 = "AGGTAB"
    list2 = "GXTXAYB"
    max_common_subsequence = return_longest_common_subsequence(list1, list2)
    print("longest common subsequence is ", max_common_subsequence)


