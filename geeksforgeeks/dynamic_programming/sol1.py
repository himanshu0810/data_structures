
# Fibonacci number using Tabulation and Memoization method


def fibonacci_number_memo(lookup, number):

    if number == 0:
        return 0
    elif number == 1:
        return 1

    if lookup.get(number) is None:
        lookup[number] = fibonacci_number_memo(lookup, number - 1) + fibonacci_number_memo(lookup, number - 2)

    return lookup[number]


def fibonacci_number_tabulation(number):
    f = [0] * (number + 1)
    f[1] = 1

    for i in range(2, number+1):
        f[i] = f[i-1] + f[i-2]
    return f[number]


if __name__ == "__main__":
    lookup = {}
    result = fibonacci_number_memo(lookup, 4)
    result1 = fibonacci_number_memo(lookup, 5)
    print("Result of finonacci number is - ", result, lookup)
    result2 = fibonacci_number_tabulation(6)
    print("Result of fibonacci number from tabulation method is ", result2)
