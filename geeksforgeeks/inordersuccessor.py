
class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.data)

class Tree:
    def __init__(self):
        self.root = None

    def inorder(self, node):
        if node:
            self.inorder(node.left)
            print(node.data, end=' ')
            self.inorder(node.right)

    def inorder_successor(self, key):
        node = self.root

        node_root = None
        while node.data != key:
            if key < node.data:
                node_root = node
                node = node.left
            else:
                node = node.right

        if node.right is None:
            if node_root is None:
                return node_root

            return node_root.data

        node_right = node.right
        while node_right.left is not None:
            node_right = node_right.left
        return node_right.data


if __name__ == "__main__":
    tree = Tree()

    tree.root = Node(20)

    tree.root.left = Node(8)
    tree.root.right = Node(22)

    tree.root.left.left = Node(4)
    tree.root.left.right = Node(12)

    tree.root.left.right.left = Node(10)
    tree.root.left.right.right = Node(14)

    tree.root.right.left = Node(21)

    tree.inorder(tree.root)
    print()

    print("Inorder successor of node is 4 ", tree.inorder_successor(4))
    print("Inorder successor of node is 8 ", tree.inorder_successor(8))
    print("Inorder successor of node is 10 ", tree.inorder_successor(10))
    print("Inorder successor of node is 12 ", tree.inorder_successor(12))
    print("Inorder successor of node is 14 ", tree.inorder_successor(14))
    print("Inorder successor of node is 20 ", tree.inorder_successor(20))
    print("Inorder successor of node is 21 ", tree.inorder_successor(21))
    print("Inorder successor of node is 22 ", tree.inorder_successor(22))
