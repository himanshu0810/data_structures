
# Complete - Longest prefix matching


from geeksforgeeks.trie.sol1 import Trie

if __name__ == "__main__":
    trie = Trie()
    ary = ["are", "area", "base", "cat", "cater", "children", "basement"]
    for each in ary:
        trie.insert(each)

    trie.print_all_strings()

    longest_prefix = trie.return_longest_prefix("chif")
    print("Longest prefix for the given key is ", longest_prefix)
