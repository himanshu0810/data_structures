
# Complete - Print unique rows in trie


from pprint import pprint


class Node:
    def __init__(self):
        self.children = {}
        self.is_end_of_word = False


class Trie:
    def __init__(self):
        self.root = None

    def insert(self, mrow):

        curr_node = None
        for number in mrow:
            if self.root is None:
                self.root = Node()
                curr_node = self.root
            elif curr_node is None:
                curr_node = self.root

            if number not in curr_node.children.keys():
                curr_node.children[number] = Node()

            curr_node = curr_node.children[number]

        curr_node.is_end_of_word = True
        return self.root

    def return_all_strings_util(self, node, strings, string):
        if node is None:
            return strings

        if node.is_end_of_word:
            strings.append(string)

        for key, child in node.children.items():
            string_1 = string + str(key)
            self.return_all_strings_util(child, strings, string_1)

        return strings

    def return_all_strings(self):
        strings = []
        self.return_all_strings_util(trie.root, strings, "")
        pprint(strings)


if __name__ == "__main__":
    trie = Trie()
    matrix = [
            [0, 1, 0, 0, 1],
            [1, 0, 1, 1, 0],
            [0, 1, 0, 0, 1],
            [1, 1, 1, 0, 0]
        ]

    for row in matrix:
        trie.insert(row)

    trie.return_all_strings()
