
# Complete - Trie insert and search, delete and print


class Node:
    def __init__(self):
        self.children = {}
        self.is_end_of_word = False

    def __repr__(self):
        return str(self.is_end_of_word)


class Trie:
    def __init__(self):
        self.head = None

    def insert(self, key):

        node = None
        for idx, ch in enumerate(key):
            if idx == 0:
                if not self.head:
                    self.head = Node()
                node = self.head

            if ch not in node.children.keys():
                new_node = Node()
                node.children[ch] = new_node
            node = node.children[ch]

        node.is_end_of_word = True
        return

    def delete(self, node, key, index):
        if index == len(key):
            if not node.children:
                return True
            node.is_end_of_word = False
            return

        new_node = node.children[key[index]]

        if self.delete(new_node, key, index+1):
            node.children.pop(key[index])

        if not node.children:
            return True
        return

    def print_all_strings(self):
        paths = []
        self.print_all_strings_util(self.head, paths, "")
        print("All strings present in trie structure are ")
        print(paths)

    def print_all_strings_util(self, node, paths, string):
        if node.is_end_of_word:
            paths.append(string)

        if not node.children:
            return paths

        children = node.children
        for key, node in children.items():
            string_1 = string + key
            self.print_all_strings_util(node, paths, string_1)

        return paths

    def return_longest_prefix(self, key):
        node = self.head
        return self.return_longest_prefix_util(node, key, 0, "")

    def return_longest_prefix_util(self, node, key, index, result):
        if index == len(key):
            if node.is_end_of_word:
                return result
            return ""

        ch = key[index]
        if ch in node.children.keys():
            return self.return_longest_prefix_util(node.children[ch], key, index+1, result + ch)
        return result

    def search(self, key):
        if not self.head:
            return False

        node = self.head
        for ch in key:
            if ch not in node.children.keys():
                return False
            node = node.children[ch]
        return node.is_end_of_word


if __name__ == "__main__":
    trie = Trie()
    trie.insert("the")
    trie.insert("a")
    trie.insert("there")
    trie.insert("anaswe")
    trie.insert("any")
    trie.insert("by")
    trie.insert("their")

    print("the", trie.search("the"))
    print("a", trie.search("a"))
    print("there", trie.search("there"))
    print("anaswe", trie.search("anaswe"))
    print("any", trie.search("any"))
    print("any1", trie.search("any1"))
    print("a", trie.search("a"))
    print("b", trie.search("b"))
    print("avd", trie.search("avd"))
    print("at", trie.search("at"))
    print("there3", trie.search("there3"))

    trie.print_all_strings()
    trie.delete(trie.head, "the", 0)
    trie.print_all_strings()
