
# Complete - Forward DNS lookup cache


class TrieNode:
    def __init__(self):
        self.children = {}
        self.is_end_of_word = False
        self.domain = None


class Trie:
    def __init__(self):
        self.head = None

    def insert(self, key, ipaddr):

        if key[:4] == "www.":
            key = key[4:]

        current_node = None
        for each in key:
            if self.head is None:
                self.head = TrieNode()
                current_node = self.head
            elif current_node is None:
                current_node = self.head

            if each not in current_node.children.keys():
                current_node.children[each] = TrieNode()

            current_node = current_node.children[each]

        current_node.is_end_of_word = True
        current_node.domain = ipaddr
        return

    def print_trie_util(self, node, strings, string):
        if node is None:
            return strings

        if node.is_end_of_word:
            strings.append(string)

        for key, each in node.children.items():
            string_1 = string + key
            self.print_trie_util(each, strings, string_1)

        return strings

    def print_trie(self):
        strings = []
        self.print_trie_util(self.head, strings, "")
        print(strings)

    def lookup(self, url):
        if not self.head:
            return

        if url[:4] == "www.":
            url = url[4:]

        node = self.head
        for each in url:
            if each not in node.children.keys():
                print("No domain found - ", url)
                return
            node = node.children[each]

        if node.is_end_of_word:
            print("Domain for ", url, " is", node.domain)
            return node.domain

        print("No domain found - ", url)
        return


if __name__ == "__main__":
    trie = Trie()

    ips = ["107.108.11.123", "107.109.123.255", "74.125.200.106"]
    urls = ["www.samsung.com", "samsung.net", "www.google.in"]

    for idx, url in enumerate(urls):
        trie.insert(url, ips[idx])

    trie.print_trie()

    urls = ["www.samsung.com", "samsung.net", "www.google.in", "www.samsung.net", "google.in"]
    for idx, ip in enumerate(urls):
        trie.lookup(ip)
