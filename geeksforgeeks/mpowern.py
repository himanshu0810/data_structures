

def return_m_power_n(m, n):

    if n == 0:
        return 1
    elif n == 1:
        return m
    elif n == -1:
        return 1 / m
    elif n < 0:
        return (1 / m) * return_m_power_n(m, n+1)

    return m * return_m_power_n(m, n-1)


if __name__ == "__main__":
    m = 4
    n = -1
    value = return_m_power_n(m, n)
    print("value is - ", value)
