
# Complete - Prims Implementation using dict
# O(n) to find the minimum weight in the vertex list


from collections import defaultdict

import sys


class Edge:
    def __init__(self, src, dst, weight):
        self.src = src
        self.dst = dst
        self.weight = weight

    def __repr__(self):
        return str(self.weight)


class Graph:
    def __init__(self, vertices):
        self.V = vertices
        self.map = defaultdict(list)

    def add_edge(self, src, dst, weight):
        edge = Edge(src, dst, weight)
        self.map[src].append(edge)

        edge = Edge(dst, src, weight)
        self.map[dst].append(edge)
        return

    def get_all_edges(self, vertex):
        return self.map[vertex]

    def get_all_vertices(self):
        return self.map.keys()

    def print(self):
        for k, v in self.map.items():
            print(k, v)


def return_extract_min(vertex_with_weights):
    min_value = sys.maxsize + 1
    min_key = None
    for k, v in vertex_with_weights.items():
        if v > min_value:
            continue
        min_value = v
        min_key = k
    return min_key


def prims_algo(gp, initial_vertex):
    results = []
    vertex_edge_mapping = {}
    vertex_with_weights = {}

    vertices = gp.get_all_vertices()
    for each in vertices:
        if each == initial_vertex:
            continue
        vertex_with_weights[each] = sys.maxsize + 1

    print(vertex_with_weights)

    ref_vertex = initial_vertex
    while vertex_with_weights:
        edges = gp.get_all_edges(ref_vertex)
        for edge in edges:
            if edge.dst not in vertex_with_weights:
                continue
            weight = edge.weight
            if vertex_with_weights[edge.dst] < weight:
                continue

            vertex_with_weights[edge.dst] = weight
            vertex_edge_mapping[edge.dst] = edge

        extract_min = return_extract_min(vertex_with_weights)
        results.append(vertex_edge_mapping[extract_min])
        vertex_with_weights.pop(extract_min)
        ref_vertex = extract_min
    return results


if __name__ == "__main__":
    gp = Graph(5)
    gp.add_edge(1, 2, 2)
    gp.add_edge(1, 3, 3)
    gp.add_edge(1, 4, 6)
    gp.add_edge(1, 5, 7)
    gp.add_edge(2, 3, 5)
    gp.add_edge(3, 4, 1)
    gp.add_edge(4, 5, 3)

    gp.print()
    initial_vertex = 1
    results = prims_algo(gp, 1)
    print(results)
