

class a:
	b = 10

	@staticmethod
	def fn():
		print(a.b)

class b1(a):
	b = 20


b1 = b1()
b1.fn()

print()

class a:
	b = 10

	@classmethod
	def fn(cls):
		print(cls.b)

class b1(a):
	b = 20



b1 = b1()
b1.fn()



class E:
    def __init__(self,a, b, c):
        self.a = a
        self.b = b
        self.c = c

    @classmethod
    def fromJson(cls, json):
        return cls(json['a'], json['b'], json['c'])


json = {
    'a': 1, 'b':1, 'c':2
}

a = E.fromJson(json)
print(a.__class__.__bases__)
print(type(a))
print(type(E))


H = type('Himanshu', (), {})
print(type(H))

h = H()
print(type(h))

print(type(type))

print(issubclass(E, object))
print(issubclass(H, object))
print(type(object))


class A:
    def __call__(self):
        print('I am calling')


a = A()

a()


b = 2
print(b.__class__)
print(type(int))
print(type(E))


Nonea= E(1,2,3)
print(a)

#
# New
# init
# call