
# Complete - Return searched element in sorted and pivoted array


def return_searched_element_util(element, array, start, end):
    mid = (start + end) // 2
    if start > end:
        return

    if array[mid] == element:
        return mid

    if array[start] < array[end]:
        if array[mid] < element:
            return return_searched_element_util(element, array, mid+1, end)
        else:
            return return_searched_element_util(element, array, start, mid-1)
    else:
        if element <= array[end]:
            return return_searched_element_util(element, array, mid+1, end)
        else:
            return return_searched_element_util(element, array, start, mid-1)


def return_searched_element(element, array):
    return return_searched_element_util(element, array, 0, len(array) - 1)


if __name__ == "__main__":
    array = [3, 4, 5, 1, 2]
    element = return_searched_element(1, array)
    element = return_searched_element(2, array)
    element = return_searched_element(3, array)
    element = return_searched_element(4, array)
    element = return_searched_element(5, array)
    element = return_searched_element(50, array)
    element = return_searched_element(-1, array)
    print("position is ", element)
