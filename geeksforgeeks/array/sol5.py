
# Complete - Find pair with a sum k


def return_pair_with_sum_k(array, k):
    hash = {}
    for each in array:
        if each in hash:
            return each, hash[each]
        hash[k-each] = each
    return


if __name__ == "__main__":
    array = [1, 4, 45, 6, 10, -8]
    pair = return_pair_with_sum_k(array, 37)
    print("Pair with sum k is ", pair)
