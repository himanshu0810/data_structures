
# Complete - Count Inversions in an array


def return_merged_array(left_arr, right_arr, total_inv):

    arr = []
    left_idx = right_idx = 0

    while left_idx != len(left_arr):
        each = left_arr[left_idx]
        if right_idx == len(right_arr) or each < right_arr[right_idx]:
            arr.append(each)
            left_idx = left_idx + 1
        else:
            arr.append(right_arr[right_idx])
            right_idx = right_idx + 1
            total_inv = total_inv + len(left_arr) - left_idx

    while right_idx != len(right_arr):
        arr.append(right_arr[right_idx])
        right_idx = right_idx + 1

    return arr, total_inv


def count_of_inversions_util(arr, total_inv):
    if not arr or len(arr) == 1:
        return arr, total_inv

    if len(arr) == 2:
        if arr[0] > arr[1]:
            total_inv = total_inv + 1
            arr[0], arr[1] = arr[1], arr[0]
            return arr, total_inv
        return arr, total_inv

    mid = len(arr) // 2
    left_arr, total_inv = count_of_inversions_util(arr[0:mid], total_inv)
    right_arr, total_inv = count_of_inversions_util(arr[mid:], total_inv)

    merged_arr, total_inv = return_merged_array(left_arr, right_arr, total_inv)
    return merged_arr, total_inv


def count_of_inversions(arr):
    total_inv = 0
    return count_of_inversions_util(arr, total_inv)


if __name__ == "__main__":
    arr = [1, 9, 6, 4, 5]
    print("Total number of inversions ", count_of_inversions(arr))
