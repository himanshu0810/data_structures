
# Complete - Merge small array into big array


def merge_small_array_into_big_array(small_array, big_array):
    rpointer = bindex = sindex = 0

    while bindex < len(big_array):
        if sindex == len(small_array):
            sindex = 0

        b_element = big_array[bindex]
        if not b_element:
            while small_array[sindex] is None:
                sindex = sindex + 1

            if big_array[rpointer] and big_array[rpointer] < small_array[sindex]:
                big_array[rpointer], big_array[bindex] = None, big_array[rpointer]
                rpointer = rpointer + 1
            else:
                big_array[bindex] = small_array[sindex]
                small_array[sindex] = None
                sindex = sindex + 1

        elif b_element < small_array[sindex]:
            rpointer = rpointer + 1
            while big_array[rpointer] is None:
                rpointer = rpointer + 1

        else:
            big_array[bindex], small_array[sindex] = small_array[sindex], big_array[bindex]
            rpointer = rpointer + 1

        bindex = bindex + 1

    return big_array


if __name__ == "__main__":
    big_array = [2, None, 7, None, None, 10, None]
    small_array = [5, 8, 12, 14]
    merge_small_array_into_big_array(small_array, big_array)
    print("merged array is ", big_array)
