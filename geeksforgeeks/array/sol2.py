
# Complete - Sorted Array - insert, search and delete an element


class SortedArray:
    def __init__(self):
        self.elements = []

    def insert(self, element):
        leng = len(self.elements)
        if leng == 0:
            self.elements.append(element)
            return

        end = leng - 1
        mid = start = 0

        while start <= end:
            mid = (start + end) // 2
            if element < self.elements[mid]:
                end = mid - 1
            else:
                start = mid + 1

        if element < self.elements[mid]:
            self.elements.insert(mid, element)
        else:
            self.elements.insert(mid+1, element)

    def search(self, element):
        length = len(self.elements)
        if length == 0:
            return False

        end = length - 1
        start = 0

        while start <= end:
            mid = (start + end) // 2
            if element == self.elements[mid]:
                return True, mid
            elif element < self.elements[mid]:
                end = mid - 1
            else:
                start = mid + 1

        return False, None

    def delete(self, element):
        result, key = self.search(element)
        if not result:
            return

        return self.elements.pop(key)


if __name__ == "__main__":
    sorted_array = SortedArray()
    sorted_array.insert(6)
    sorted_array.insert(5)
    sorted_array.insert(8)
    sorted_array.insert(7)
    sorted_array.insert(10)
    sorted_array.insert(3)
    print(sorted_array.elements)
    print("Searching for element is ", sorted_array.search(3))
    print("Searching for element is ", sorted_array.search(5))
    print("Searching for element is ", sorted_array.search(6))
    print("Searching for element is ", sorted_array.search(7))
    print("Searching for element is ", sorted_array.search(8))
    print("Searching for element is ", sorted_array.search(10))
    sorted_array.delete(8)
    print(sorted_array.elements)
