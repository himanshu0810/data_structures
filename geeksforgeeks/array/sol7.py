
# Complete - Return element coming odd number of times


def return_number_coming_odd_times(array):
    element = 0
    for each in array:
        element = element ^ each
    return element


if __name__ == "__main__":
    array = [1, 2, 3, 2, 3, 1, 3]
    no = return_number_coming_odd_times(array)
    print("Element coming odd number of times -", no)
