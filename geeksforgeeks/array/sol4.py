
# Complete - return leaders for each element


def return_leaders(array):
    leaders = []
    max = None
    for each in array[::-1]:
        if not leaders:
            leaders.append(each)
            max = each
            continue
        if each > max:
            leaders.append(each)
            max = each
    return leaders


if __name__ == "__main__":
    array = [16, 17, 4, 3, 5, 2]
    leaders = return_leaders(array)
    print("leaders are ", leaders)
