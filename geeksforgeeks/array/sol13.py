
# Complete - Program for array rotation


def return_rotated_array(array, d):
    current_iter = 0
    len1 = len(array)
    round_flag = False
    if len1 % d == 0:
        round_flag = True

    current_idex = len1 - 1
    temp = None
    while current_iter < d:
        if current_idex < 0:
            current_iter = current_iter + 1
            current_idex = len(array) + current_idex
            if round_flag:
                array[current_idex] = temp
                current_idex = current_idex - 1
                temp = None
                continue

        element = array[current_idex]
        if temp:
            array[current_idex] = temp
        temp = element
        current_idex = current_idex - d
    return array


if __name__ == "__main__":
    ary = [1, 2, 3, 4, 5]  #, 7, 8, 9, 10, 15]
    d = 5

    rotated_array = return_rotated_array(ary, d)
    print("Return Rotated Array is ")
    print(rotated_array)
