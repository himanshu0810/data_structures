
# Complete - Majority Element


def return_majority_element_using_hash_table(array):
    hash_table = {}
    length = len(array)
    for each in array:
        if each in hash_table:
            count = hash_table[each]
            count = count + 1
            if count >= length // 2:
                return each
            hash_table[each] = count
            continue

        hash_table[each] = 1
    return


def return_majority_element_in_o_one_space(array):
    count = 0
    element = None
    for idx, each in enumerate(array):
        if idx == 0:
            count = 1
            element = each
        elif count == 0:
            element = each
            count = 1
        else:
            if each == element:
                count = count + 1
            else:
                count = count - 1

    majority = 0
    for each in array:
        if each == element:
            majority = majority + 1
    if majority >= (len(array) // 2):
        return element
    return


if __name__ == "__main__":
    array = [3, 3, 4, 2, 4, 4, 2, 4, 4]
    majority_element = return_majority_element_using_hash_table(array)
    print("Majority element is ", majority_element)
    majority_element = return_majority_element_in_o_one_space(array)
    print("Majority element using o(1) space is ", majority_element)
