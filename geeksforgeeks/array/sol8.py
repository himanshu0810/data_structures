
# Complete - Return largest contiguous subarray


def largest_contiguous_subarray(array):
    start = prev_sum = end = 0
    for idx, each in enumerate(array):
        if each + prev_sum >= each and  each + prev_sum > each:
            prev_sum = each + prev_sum
            end = idx
        else:
            start = end = idx
            prev_sum = each
    return start, end


if __name__ == "__main__":
    array = [-2, -3, 4, -1, -2, 1, 5, -3]
    sum, subarray = largest_contiguous_subarray(array)
    print("Largest sum and contiguous subarray is ", sum, subarray)
