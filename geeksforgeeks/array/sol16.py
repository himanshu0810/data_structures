
# Complete - Maximum sum such that no two elements are adjacent


def return_max_sum_with_no_adjacent_element(array):
    total_sum = left_sum = 0
    for idx, each in enumerate(array):
        if idx % 2 == 0:
            left_sum = left_sum + each
        total_sum = each + total_sum

    return max(left_sum, total_sum-left_sum)


if __name__ == "__main__":
    ary = [1, 20, 3]
    maximum_sum = return_max_sum_with_no_adjacent_element(ary)
    print("Maximum sum is ", maximum_sum)
