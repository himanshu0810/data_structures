
# Complete - Return Missing number


def return_missing_number(array):
    n = len(array) + 1
    return ((n + 1) * n) // 2 - sum(array)


if __name__ == "__main__":
    array = [1, 2, 4, 5, 6]
    missing_number = return_missing_number(array)
    print("Missing number is ", missing_number)
