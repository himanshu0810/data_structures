
# Complete - Sort elements by frequency


def print_in_order_according_to_frequency(array):

    freq = {}
    for idx, each in enumerate(array):
        if each in freq:
            freq[each][0] = freq[each][0] + 1
            continue
        node = [1, idx]
        freq[each] = node

    li = sorted(freq.items(), key=lambda e: (-e[1][0], e[1][1]))
    for each in li:
        value = each[0]
        freq = each[1][0]
        for i in range(freq):
            print(value, end=' ')


if __name__ == "__main__":
    ary = [2, 5, 2, 8, 5, 6, 8, 8]
    print_in_order_according_to_frequency(ary)
