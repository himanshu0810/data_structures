

# Complete - Reverse the array iteratively and recursively


def return_reversed_array_iteratively(array):
    if not array:
        return array

    start = 0
    end = len(array) - 1
    while start < end:
        array[start], array[end] = array[end], array[start]
        start, end = start + 1, end - 1

    return array


def return_reversed_array_recursively_util(array, start, end):
    if start >= end:
        return array
    array[start], array[end] = array[end], array[start]
    return_reversed_array_recursively_util(array, start+1, end-1)
    return array


def return_reversed_array_recursively(array):
    return return_reversed_array_recursively_util(array, 0, len(array) - 1)


if __name__ == "__main__":
    array = []
    result_array = return_reversed_array_iteratively(array)
    print("Reversed array is ", result_array)
    result_array = return_reversed_array_recursively(array)
    print("Reversed array is ", result_array)
