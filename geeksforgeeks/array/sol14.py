

# Complete - Rotate array using reversal algorithm


def reverse_array(array, left_idx, right_idx):
    if right_idx <= left_idx:
        return array

    array[left_idx], array[right_idx] = array[right_idx], array[left_idx]
    reverse_array(array, left_idx+1, right_idx-1)
    return array


def return_rotated_array(ary, k):
    reverse_array(ary, 0, k-1)
    reverse_array(ary, k, len(ary)-1)
    reverse_array(ary, 0, len(ary)-1)
    return ary


if __name__ == "__main__":
    ary = [1, 2, 3]
    k = 3
    array = return_rotated_array(ary, k)
    print("rotated arrau is ")
    print(array)
