import sys


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.data)


class Tree:
    def __init__(self):
        self.root = None

    def inorder(self, root):
        if root:
            self.inorder(root.left)
            print(root.data, end=' ')
            self.inorder(root.right)

    def tree_top_view(self, root, dist, hash):
        if root is None:
            return

        if dist not in hash:
            print(root.data, end=' ')
            hash[dist] = root

        self.tree_top_view(root.left, dist-1, hash)
        self.tree_top_view(root.right, dist+1, hash)



if __name__ == "__main__":
    tree = Tree()
    tree.root = Node(4)
    tree.root.left = Node(2)
    tree.root.right = Node(5)
    tree.root.left.left = Node(1)
    tree.root.left.right = Node(3)

    # tree.inorder(tree.root)
    tree.tree_top_view(tree.root, 0, {})
    # tree.root.right.left.left = Node(6)
    # tree.root.right.left.right = Node(7)
