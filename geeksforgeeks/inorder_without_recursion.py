
# Inorder of a tree without recursion
from pprint import pprint


class Stack:
    def __init__(self):
        self.top = -1
        self.elements = []

    def push(self, element):
        self.elements.append(element)
        self.top = self.top + 1
        return element

    def is_empty(self):
        if self.top == -1:
            return True
        return False

    def peek(self):
        if self.is_empty():
            return

        return self.elements[self.top]

    def pop(self):
        if self.is_empty():
            return

        element = self.elements.pop()
        self.top = self.top - 1
        return element

    def print(self):
        print(self.elements)


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.data)


class Tree:
    def __init__(self):
        self.root = None

    def print(self, node):
        if node:
            self.print(node.left)
            print(node.data, end=' ')
            self.print(node.right)

    def inorder_without_recursion(self, st):
        if self.root is None:
            return

        st = Stack()
        node = self.root

        while True:
            if node is not None:
                st.push(node)
                node = node.left
                continue

            if st.is_empty():
                break

            node = st.pop()
            print(node.data, end=' ')
            node = node.right
            continue






if __name__ == "__main__":
    tree = Tree()
    tree.root = Node(8)

    tree.root.left = Node(2)
    tree.root.right = Node(15)

    tree.root.right.left = Node(11)
    tree.root.right.right = Node(18)

    tree.root.right.left.left = Node(9)
    # tree.root.right.left.right = Node(13)

    tree.print(tree.root)
    print()

    st = Stack()
    tree.inorder_without_recursion(st)