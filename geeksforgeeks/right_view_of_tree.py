
# Completed - Right view of tree


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.data)


class Tree:
    def __init__(self):
        self.root = None

    def print(self, node):
        if node:
            self.print(node.right)
            print(node.data, end=' ')
            self.print(node.left)

    def recursively_print_right_view(self):
        node = self.root
        self.recursively_print_right_view_util(node, 1, [0])

    def recursively_print_right_view_util(self, node, level, max_level):
        if node is None:
            return

        if level > max_level[0]:
            print(node.data, end=' ')
            max_level[0] = level

        self.recursively_print_right_view_util(node.right, level+1, max_level)
        self.recursively_print_right_view_util(node.left, level+1, max_level)


if __name__ == "__main__":
    tree = Tree()
    tree.root = Node(20)

    tree.root.left = Node(8)
    tree.root.right = Node(22)

    tree.root.left.left = Node(5)
    tree.root.left.right = Node(3)

    tree.root.right.left = Node(4)
    tree.root.right.right = Node(25)

    tree.root.left.right.left = Node(10)
    tree.root.left.right.right = Node(14)

    tree.print(tree.root)
    print()

    tree.recursively_print_right_view()
