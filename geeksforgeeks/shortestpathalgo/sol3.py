
# Completed - Bellman - Ford Algorithm


from collections import defaultdict

import sys
from pprint import pprint


class Graph:
    def __init__(self, V):
        self.V = V
        self.graph = defaultdict(list)

    def insert_edge(self, src, dst, weight):
        self.graph[src].append([dst, weight])

    def remove_edge(self, src):
        self.graph.pop(src)

    def return_shortest_paths(self, init_vertex):
        edges = self.return_all_edges()
        weights = [sys.maxsize for _ in range(self.V) ]
        weights[init_vertex] = 0

        for i in range(self.V-1):
            for src, dst, weight in edges:
                if weights[src] + weight < weights[dst]:
                    weights[dst] = weights[src] + weight
        return weights[:-1]

    def return_all_edges(self):
        edges = []
        for key, ed in self.graph.items():
            for each in ed:
                edges.append([key, each[0], each[1]])
        return edges

    def print_graph(self):
        for k, v in self.graph.items():
            print(k, "->", v)



if __name__ == "__main__":
    gp = Graph(5)
    gp.insert_edge(0, 1, -1)
    gp.insert_edge(0, 2, 4)
    gp.insert_edge(1, 2, 3)
    gp.insert_edge(1, 3, 2)
    gp.insert_edge(1, 4, 2)
    gp.insert_edge(3, 2, 5)
    gp.insert_edge(3, 1, 1)
    gp.insert_edge(4, 3, -3)
    init_vertex = 0

    shortest_paths = gp.return_shortest_paths(init_vertex)
    print("Shortest path from vertex - ", init_vertex)
    pprint(shortest_paths)
