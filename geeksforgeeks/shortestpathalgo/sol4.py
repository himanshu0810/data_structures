
# Complete - Floyd Warshall Algorithm


from collections import defaultdict

from pprint import pprint


class Graph:
    def __init__(self, V):
        self.V = V
        self.graph = defaultdict(list)
        self.vertices = set()

    def insert_edge(self, src, dst, weight):
        self.graph[src].append([dst, weight])
        self.vertices.add(src)
        self.vertices.add(dst)

    def print_graph(self):
        for k, v in self.graph.items():
            print(k, "->", v)

    def floyd_warshall(self):
        init_matrix = []

        inf = 1000
        for i in range(len(self.vertices)):
            li = [inf for _ in range(len(self.vertices))]
            edges = self.graph[i]
            for edge in edges:
                li[edge[0]] = edge[1]
            li[i] = 0
            init_matrix.append(li)

        for k in range(len(self.vertices)):
            for i in range(len(self.vertices)):
                for j in range(len(self.vertices)):
                    if i == j:
                        init_matrix[i][j] = 0
                    elif k == i:
                        continue
                    else:
                        init_matrix[i][j] = min(init_matrix[i][j], init_matrix[i][k] +
                                                init_matrix[k][i])
        pprint(init_matrix)
        return init_matrix


if __name__ == "__main__":
    gp = Graph(4)
    gp.insert_edge(0, 1, 5)
    gp.insert_edge(1, 2, 3)
    gp.insert_edge(2, 3, 1)
    gp.insert_edge(0, 3, 10)

    gp.print_graph()
    gp.floyd_warshall()
