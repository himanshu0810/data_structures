
# Complete - Shortest weight using topological sort


from collections import defaultdict

import sys
from pprint import pprint


class Graph:
    def __init__(self, V):
        self.vertices = set()
        self.V = V
        self.graph = defaultdict(list)

    def insert(self, src, dst, weight):
        self.vertices.add(src)
        self.vertices.add(dst)
        self.graph[src].append([dst, weight])

    def print_graph(self):

        for k, v in self.graph.items():
            print(k, "-->", v)

    def topogical_sort_util(self, visited, sorted, vertex):
        visited.add(vertex)

        edges = self.graph[vertex]
        for dst, weight in edges:
            if dst in visited:
                continue
            self.topogical_sort_util(visited, sorted, dst)

        sorted.append(vertex)

    def topological_sort(self):
        visited = set()
        sorted = []
        a = self.graph.copy()
        for k in a:
            if k in visited:
                continue
            self.topogical_sort_util(visited, sorted, k)

        print(sorted)
        return sorted

    def return_lowest_weights(self):
        sorted = self.topological_sort()
        weights = {}
        inf = sys.maxsize
        for each in sorted:
            weights[each] = inf

        sorted = sorted[::-1]
        a = self.graph.copy()
        weights['r'] = 0
        for each in sorted:
            edges = a[each]
            for dst, weight in edges:
                if weights[each] + weight < weights[dst]:
                    weights[dst] = weights[each] + weight

        pprint(weights)


if __name__ == "__main__":
    gp = Graph(6)
    gp.insert("r", "t", 3)
    gp.insert("r", "s", 5)
    gp.insert("t", "y", -1)
    gp.insert("t", "x", 7)
    gp.insert("x", "y", -1)
    gp.insert("x", "z", 1)
    gp.insert("s", "x", 6)
    gp.insert("s", "t", 2)
    gp.insert("y", "z", -2)

    gp.print_graph()
    print("return weights")
    gp.return_lowest_weights()
