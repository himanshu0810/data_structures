
# Complete - Return shortest path with exactly k edges

import sys


def return_shortest_path(matrix, inf, u, v, k):
    V = 4

    sp = [[[None for _ in range(k+1)] for _ in range(V)] for _ in range(V)]

    for edge in range(k+1):
        for i in range(V):
            for j in range(V):
                sp[i][j][edge] = inf
                if edge == 0 and i == j:
                    sp[i][j][edge] = 0
                elif edge == 1 and matrix[i][j] != inf:
                    sp[i][j][edge] = matrix[i][j]

                if edge > 1:
                    for a in range(V):
                        if matrix[i][a] != inf and i != a and j != a and sp[a][j][edge-1] != inf:
                            r1 = sp[i][j][edge]
                            r2 = matrix[i][a]
                            r3 = sp[a][j][edge-1]
                            sp[i][j][edge] = min(r1, r2 + r3)

    return sp[u][v][k]


if __name__ == "__main__":
    inf = sys.maxsize + 1
    matrix = [    
        [0, 10, 3, 2],
        [inf, 0, inf, 7],
        [inf, inf, 0, 6],
        [inf, inf, inf, 0]
    ]
    u, v, k = 0, 3, 2
    print(return_shortest_path(matrix, inf, u, v, k))
