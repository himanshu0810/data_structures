
# Complete - Djiktras Algorithm


from collections import defaultdict
from pprint import pprint


class Element:
    def __init__(self, data, distance=None):
        self.data = data
        self.distance = distance

    def __repr__(self):
        return str(self.data) + "-" + str(self.distance)


class Heap:
    def __init__(self):
        self.elements = []
        self.size = 0
        self.pos = []

    def is_empty(self):
        return len(self.elements) == 0

    def swap(self, index, index_1):
        if index_1 <= index:
            return
        self.elements[index], self.elements[index_1] = self.elements[index_1], \
                                                       self.elements[index]

    def get_parent_index(self, child_index):
        return (child_index - 1) // 2

    def get_left_child_index(self, parent_index):
        return 2 * parent_index + 1

    def get_right_child_index(self, parent_index):
        return 2 * parent_index + 2

    def has_parent(self, child_index):
        return self.get_parent_index(child_index) > -1

    def has_left_child(self, parent_index):
        return self.get_left_child_index(parent_index) < self.size

    def has_right_child(self, parent_index):
        return self.get_right_child_index(parent_index) < self.size

    def get_parent(self, child_index):
        if not self.has_parent(child_index):
            return
        return self.elements[self.get_parent_index(child_index)]

    def get_left_child(self, parent_index):
        if not self.has_left_child(parent_index):
            return
        return self.elements[self.get_left_child_index(parent_index)]

    def get_right_child(self, parent_index):
        if not self.has_right_child(parent_index):
            return
        return self.elements[self.get_right_child_index(parent_index)]

    def insert(self, element, distance):
        self.size = self.size + 1
        element = Element(element, distance)
        self.elements.append(element)
        self.heapify_up()

    def extract_min(self):
        element = self.elements.pop(0)
        self.size = self.size - 1
        self.swap(0, self.size-1)
        self.heapify_down(0)
        return element

    def heapify_up(self):
        index = self.size - 1
        while self.has_parent(index) and self.get_parent(index).distance > \
                self.elements[index].distance:
            parent_index = self.get_parent_index(index)
            self.swap(index, parent_index)
            index = parent_index

    def heapify_down(self, index):

        while self.has_left_child(index):
            smaller_child_index = self.get_left_child_index(index)

            if self.has_right_child(index) and self.get_right_child(index).distance \
                    < self.elements[smaller_child_index].distance:
                smaller_child_index = self.get_right_child_index(index)

            if self.elements[index].distance <= \
                    self.elements[smaller_child_index].distance:
                break

            self.swap(self.get_parent_index(index), smaller_child_index)
            index = smaller_child_index

    def print(self):
        print(self.elements)

    def is_in_min_heap(self, dst):
        for element in self.elements:
            if element.data == dst:
                return True
        return False

    def decrease_key(self, vertex, distance):

        index = element = None
        for idx, node in enumerate(self.elements):

            if node.data == vertex:
                index = idx
                element = node
                break

        element.distance = distance
        self.heapify_down(index)


class Node:
    def __init__(self, dst, weight):
        self.dst = dst
        self.weight = weight

    def __repr__(self):
        return str(self.dst) + "-->" + str(self.weight)


class Graph:
    def __init__(self, V):
        self.graph = defaultdict(list)
        self.V = V
        self.vertices = set()

    def add_edge(self, src, dst, weight):
        self.graph[src].append(Node(dst, weight))
        self.graph[dst].append(Node(src, weight))
        self.vertices.add(src)
        self.vertices.add(dst)
        return

    def djiktras(self, init_vertex):

        dist = [None for _ in range(9)]
        inf = 9999
        min_heap = Heap()
        min_heap.insert(init_vertex, 0)

        for vertex in self.vertices:
            if vertex == init_vertex:
                dist[init_vertex] = 0
                continue
            dist[vertex] = inf
            min_heap.insert(vertex, inf)

        while not min_heap.is_empty():

            node = min_heap.extract_min()
            usrc, udist = node.data, node.distance
            print(node.data, "-->", node.distance)

            edges = self.graph[node.data]
            for each in edges:
                dst, weight = each.dst, each.weight
                if min_heap.is_in_min_heap(dst) and dist[usrc] != inf and \
                        dist[usrc] + weight < dist[dst]:
                    dist[dst] = dist[usrc] + weight
                    min_heap.decrease_key(dst, dist[dst])


if __name__ == "__main__":
    gp = Graph(5)
    gp.add_edge(0, 1, 4)
    gp.add_edge(0, 7, 8)
    gp.add_edge(1, 7, 11)
    gp.add_edge(7, 8, 7)
    gp.add_edge(7, 6, 1)
    gp.add_edge(8, 6, 6)

    print("Printing the graph")
    pprint(gp.graph)

    print("Initialise the djiktras algo")
    gp.djiktras(0)
