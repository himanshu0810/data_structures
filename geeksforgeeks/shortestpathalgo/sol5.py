from geeksforgeeks.shortestpathalgo.sol3 import Graph

# Complete = Johnson Algorithm


def johnson_algorithm(graph):
    no_of_vertices = graph.V
    new_vertex = no_of_vertices
    graph.V = graph.V + 1

    for vertex in range(no_of_vertices):
        graph.insert_edge(new_vertex, vertex,0)

    # Calling bellmen Ford Algorithm
    hvalues = graph.return_shortest_paths(new_vertex)
    print(hvalues)

    graph.remove_edge(new_vertex)

    # Before updating the weights
    graph.print_graph()

    # Update weights
    for src, edges in graph.graph.items():
        for node in edges:
            node[1] = hvalues[src] - hvalues[node[0]] + node[1]

    print("After updating the weights")
    # After updating the weights
    graph.print_graph()


    # TODO call djiktras for each vertex
    return


if __name__ == "__main__":
    gp = Graph(4)
    gp.insert_edge(0, 1, -5)
    gp.insert_edge(0, 3, 3)
    gp.insert_edge(1, 2, 4)
    gp.insert_edge(2, 3, 1)
    gp.insert_edge(0, 2, 2)

    johnson_algorithm(gp)
