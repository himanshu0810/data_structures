class Singleton(type):
    _instances = {}

    def __new__(mcls, *args, **kwargs):
        print(1)
        # print('singleton new method')
        return super().__new__(mcls, *args, **kwargs)

    def __init__(cls, *args, **kwargs):
        print(2)
        # print('***singleton init**')

    def __call__(cls, *args, **kwargs):
        print(3)
        # print('I am singleton calling method')
        # print(cls)
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Parent(metaclass=Singleton):
    def __new__(cls, *args, **kwargs):
        print(4)
        # print('I am parent new')
        # print(cls)
        # print(args)
        # print(kwargs)
        return super(Parent, cls).__new__(cls)

    def __init__(self, *args, **kwargs):
        print(5)
        # print('I am parent init')
        # print(self)
        # print(args)
        # print(kwargs)
        self.args = args
        self.kwargs = kwargs

type()
p = Parent(1, 2, a=30)
print(p.__metaclass__)
# print(p)

print('starting for p2')

# p2 = Parent(1, 2, a=30)
#
# print(p2)
