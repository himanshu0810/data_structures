from classes.queue import Queue
from classes.stack import Stack


if __name__ == "__main__":
    queue = Queue()
    st1 = Stack()
    st2 = Stack()
    st1.push(10)
    st1.push(1)
    st1.push(4)
    st1.push(3)
    st1.push(2)
    st1.push(12)
    st1.push(13)
    st1.push(7)

    st2.push(st1.pop())
    st2.push(st1.pop())
    st2.push(st1.pop())
    st2.push(st1.pop())
    st2.push(st1.pop())
    st2.push(st1.pop())
    st2.push(st1.pop())
    st2.push(st1.pop())
    print(st2.peek())
    print(st2.elements)
    print(st2.pop())
