from pprint import pprint


def find_increasing_subsequence(values):
    coin_array = [1 for _ in range(len(values))]
    temp_array = [id for id in range(len(values))]

    for i in range(1, len(values)):
        for j in range(i):
            if values[j] < values[i]:
                if 1 + coin_array[j] > coin_array[i]:
                    coin_array[i] = 1 + coin_array[j]
                    temp_array[i] = j

    return coin_array, temp_array


if __name__ == "__main__":
    values = [3, 4, -1, 0, 6, 2, 3]
    coin_array, temp_array = find_increasing_subsequence(values)
    pprint(coin_array)
    pprint(temp_array)
    j = max(temp_array)
    idx = temp_array.index(j)
    while idx > 0:
        print(temp_array[idx], end=' ')
        if temp_array[idx] == idx:
            break
        idx = temp_array[idx]
