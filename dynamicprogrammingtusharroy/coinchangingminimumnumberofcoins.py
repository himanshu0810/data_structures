from pprint import pprint


def return_coin_matrix(coins, total):

    coin_matrix = []
    for row, coin_value in enumerate(coins):
        coin_matrix.append([0])
        for col in range(1, total+1):
            if col < coins[row]:
                if row == 0:
                    coin_matrix[row].append(0)
                else:
                    coin_matrix[row].append(coin_matrix[row-1][col])
            elif col == coins[row]:
                coin_matrix[row].append(1)
            else:
                if row == 0:
                    coin_matrix[row].append(1 + coin_matrix[row][col-coins[row]])
                else:
                    coin_matrix[row].append(min(1 + coin_matrix[row][col-coins[row]],
                                                coin_matrix[row-1][col]))
    pprint(coin_matrix)
    return coin_matrix



if __name__ == "__main__":
    total = 11
    coins = [1, 5, 6, 8]
    coin_matrix = return_coin_matrix(coins, total)
    i = len(coin_matrix) - 1
    j = len(coin_matrix[0]) - 1
    while i > 0 and j > 0:
        if coin_matrix[i][j] == coin_matrix[i-1][j]:
            i = i - 1
        else:
            print(coins[i], end=' ')
            j = j - coins[i]
            i = i - 1




