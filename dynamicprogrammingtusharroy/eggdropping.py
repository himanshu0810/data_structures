from pprint import pprint


def return_floor_egg_matrix(floors, eggs):
    li = []
    for row in range(eggs):
        li.append([])
        for col in range(floors):
            if col < row:
                li[row][col] = li[row-1][col]
            else:
                li[row][col] = 1
    pprint(li)
    return li


if __name__ == "__main__":
    floors = 6
    eggs = 2
    floor_egg_matrix = return_floor_egg_matrix(floors, eggs)
