def find_subset(li, sum):
    if sum == 0:
        return True

    if len(li) == 0 and sum != 0:
        return False

    if len(li) == 1 and li[0] != sum:
        return False

    if li[-2] > sum:
        return False

    return find_subset(li[:-1], sum) or find_subset(li[:-1], sum - li[-1])


if __name__ == "__main__":
    li = [3, 34, 4, 12, 5, 2]
    result = find_subset(li, 12)
    print("result is - ", result)
