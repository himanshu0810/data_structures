from pprint import pprint


def createsubsetmatrix(li, sum):
    subset = []
    a = [True]
    for _ in range(sum):
        a.append(False)
    subset.append(a)

    for idx, row in enumerate(li):
        subset.append([True])
        idx = idx + 1
        for col in range(1, sum+1):
            if col < row:
                subset[idx].append(subset[idx-1][col])
            elif col == row:
                subset[idx].append(True)
            else:
                subset[idx].append(subset[idx-1][col-row])
    return subset


if __name__ == "__main__":
    li = [2, 3, 7, 8, 11]
    sum = 12

    subset = createsubsetmatrix(li, sum)

    row = len(subset) - 1
    col = sum
    while row > 0 and col > 0:
        if subset[row][col] == True:
            print(li[row-1])
            col = col - li[row-1]
        row = row - 1

    print("not exist")


