from pprint import pprint

if __name__ == "__main__":
    str1 = "aggtab"
    str2 = "gxtxayb"

    li = []
    li.append([0 for _ in range(len(str2) + 1)])

    for row in range(len(str1)):
        li.append([0])
        for col in range(len(str2)):
            if str2[col] == str1[row]:
                li[row+1].append(li[row][col] + 1)
            else:
                li[row+1].append(max(li[row+1][col], li[row][col+1]))

    print(li[len(li)-1][-1])
    k = ""
    i = len(li) - 1
    j = len(li[0]) - 1
    while i > 0 and j > 0:
        if li[i][j] == li[i - 1][j]:
            i = i - 1
        elif li[i][j] == li[i][j - 1]:
            j = j - 1
        else:
            k = k + str1[i-1]
            i = i - 1
            j = j - 1
    print("k is ", k[::-1] )
