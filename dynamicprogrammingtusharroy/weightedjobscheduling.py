from pprint import pprint


def return_profit_matrix(jobs, profits):
    temp = profits.copy()
    for idx in range(1, len(jobs)):
        start_time, end_time = jobs[idx][0], jobs[idx][1]
        for j in range(idx):
            job_each_start_time, job_each_end_time = jobs[j][0], jobs[j][1]
            if job_each_end_time <= start_time and temp[idx] < profits[idx] + temp[j]:
                temp[idx] = temp[j] + profits[idx]
    return temp


if __name__ == "__main__":
    jobs = [[1, 3], [2, 5], [4, 6], [6, 7], [5, 8], [7, 9]]
    profits = [5,6,5,4,11,2]
    profit_array = return_profit_matrix(jobs, profits)
    # print(profit_array)
    # print(profits)
    max_elem = max(profit_array)
    idx = profit_array.index(max_elem)
    while idx > -1:
        print(jobs[idx])
        diff = profit_array[idx] - profits[idx]
        if diff == 0:
            break
        idx = profits.index(diff)
