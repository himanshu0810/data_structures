from pprint import pprint


def return_knapsack_weights(weights, values, max_weight):
    knapsack_weights = []
    for row, each_weight in enumerate(weights):
        knapsack_weights.append([0])
        for col in range(1, max_weight + 1):
            if col < each_weight and row == 0:
                knapsack_weights[row].append(knapsack_weights[row][col-1])
            elif col < each_weight:
                knapsack_weights[row].append(knapsack_weights[row-1][col])
            else:
                if row == 0:
                    knapsack_weights[row].append(values[row])
                else:
                    knapsack_weights[row].append(max(values[row] + knapsack_weights[row-1][col-each_weight],
                                                     knapsack_weights[row-1][col]))

    return knapsack_weights


if __name__ == "__main__":
    values = [60, 100, 120]
    weights = [10, 20, 30]

    knapsack_weights = return_knapsack_weights(weights, values, 50)
    i = len(knapsack_weights) - 1
    j = len(knapsack_weights[0]) - 1
    while i > 0 and j > 0:
        if knapsack_weights[i][j] == knapsack_weights[i-1][j]:
            i = i - 1
        else:
            print(weights[i], end=' ')
            j = j - weights[i]
            i = i - 1
