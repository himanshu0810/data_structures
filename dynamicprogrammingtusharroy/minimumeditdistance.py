from pprint import pprint

if __name__ == "__main__":
    str1 = "azced"
    str2 = "abcdef"

    li = [[0,1,2,3,4 ,5 ,6]]

    for row in range(len(str1)):
        li.append([row+1])
        for col in range(len(str2)):
            if str1[row] == str2[col]:
                li[row+1].append(li[row][col])
            else:
                li[row+1].append(min(li[row+1][col], li[row][col], li[row][col+1]) + 1)

    i = len(li) - 1
    j = len(li[0]) - 1
    while i > 0 and j > 0:
        if str1[i-1] == str2[j-1]:
            i = i - 1
            j = j - 1
        elif li[i][j] == li[i-1][j-1] + 1:
            print("edit ", str1[i-1], " ", str2[j-1])
            i = i - 1
            j = j - 1
        elif li[i][j] == li[i][j-1] + 1:
            print("delete ", str2[j-1])
            j = j - 1
        else:
            print("delete ", str1[i - 1])
            i = i - 1


