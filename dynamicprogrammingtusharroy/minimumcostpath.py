from pprint import pprint

if __name__ == "__main__":
    li = [
        [1,2,5,7],
        [11,13,1,9],
        [6,7,3,4],
        [5,6,8,9],
        [10,2,3,11]
    ]

    weights_list = []

    for row in range(len(li)):

        for col in range(len(li[0])):
            if col == 0:
                if row == 0:
                    weights_list.append([li[row][col]])
                else:
                    weights_list.append([li[row][col] + weights_list[row-1][col]])
            else:
                if row == 0:
                    weights_list[row].append(li[row][col] + weights_list[row][col-1])
                else:
                    elem = min(li[row][col] + weights_list[row][col-1],
                               li[row][col] + weights_list[row-1][col])
                    weights_list[row].append(elem)

    paths_contributed = []
    row = len(li) - 1
    col = len(li[0]) - 1
    while row > 0  and col > 0:
        if weights_list[row][col] - li[row][col] == weights_list[row-1][col]:
            paths_contributed.append(li[row-1][col])
            row = row - 1
        else:
            paths_contributed.append(li[row][col-1])
            col = col - 1


    pprint(weights_list)
    print(paths_contributed)
