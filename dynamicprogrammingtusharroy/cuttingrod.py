from pprint import pprint


def return_rod_matrix(lengths, weights, max_length):
    rod_matrix = []

    for row, each_weight in enumerate(weights):
        rod_matrix.append([0])
        for col in range(max_length + 1):
            if col < row + 1 and row != 0:
                rod_matrix[row].append(rod_matrix[row-1][col])
            elif col < row + 1 and row == 0:
                rod_matrix[row].append(0)
            else:
                if row == 0:
                    rod_matrix[row].append(rod_matrix[row][col-row-1] + lengths[row])
                else:
                    rod_matrix[row].append(max(rod_matrix[row][col-row-1]+lengths[row],
                                               rod_matrix[row-1][col]))
    pprint(rod_matrix)
    return rod_matrix


if __name__ == "__main__":
    lengths = [1, 2, 3, 4]
    weights = [2, 5, 7, 8]
    given_length = 5
    rod_matrix = return_rod_matrix(weights, lengths, given_length)

