from pprint import pprint


def return_substring_cost(str_1, str_2):
    li = []
    a = [0]
    b = [0 for _ in range(len(str_1))]
    a = a + b
    li.append(a)

    for row in range(len(str_2)):
        li.append([0])
        for col in range(len(str_1)):
            if str_2[row] == str_1[col]:
                li[row + 1].append(li[row][col] + 1)
            else:
                li[row + 1].append(0)
    return li


if __name__ == "__main__":

    str_1 = "abcdaf"
    str_2 = "zbcdf"
    li = return_substring_cost(str_1, str_2)
    max = 0
    col = -1
    for idx, each in enumerate(li):
        for i, a in enumerate(each):
            if a > max:
                max = a
                col = i
    print("max and index are ", max, col)
    print("maximum substring is ", str_1[col-max:col])
