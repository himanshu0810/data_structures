


if __name__ == "__main__":
    input = [2, 3, 1, 1, 2, 4, 2, 0, 1, 1]
    # input  = [1,3,5,8,9,2,6,7,6,8,9]
    no_of_jumps = [0]
    actual_jumps = [-1]
    for i in range(1, len(input)):
        for j in range(i):
            elem = input[j] + j
            if elem >= i:
                if len(no_of_jumps) < i+1:
                    no_of_jumps.append(1 + no_of_jumps[j])
                    actual_jumps.append(j)
                else:
                    if no_of_jumps[i] > 1 + no_of_jumps[j]:
                        no_of_jumps[i] = 1 + no_of_jumps[j]
                        actual_jumps[i] = j

    print(input)
    print(no_of_jumps)
    print(actual_jumps)
    print()
    i = len(actual_jumps) - 1
    while i >= 0:
        print(i, end=' ')
        i = actual_jumps[i]

