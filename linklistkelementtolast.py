class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:
    def __init__(self):
        self.head = None

    def insert(self, data):
        if self.head is None:
            self.head = Node(data)
            return self.head
        node = self.head
        while node.next is not None:
            node = node.next
        node.next = Node(data)
        return node.next

    def print(self):
        node = self.head
        while node is not None:
            print(node.data, end=' ')
            node = node.next

    def last_to_k_position(self, k):
        node = self.head
        a = self.last_position(node, k)
        return a

    def last_position(self, node, k):
        if node:
            return None, k

        res_node, k = self.last_position(node.next, k)
        k = k - 1
        if k == 0:
            return node, k
        return res_node, k


if __name__ == "__main__":
    linked_list = LinkedList()
    linked_list.insert(10)
    linked_list.insert(3)
    linked_list.insert(5)
    linked_list.insert(1)
    linked_list.insert(8)
    linked_list.print()
    print("")
    a, _ = linked_list.last_to_k_position(6)
    print(a.data)
