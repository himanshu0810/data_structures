
# Priority Queue using ordered array
# Insert - O(n)
# Delete - O(1)
# Max - O(1)


class PriorityQueue:
    def __init__(self):
        self.array = []

    def insert(self, data):
        if not self.array:
            self.array.append(data)
            return

        for idx, each in enumerate(self.array):
            if data < each:
                self.array.insert(idx, data)
                return self.array

        self.array.append(data)
        return self.array

    def get_highest_priority(self):
        if not self.array:
            return
        return self.array[-1]

    def delete_highest_priority(self):
        if not self.array:
            return
        return self.array.pop()

    def print(self):
        print(self.array)


if __name__ == "__main__":
    pq = PriorityQueue()
    pq.insert(20)
    pq.insert(3)
    pq.insert(1)
    pq.insert(10)
    pq.insert(16)
    pq.insert(12)
    pq.print()
    print(pq.get_highest_priority())
    pq.delete_highest_priority()
    pq.print()
    pq.delete_highest_priority()
    pq.print()
