

class MinHeap:
    def __init__(self):
        self.elements = []

    def swap(self, index, index1):
        temp = self.elements[index]
        self.elements[index] = self.elements[index1]
        self.elements[index1] = temp


    def has_parent(self, index):
        return index // 2 >= 0

    def hasleftchild(self, index):
        return 2 * index + 1 < len(self.elements)

    def hasrightchild(self, index):
        return 2 * index + 2 < len(self.elements)

    def get_parent_index(self, child_index):
        return (child_index - 1) // 2

    def get_left_child_index(self, parent_index):
        return 2 * parent_index + 1

    def get_right_child_index(self, parent_index):
        return 2 * parent_index + 2

    def get_parent(self, parent_index):
        return self.elements[parent_index]

    def get_left_child(self, left_child_index):
        return self.elements[left_child_index]

    def get_right_child(self, right_child_index):
        return self.elements[right_child_index]

    def insert(self, value):
        self.elements.append(value)
        self.heapify_up()

    def remove(self, value):
        if value not in self.elements:
            return

        if len(self.elements) == 1:
            self.elements = []
            return

        index = self.elements.index(value)
        if index == len(self.elements) - 1:
            self.elements.remove(value)
            return

        self.elements[index] = self.elements[-1]
        self.elements.pop()
        self.heapify_down()

    def heapify_up(self):
        index = len(self.elements) - 1

        while self.has_parent(index) and self.get_parent(self.get_parent_index(index)) > \
                self.elements[index]:
            self.swap(self.get_parent_index(index), index)
            index = self.get_parent_index(index)

    def heapify_down(self):
        index = 0
        while self.hasleftchild(index):
            left_index = self.get_left_child_index(index)
            if self.elements[index] > self.elements[left_index]:
                self.swap(index, left_index)
                index = left_index
                continue
            elif self.elements[index] > self.elements[self.get_right_child_index(index)]:
                right_index = self.get_right_child_index(index)
                self.swap(index, right_index)
                index = right_index
                continue
            break
