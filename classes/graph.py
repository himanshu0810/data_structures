

class Vertex:
    def __init__(self, node):
        self.adjacent = {}
        self.node = node
        self.is_visited = False

    def get_id(self):
        return self.node

    def get_connections(self):
        return self.adjacent.keys()

    def add_neighbour(self, node, weight=0):
        self.adjacent[node] = weight

    def remove_neighbour(self, node):
        self.adjacent.pop(node, None)

    def get_weight(self, neighbour):
        return self.adjacent[neighbour]

    def __str__(self):
        return self.node + " " + str(self.adjacent)


class Graph:
    def __init__(self):
        self.vertices = {}
        self.num_vertices = 0

    def add_vertex(self, node):
        self.num_vertices = self.num_vertices + 1
        vertex = Vertex(node)
        self.vertices[node] = vertex
        return vertex

    def get_vertex(self, node):
        return self.vertices[node]

    def get_vertices(self):
        return self.vertices.keys()

    def add_edge(self, frm, to, cost=0):
        if frm not in self.vertices.keys():
            self.add_vertex(frm)
        if to not in self.vertices.keys():
            self.add_vertex(to)

        self.vertices[frm].add_neighbour(to, cost)
        self.vertices[to].add_neighbour(frm, cost)

    def remove_edge(self, frm, to):
        if frm not in self.vertices.keys():
            return
        if to not in self.vertices.keys():
            return

        self.vertices[frm].remove_neighbour(to)
        self.vertices[to].remove_neighbour(frm)

    def breadth_first_search_traversal_1(self, vertex):
        if vertex not in self.get_vertices():
            return

        root = self.vertices[vertex]
        root.is_visited = True
        queue = [root]
        while True:
            node_count = len(queue)
            if node_count == 0:
                break

            while node_count > 0:
                node = queue.pop(0)
                for v in node.get_connections():
                    each = self.vertices[v]
                    if not each.is_visited:
                        each.is_visited = True
                        queue.append(each)

                print(node.node, end=' ')
                node_count = node_count - 1
            print('')

    def breadth_first_search_traversal(self, vertex):
        if vertex not in self.get_vertices():
            return

        root = self.vertices[vertex]
        root.is_visited = True
        queue = [root]
        while queue:
            v = queue.pop(0)
            if v == "level":
                print("")
                continue

            check = False
            for each in v.get_connections():
                each = self.vertices[each]
                if not each.is_visited:
                    each.is_visited = True
                    queue.append(each)
                    check = True

            if check:
                queue.append("level")

            print(v.node, end=' ')

    def depth_first_search_traversal(self, vertex):
        if vertex not in self.get_vertices():
            return

        self.vertices[vertex].is_visited = True

        for each in self.vertices[vertex].get_connections():
            each_obj = self.vertices[each]
            if each_obj.is_visited:
                continue

            each_obj.is_visited = True
            self.depth_first_search_traversal(each)
        print(self.vertices[vertex].node)

    def find_all_paths(self, vertex, paths):
        if vertex not in self.get_vertices() or self.vertices[vertex].is_visited:
            return

        vertex_obj = self.vertices[vertex]
        vertex_obj.is_visited = True
        connections = [each for each in vertex_obj.get_connections() if not self.vertices[each].is_visited]
        if not connections:
            paths.append(vertex)
            print("paths", paths)
            paths.pop()
        else:
            for each in connections:
                paths.append(vertex)
                self.vertices[vertex].is_visited = True
                self.find_all_paths(each, paths)
                paths.remove(vertex)
        return paths


if __name__ == "__main__":
    g = Graph()

    # g.add_vertex('a')
    # g.add_vertex('b')
    # g.add_vertex('c')
    # g.add_vertex('d')
    # g.add_vertex('e')
    # g.add_vertex('f')

    g.add_edge('s', 'a', 7)
    g.add_edge('s', 'b', 9)
    g.add_edge('b', 't', 14)
    g.add_edge('a', 'm', 10)
    g.add_edge('a', 'n', 15)
    g.add_edge('a', 'o', 11)
    g.add_edge('m', 'f', 2)
    g.add_edge('n', 'f', 6)
    g.add_edge('o', 'f', 9)

    # Printing all the vertices
    # print("Vertices present - ", list(g.get_vertices()))

    # Printing the connections of each vertex
    # for v in g.get_vertices():
    #     print(v, "-", list(g.vertices[v].get_connections()))

    # Print the connected nodes of a specific vertex
    print("Connections present for node a - ", list(g.vertices['a'].get_connections()))

    # Add an edge after creating the graph
    # g.add_edge('a', 'd', 0)

    # Printing the connections of each vertex after adding an edge between a and d
    # for v in g.get_vertices():
    #     print(v, "-", list(g.vertices[v].get_connections()))

    # Remove an edge between d and c
    # g.remove_edge('d', 'c')

    # Printing the connections of each vertex
    print("Printing after removing the edge ")
    # for v in g.get_vertices():
    #     print(v, "-", list(g.vertices[v].get_connections()))

    print("printing breadth first traversal")
    # g.breadth_first_search_traversal_1('o')

    print("=====================")
    print("printing depth first traversal")
    # print(g.depth_first_search_traversal('s'))


    g.find_all_paths('s', [])
