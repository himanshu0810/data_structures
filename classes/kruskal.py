from pprint import pprint


class Edge:
    def __init__(self, src, dst, weight):
        self.src = src
        self.dst = dst
        self.weight = weight

    def __repr__(self):
        return str(self.src) + "->" + str(self.dst)


class Graph:
    def __init__(self, vertes):
        self.edges = []
        self.map = {}
        self.V = vertes

    def add_edge(self, src, dst, weight=0):
        edge = Edge(src, dst, weight)
        self.edges.append(edge)
        return edge

    def return_all_edges(self):
        return self.edges

    def kruskal_mst(self):
        edges = sorted(self.edges, key=lambda item: item.weight)
        print(edges)
        parent = []
        rank = []
        result = []

        for node in range(len(self.edges)):
            parent.append(node)
            rank.append(0)

        i = e = 0
        while len(result) < self.V - 1:
            edge = edges[i]
            u, v, w = edge.src, edge.dst, edge.weight
            i = i + 1

            u_parent = self.find_parent(parent, u)
            v_parent = self.find_parent(parent, v)
            if u_parent == v_parent:
                continue

            e = e + 1
            result.append([u, v, w])
            self.union(parent, rank, u, v)
        pprint(result)

    def find_parent(self, parent, u):
        if parent[u] == u:
            return u
        return self.find_parent(parent, parent[u])

    def union(self, parent, rank, u, v):
        uroot = self.find_parent(parent, u)
        vroot = self.find_parent(parent, v)

        rank_uroot = rank[uroot]
        rank_vroot = rank[vroot]
        if rank_uroot > rank_vroot:
            parent[vroot] = uroot
        elif rank_uroot < rank_vroot:
            parent[uroot] = vroot
        else:
            parent[vroot] = uroot
            rank[uroot] = rank[uroot] + 1


if __name__ == "__main__":
    gp = Graph(4)
    gp.add_edge(0, 1, 10)
    gp.add_edge(0, 2, 6)
    gp.add_edge(0, 3, 5)
    gp.add_edge(1, 3, 15)
    gp.add_edge(2, 3, 4)

    gp.kruskal_mst()
