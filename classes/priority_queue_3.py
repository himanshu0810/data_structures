
# Priority Queue using unordered linked list
# Insert - O(1)
# Extract Max - O(n)
# Delete Max - O(n)


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class PriorityQueue:
    def __init__(self):
        self.head = None

    def insert(self, data):
        if not self.head:
            self.head = Node(data)
            return self.head

        node = self.head
        while node.next:
            node = node.next
        node.next = Node(data)
        return

    def get_highest_priority(self):
        node = self.head
        max_element = -1
        while node:
            if node.data > max_element:
                max_element = node.data
            node = node.next
        return max_element

    def delete_highest_priority(self):
        if not self.head:
            return

        if self.head.next is None:
            self.head = None
            return

        node = self.head
        max_element = -1
        temp = prev = node
        while node:
            if node.data > max_element:
                max_element = node.data
                temp = prev
            prev = node
            node = node.next
        temp.next = temp.next.next
        return

    def print(self):
        if not self.head:
            return

        node = self.head
        while node:
            print(node.data, end=' ')
            node = node.next
        print()


if __name__ == "__main__":
    pq = PriorityQueue()
    pq.insert(20)
    pq.insert(3)
    pq.insert(1)
    pq.insert(10)
    pq.insert(16)
    pq.insert(12)
    pq.print()
    print(pq.get_highest_priority())
    pq.delete_highest_priority()
    pq.print()
