class Solution(object):

    def partition(self, arr, k, l, r):

        pivot = arr[r]
        r1 = r - 1
        while l < r1:
            if arr[l] < pivot:
                l = l + 1
            elif arr[r1] > pivot:
                r1 = r1 - 1
            else:
                arr[l], arr[r1] = arr[r1], arr[l]

        if arr[r1] < pivot:
            return r
        arr[r], arr[r1] = arr[r1], arr[r]
        return r1

    def quick_select(self, arr, k, l, r):
        p = self.partition(arr, k, l, r)
        if p == k:
            return arr[:p]
        elif p > k:
            self.quick_select(arr, k, l, p - 1)
        else:
            self.quick_select(arr, k, p + 1, r)

        return arr[:k]

    def kClosest(self, points, K):
        """
        :type points: List[List[int]]
        :type K: int
        :rtype: List[List[int]]
        """
        points = [3, 2, 5, 1, 6, 8]
        k = 3
        print(self.quick_select(points, k, 0, len(points) - 1))


if __name__ == "__main__":
    sol = Solution()
    points = None
    sol.kClosest(points, 3)
