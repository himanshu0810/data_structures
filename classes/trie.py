

class TrieNode:
    def __init__(self):
        self.childs = {}
        self.is_complete_word = False

    def insert_string(self, data):
        for ch in data:
            if ch not in self.childs.keys():
                self.childs[ch] = TrieNode()
            self = self.childs[ch]
        self.is_complete_word = True

    def is_present(self, word):
        for char in word:
            if char not in self.childs.keys():
                return
            self = self.childs[char]
        return self.is_complete_word

    def find_strings(self, prefix):
        results = []
        if self.is_complete_word:
            results.append(prefix)

        for char in self.childs:
            results.extend(self.childs[char].find_strings(prefix + char))
        return results

    def longest_prefix(self, prefix):
        for pf in prefix:
            if pf not in self.childs.keys():
                return
            self = self.childs[pf]
        return self.find_strings(prefix)

    def remove_string_helper(self, string, pnode, index):
        if pnode:
            flag = False
            if index < len(string):
                flag = self.remove_string_helper(string, pnode.childs.get(string[index]),
                                                 index + 1)

            if index == len(string) and pnode.is_complete_word:
                pnode.is_complete_word = False
                return len(pnode.childs) == 0

            if flag:
                pnode.childs.pop(string[index])
                return len(self.childs) == 0

        return False

    def remove_string(self, string):
        return self.remove_string_helper(string, self.childs.get(string[0]), 1)


if __name__ == "__main__":
    root = TrieNode()

    root.insert_string("abcd")
    root.insert_string("abc")
    root.insert_string("dag")
    root.insert_string("abcl")
    root.insert_string("dagr")
    root.insert_string("himansu")
    root.insert_string("himanshu")
    root.insert_string("him")
    root.insert_string("hi")

    # print(root.is_present("da"))
    print(root.find_strings(""))
    # print(root.longest_prefix("ab"))

    print(root.remove_string("abcl"))
    print(root.find_strings(""))
