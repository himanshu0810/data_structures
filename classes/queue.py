

class Queue:
    def __init__(self):
        self.top = -1
        self.elements = []

    def enqueue(self, value):
        self.elements.append(value)
        self.top = self.top + 1

    def dequeue(self):
        if self.is_empty():
            return
        element = self.elements[0]
        self.elements.remove(element)
        self.top = self.top - 1
        return element

    def peek(self):
        if self.is_empty():
            return
        return self.elements[0]

    def is_empty(self):
        return len(self.elements) == 0


if __name__ == "__main__":
    queue = Queue()
    queue.enqueue(10)
    queue.enqueue(1)
    queue.enqueue(4)
    queue.enqueue(12)
    queue.dequeue()
    queue.dequeue()
    queue.enqueue(100)
    print(queue.elements)
    print(queue.is_empty())
