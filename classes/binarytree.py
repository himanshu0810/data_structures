
class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BinaryTree:
    def __init__(self):
        self.head = None

    def insert_helper(self, queue, data):
        if len(queue) == 0:
            return

        node = queue.pop(0)
        if node.left is None:
            node.left = Node(data)
            queue = []
            return
        else:
            queue.append(node.left)
        if node.right is None:
            node.right = Node(data)
            queue = []
            return
        else:
            queue.append(node.right)
        self.insert_helper(queue, data)


    def insert(self, data):
        if not self.head:
            self.head = Node(data)
            return

        current_list = [self.head]
        self.insert_helper(current_list, data)

    def inorder(self, root):
        if root.left is not None:
            self.inorder(root.left)
        if root.right is not None:
            self.inorder(root.right)
        print(root.data, end=' ')


if __name__ == "__main__":
    bt = BinaryTree()
    bt.insert(10)
    bt.insert(1)
    bt.insert(12)
    bt.insert(3)
    bt.insert(14)
    bt.insert(34)
    bt.insert(7)

    print(bt.head.data)
    bt.inorder(bt.head)
