
# priority_queue_using_unordered_array
# Insert - O(1)
# Delete - O(n)
# Max - O(n)


class PriorityQueue:
    def __init__(self):
        self.array = []

    def insert(self, data):
        self.array.append(data)
        return self.array

    def get_highest_priority(self):
        return max(self.array)

    def delete_highest_priority(self):
        max_element = max(self.array)
        self.array.remove(max_element)
        return self.array

    def print(self):
        print(self.array)


if __name__ == "__main__":
    pq = PriorityQueue()
    pq.insert(20)
    pq.insert(3)
    pq.insert(1)
    pq.insert(10)
    pq.insert(16)
    pq.insert(12)
    print(pq.get_highest_priority())
    pq.delete_highest_priority()
    pq.print()
