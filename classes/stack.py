

class Stack:
    def __init__(self):
        self.top = -1
        self.elements = []

    def push(self, value):
        self.elements.append(value)
        self.top = self.top + 1

    def peek(self):
        if self.is_empty():
            return
        return self.elements[self.top]

    def is_empty(self):
        return len(self.elements) == 0

    def pop(self):
        element = self.elements[self.top]
        self.top = self.top - 1
        self.elements.pop()
        return element


if __name__ == "__main__":
    st = Stack()
    st.push(10)
    st.push(1)
    st.push(4)
    st.push(12)
    print(st.peek())
    st.pop()
    st.pop()
    print(st.elements)
    st.push(12)
    st.push(13)
    print(st.elements)
