

class Node:
    def __init__(self, data):
        self.data = data
        self.rank = 0
        self.parent = None


class DisjointSets:
    def __init__(self):
        self.map = {}

    def make_set(self, data):
        node = Node(data)
        node.parent = node
        self.map[data] = node
        return node

    def union_by_rank(self, data_1, data_2):
        node1 = self.map[data_1]
        node2 = self.map[data_2]

        parent1 = self.find_set(node1)
        parent2 = self.find_set(node2)

        if parent1.data == parent2.data:
            return

        if parent1.rank >= parent2.rank:
            parent2.parent = parent1
            parent1.rank = parent1.rank + 1 if parent1.rank == parent2.rank else parent1.rank
        else:
            parent1.parent = parent2

    def find_set(self, node):
        parent = node.parent
        if parent == node:
            return parent
        node.parent = self.find_set(node.parent)
        return node.parent

    def find_set_by_data(self, data):
        node = self.map[data]
        parent = self.find_set(node)
        return parent.data


if __name__ == "__main__":
    ds = DisjointSets()
    ds.make_set(1)
    ds.make_set(2)
    ds.make_set(3)
    ds.make_set(4)
    ds.make_set(5)
    ds.make_set(6)
    ds.make_set(7)

    ds.union_by_rank(1, 2)
    ds.union_by_rank(2, 3)
    ds.union_by_rank(4, 5)
    ds.union_by_rank(6, 7)
    ds.union_by_rank(5, 6)
    ds.union_by_rank(3, 7)

    print(ds.find_set_by_data(1))
    print(ds.find_set_by_data(2))
    print(ds.find_set_by_data(3))
    print(ds.find_set_by_data(4))
    print(ds.find_set_by_data(5))
    print(ds.find_set_by_data(6))
    print(ds.find_set_by_data(7))
