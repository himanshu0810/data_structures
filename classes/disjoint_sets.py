
class Edge:
    def __init__(self, src=None, dst=None):
        self.src = src
        self.dst = dst

    def __repr__(self):
        return str(self.src) + "->" + str(self.dst)


class Graph:
    def __init__(self):
        self.edges = []
        self.vertices = set()

    def add_edge(self, src, dst):
        edge = Edge(src, dst)
        self.edges.append(edge)
        self.vertices.add(src)
        self.vertices.add(dst)
        return edge

    def get_all_edges(self):
        return self.edges

    def get_all_vertices(self):
        return self.vertices


class DisjointSet:
    def __init__(self):
        self.initialize_sets = {}
        self.sets = []

    def find_parent(self, vertex):
        if self.initialize_sets[vertex] == -1:
            return vertex

        return self.find_parent(self.initialize_sets[vertex])

    def union_and_merge(self, src_parent, dst_parent):
        self.initialize_sets[src_parent] = dst_parent

    def is_cyclic(self, graph):
        edges = graph.get_all_edges()
        for each in graph.get_all_vertices():
            self.initialize_sets[each] = -1

        for each in edges:
            src_parent = self.find_parent(each.src)
            dst_parent = self.find_parent(each.dst)
            if src_parent == dst_parent:
                return True

            self.union_and_merge(src_parent, dst_parent)

        return False


if __name__ == "__main__":
    graph = Graph()

    graph.add_edge(0, 1)
    graph.add_edge(6, 2)
    graph.add_edge(0, 2)

    edges = graph.get_all_edges()
    # print("Edges are ", edges, graph.get_all_vertices())
    disjoint_sets = DisjointSet()
    is_cycle = disjoint_sets.is_cyclic(graph)
    print("Graph is cyclic or not - ", is_cycle)
