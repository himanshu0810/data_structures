
# Palindrome String - Completed


def check_palindrome_string(st1):
    l = 0
    h = len(st1)-1
    while h > l:
        if st1[l] != st1[h]:
            return False
        l = l + 1
        h = h - 1
    return True


if __name__ == "__main__":
    st1 = "abccba"
    print("String is palindrome or not ", check_palindrome_string(st1))
