
# Minimum Steps -
import copy
from pprint import pprint


def return_number_of_steps_util(number_of_steps, paths, current_step):
    if current_step > number_of_steps:
        return None

    if current_step == number_of_steps:
        return paths

    if not paths:
        paths = [current_step]

    total_paths = []
    for i in range(1, 4):
        paths_copy = paths.copy()
        last_step = paths_copy[-1]
        paths_copy.append(last_step+i)
        p = return_number_of_steps_util(number_of_steps, copy.deepcopy(paths_copy), current_step + i)
        if p:
            if type(p[0]) is list:
                for each in p:
                    total_paths.append(each)
            else:
                total_paths.append(p)
    return total_paths


def return_number_of_steps(number_of_steps):
    paths = []
    return return_number_of_steps_util(number_of_steps, paths, 0)


def countways(n):
    if n < 0:
        return 0
    elif n == 0:
        return 1
    else:
        return countways(n-1) + countways(n-2) + countways(n-3)

if __name__ == "__main__":
    number_of_steps = 5
    print("Print minimum number of steps - ")
    paths = return_number_of_steps(number_of_steps)
    pprint(paths)
    print(len(paths))
    print(countways(number_of_steps))


