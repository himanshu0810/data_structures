
# Alternatively reverse linked list in group of k -


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

    def __repr__(self):
        return str(self.data)


class LinkedList:
    def __init__(self):
        self.head = None

    def print(self):
        node = self.head
        while node is not None:
            print(node.data, end=' ')
            node = node.next

    def insert(self, element):
        if not self.head:
            self.head = Node(element)
            return

        node = self.head
        while node.next is not None:
            node = node.next

        node.next = Node(element)
        return

    def reverse_linked_list_in_k(self, k):
        new_head = None
        node = self.head
        while node is not None:
            head, node = self.reverse_linked_list_in_k_util(node, k, 1)
            if node == head:
                break
            if not new_head:
                new_head = head

            node = node.next
        return new_head

    def reverse_linked_list_in_k_util(self, node, k, no):
        if node.next is None:
            return node, node
        if k == no:
            return node, node

        new_head, node1 = self.reverse_linked_list_in_k_util(node.next, k, no+1)
        temp = node.next.next
        node.next.next = node
        node.next = temp
        return new_head, node




if __name__ == "__main__":
    linked_list = LinkedList()
    linked_list.insert(1)
    linked_list.insert(2)
    linked_list.insert(3)
    linked_list.insert(4)
    linked_list.insert(5)
    linked_list.insert(6)
    linked_list.insert(7)
    linked_list.insert(8)
    linked_list.insert(9)

    linked_list.print()
    print()
    linked_list.head = linked_list.reverse_linked_list_in_k(3)
    linked_list.print()

