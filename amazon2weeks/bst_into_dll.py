
# Convert tree into list - Completed


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.data)



def print_list(list):
    node = list.head
    while node.right is not None:
        print(node.data, end=' ')
        node = node.right

    print(node.data)

    while node is not None:
        print(node.data, end=' ')
        node = node.left


def merge_two_linked_list(list1, list2):
    if list1 is None and list2 is None:
        return
    elif list1 is not None and list2 is None:
        return list1
    elif list1 is None and list2 is not None:
        return list2
    else:
        list1_last = list1
        while list1_last.right is not None:
            list1_last = list1_last.right

    list1_last.right = list2
    list2.left = list1_last
    return list1


class BinaryTree:
    def __init__(self):
        self.root = None

    def inorder(self, root):
        if root:
            self.inorder(root.left)
            print(root.data, end=' ')
            self.inorder(root.right)


def convert_tree_into_list(root):
    if root is None:
        return

    merged_left = convert_tree_into_list(root.left)
    merged_right = convert_tree_into_list(root.right)
    root.left = root
    root.right = root

    return merge_two_linked_list(merge_two_linked_list(merged_left, root), merged_right)


if __name__ == "__main__":

    print("merged list")

    tree = BinaryTree()
    tree.root = Node(10)

    tree.root.left = Node(12)
    tree.root.right = Node(15)

    tree.root.left.left = Node(25)
    tree.root.left.right = Node(30)

    tree.root.right.left = Node(36)

    tree.inorder(tree.root)

    m = convert_tree_into_list(tree.root)
    # print_list(m)
