
# Merge Sort - Completed


def return_merged_arr(left_arr, right_arr):
    merged_arr = []
    left_idx = right_idx = 0
    while left_idx < len(left_arr):
        if right_idx == len(right_arr):
            merged_arr.append(left_arr[left_idx])
            left_idx = left_idx + 1
        elif left_arr[left_idx] < right_arr[right_idx]:
            merged_arr.append(left_arr[left_idx])
            left_idx = left_idx + 1
        else:
            merged_arr.append(right_arr[right_idx])
            right_idx = right_idx + 1

    while right_idx < len(right_arr):
        merged_arr.append(right_arr[right_idx])
        right_idx = right_idx + 1

    return merged_arr


def return_sorted_array(arr):
    if len(arr) == 1:
        return arr

    mid = len(arr) // 2
    left_arr = return_sorted_array(arr[:mid])
    right_arr = return_sorted_array(arr[mid:])
    merged_arr = return_merged_arr(left_arr, right_arr)
    return merged_arr


if __name__ == "__main__":
    arr = [38, 27, 43, 3, 9, 82, 10]
    print("Sorted array is ", return_sorted_array(arr))
