
# Quick Sort - Completed


def partition_array(ar, start, end):

    pivot = ar[end]
    p_index = start
    for i in range(start, end):
        if ar[i] < pivot:
            ar[i], ar[p_index] = ar[p_index], ar[i]
            p_index = p_index + 1

    ar[p_index], ar[end] = ar[end], ar[p_index]
    return p_index


def return_sorted_array_util(ar, start, end):
    if start < end:
        pivot_position = partition_array(ar, start, end)
        return_sorted_array_util(ar, start, pivot_position-1)
        return_sorted_array_util(ar, pivot_position+1, len(ar)-1)


def return_sorted_array(ar):
    return_sorted_array_util(ar, 0, len(ar)-1)
    return ar


if __name__ == "__main__":
    ar = [10, 1, 4, 9, 13]
    print(return_sorted_array(ar))
