
# Bucket Sort - Completed
from amazon2weeks.insertion_sort import return_sorted_array


def return_sorted_array_1(arr):
    no_of_buckets = 10
    b = []
    for _ in range(no_of_buckets):
        b.append([])

    for each in arr:
        b[each // no_of_buckets].append(each)

    for idx, each in enumerate(b):
        b[idx] = return_sorted_array(each)

    k = 0
    for each in b:
        for j in each:
            arr[k] = j
            k = k + 1

    return arr


if __name__ == "__main__":
    arr = [29, 25, 3, 49, 9, 37, 21, 43]
    print("Sorted array is", return_sorted_array_1(arr))
