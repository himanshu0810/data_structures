
# Inorder Predecessor - Completed


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class Tree:
    def __init__(self):
        self.root = None

    def inorder(self, node):
        if node:
            self.inorder(node.left)
            print(node.data, end=' ')
            self.inorder(node.right)

    def find_inorder_predecessor_successor(self, key):
        print(self.find_inorder_predecessor_util(key, self.root, None, None))

    def find_inorder_predecessor_util(self, key, root, prede, succe):
        if root is None:
            return prede, succe

        if key == root.data:
            if root.left is None:
                prede = -1
            else:
                root = root.left
                while root.right is not None:
                    root = root.right
                prede = root.data

            if root.right is None:
                succe = -1
            else:
                root = root.right
                while root.left is not None:
                    root = root.left
                succe = root.data
            return prede, succe

        if key < root.data:
            return self.find_inorder_predecessor_util(key, root.left, prede, root.data)

        return self.find_inorder_predecessor_util(key, root.right, root.data, succe)


if __name__ == "__main__":
    tree = Tree()
    tree.root = Node(50)

    tree.root.left = Node(30)
    tree.root.right = Node(70)

    tree.root.left.left = Node(20)
    tree.root.left.right = Node(40)

    tree.root.right.left = Node(60)
    tree.root.right.right = Node(80)

    tree.find_inorder_predecessor_successor(21)
