
# No of islands -



def return_no_of_islands_util(row, col, matrix, visited, count, continuation):

    visited.append(vertex)
    print(vertex.data, end=' ')

    for child in self.graph[vertex]:
        if child in visited:
            continue

        visited.append(vertex)
        return_no_of_islands_util(visited, child)









def return_no_of_islands(mat):
    count = 0
    visited = []
    rows = [-1, 0, 1, -1, 1, -1, 0, 1]
    cols = [-1, -1, -1, 0, 0, 1, 1, 1]
    return return_no_of_islands_util(0, 0, mat, visited, count, False)


if __name__ == "__main__":
    mat = [
        [1, 1, 0, 0, 0],
        [0, 1, 0, 0, 1],
        [1, 0, 0, 1, 1],
        [0, 0, 0, 0, 0],
        [1, 0, 1, 0, 1]
    ]
    print("No of islands - ", return_no_of_islands(mat))

