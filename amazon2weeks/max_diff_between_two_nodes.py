
# Max difference between node and its ancestors
import sys


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.data)


class BinaryTree:
    def __init__(self):
        self.root = None

    def inorder(self):
        self.inorder_util(self.root)

    def inorder_util(self, root):
        if root:
            self.inorder_util(root.left)
            print(root.data, end=' ')
            self.inorder_util(root.right)

    def max_difference(self):
        nodes, max_diff = self.max_difference_util(self.root, None, [], [-sys.maxsize])
        print(max_diff, nodes)

    def max_difference_util(self, node, prev_node, nodes, max_diff):
        if node is None:
            return nodes, max_diff

        if prev_node:
            if prev_node.data - node.data > max_diff[0]:
                max_diff[0] = prev_node.data - node.data
                if not nodes:
                    nodes.append((node, prev_node))
                else:
                    nodes[0] = (node, prev_node)

        if prev_node is None:
            prev_node = node
        elif node.data > prev_node.data:
            prev_node = node

        self.max_difference_util(node.left, prev_node, nodes, max_diff)
        self.max_difference_util(node.right, prev_node, nodes, max_diff)
        return nodes, max_diff

    def return_min_height(self):
        if self.root is None:
            return

        node = self.root
        min_height = self.return_min_height_util(node, 0, [sys.maxsize + 1 ])
        print(min_height)

    def return_min_height_util(self, node, level, result):
        if node is None:
            return result

        if node.left is None and node.right is None:
            if result[0] > level:
                result[0] = level
            return result

        self.return_min_height_util(node.left, level+1, result)
        self.return_min_height_util(node.right, level+1, result)
        return result


if __name__ == "__main__":
    bt = BinaryTree()

    bt.root = Node(8)

    bt.root.left = Node(3)
    bt.root.left.left = Node(1)
    bt.root.left.right = Node(6)
    bt.root.left.right.left = Node(4)
    bt.root.left.right.right = Node(7)

    bt.root.right = Node(10)
    bt.root.right.right = Node(14)
    bt.root.right.right.left = Node(13)


    bt.inorder()
    bt.max_difference()
    print()
    bt.return_min_height()
