
# Selection Sort - Completed


def return_sorted_array(arr):

    for i in range(len(arr)):
        min = min_idx = None

        for j in range(i, len(arr)):
            if min is None or min > arr[j]:
                min = arr[j]
                min_idx = j

        arr[min_idx], arr[i] = arr[i], arr[min_idx]
    return arr


if __name__ == "__main__":
    arr = [10, 1, 3, 9, 7]
    print("Sorted array is ", return_sorted_array(arr))
