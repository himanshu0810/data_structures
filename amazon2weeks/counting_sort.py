
# Counting Sort - Complete


def return_sorted_array(ar):
    counting_array = [0 for _ in range(10)]

    for each in ar:
        counting_array[each] = counting_array[each] + 1

    for idx, i in enumerate(counting_array):
        if idx == 0:
            continue

        counting_array[idx] = counting_array[idx] + counting_array[idx-1]

    sorted_array = [None for _ in range(len(ar))]
    for idx, each in enumerate(ar):
        sorted_array[counting_array[each]-1] = each
        counting_array[each] = counting_array[each] - 1

    return sorted_array


if __name__ == "__main__":
    ar = [1, 4, 1, 2, 7, 5, 2]
    print("Sorted array is with counting sort is ", return_sorted_array(ar))
