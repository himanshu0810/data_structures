
# Min Heap - Complete
from pprint import pprint


class Node:
    def __init__(self, key, weight):
        self.key = key
        self.weight = weight

    def __repr__(self):
        return str(self.key) + "->" + str(self.weight)


class MinHeap:
    def __init__(self):
        self.elements = []
        self.position = -1
        self.pos_dict = {}

    @staticmethod
    def get_parent_index(i):
        return (i-1) // 2

    @staticmethod
    def get_left_child_index(i):
        return 2 * i + 1

    @staticmethod
    def get_right_child_index(i):
        return 2 * i + 2

    def has_parent(self, i):
        return self.get_parent_index(i) > -1

    def has_left_child(self, i):
        return self.get_left_child_index(i) < len(self.elements)

    def has_right_child(self, i):
        return self.get_right_child_index(i) < len(self.elements)

    def get_parent(self, i):
        if not self.has_parent(i):
            return
        return self.elements[self.get_parent_index(i)]

    def get_left_child(self, i):
        if not self.has_left_child(i):
            return
        return self.elements[self.get_left_child_index(i)]

    def get_right_child(self, i):
        if not self.has_right_child(i):
            return
        return self.elements[self.get_right_child_index(i)]

    def insert(self, key, weight):
        node = Node(key, weight)
        self.elements.append(node)
        self.position = self.position + 1
        self.pos_dict[node.key] = self.position
        self.heapify_up(self.position)

    def swap(self, idx, idx1):
        self.elements[idx], self.elements[idx1] = self.elements[idx1], self.elements[idx]
        self.pos_dict[self.elements[idx].key] = idx
        self.pos_dict[self.elements[idx1].key] = idx1
        return self.elements

    def get_element(self, index):
        return self.elements[index]

    def heapify_up(self, index):
        while self.has_parent(index) and self.get_parent(index).weight > \
                self.get_element(index).weight:
            self.swap(self.get_parent_index(index), index)
            index = self.get_parent_index(index)

    def print_heap(self):
        pprint(self.elements)

    def extract_min(self):
        element = self.get_element(0)
        self.swap(0, len(self.elements)-1)
        self.pos_dict.pop(element.key)
        self.position = self.position - 1
        self.elements.pop()
        self.heapify_down(0)
        return element

    def heapify_down(self, index):

        while self.has_left_child(index):
            smaller_child_index = self.get_left_child_index(index)
            if self.has_right_child(index) and self.get_right_child(index).weight < \
                    self.get_left_child(index).weight:
                smaller_child_index = self.get_right_child_index(index)

            if self.get_element(smaller_child_index).weight > self.get_element(index).weight:
                break

            self.swap(smaller_child_index, index)
            index = smaller_child_index

    def decrease_key(self, key, new_value):
        position = self.pos_dict[key]
        node = self.elements[position]
        node.weight = new_value
        self.heapify_up(position)

    def is_empty(self):
        return len(self.elements) == 0

    def check_key_in_min_heap(self, key):
        return key in self.pos_dict

#
# if __name__ == "__main__":
#     min_heap = MinHeap()
#     min_heap.insert(0, 0)
#     min_heap.insert(7, 10)
#     min_heap.insert(1, 4)
#     min_heap.insert(2, 6)
#     min_heap.insert(8, 7)
#     min_heap.insert(6, 8)
#
#     min_heap.print_heap()
#     min_heap.extract_min()
#     min_heap.extract_min()
#     min_heap.print_heap()
#     print(min_heap.pos_dict)
#     min_heap.decrease_key(8, 2)
#     min_heap.print_heap()
