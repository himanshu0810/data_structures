from collections import defaultdict

import sys

from amazon2weeks.min_heap import MinHeap


class Graph:
    def __init__(self):
        self.graph = defaultdict(list)
        self.vertices = set()

    def add_edge(self, src, dst, weight):
        self.graph[src].append([dst, weight])
        self.vertices.add(src)
        self.vertices.add(dst)
        return

    def print_graph(self):
        for k, v in self.graph.items():
            print(k, "-->", v)

    def prim_algo(self):

        min_heap = MinHeap()
        for v in self.vertices:
            min_heap.insert(v, sys.maxsize+1)

        mst = {}
        min_heap.decrease_key(0, 0)
        min_heap.print_heap()
        while not min_heap.is_empty():
            extract_min = min_heap.extract_min()
            key, weight = extract_min.key, extract_min.weight

            for child, cweight in self.graph[key]:
                if child not in min_heap.pos_dict:
                    continue

                child_position = min_heap.pos_dict[child]
                child_element = min_heap.elements[child_position]
                if child_element.weight < weight + cweight:
                    continue

                mst[child] = [extract_min, cweight]
                min_heap.decrease_key(child_element.key, weight+cweight)

        self.print_arr(mst)

    @staticmethod
    def print_arr(mst):

        for k, v in mst.items():
            print(v[0].key, "-->", k, "--", v[1])


if __name__ == "__main__":
    graph = Graph()
    graph.add_edge(0, 1, 4)
    graph.add_edge(1, 0, 4)
    graph.add_edge(0, 7, 8)
    graph.add_edge(7, 0, 8)
    graph.add_edge(1, 2, 8)
    graph.add_edge(2, 1, 8)
    graph.add_edge(1, 7, 11)
    graph.add_edge(7, 1, 11)
    graph.add_edge(7, 8, 7)
    graph.add_edge(8, 7, 7)
    graph.add_edge(7, 6, 1)
    graph.add_edge(6, 7, 1)
    graph.add_edge(6, 8, 6)
    graph.add_edge(8, 6, 6)
    graph.add_edge(2, 8, 2)
    graph.add_edge(8, 2, 2)
    # graph.print_graph()
    graph.prim_algo()
