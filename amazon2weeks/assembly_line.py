

def return_generated_matrix(k):
    ma = [[None for _ in range(k)] for _ in range(k)]
    squared_numbers = [i+1 for i in range(k*k)]

    iter = 0
    left_idx = 0
    right_idx = k -1
    top = 0
    bottom = k - 1
    while left_idx <= right_idx or top <= bottom:
        for i in range(left_idx, right_idx+1):
            ma[top][i] = squared_numbers[iter]
            iter = iter + 1

        for i in range(top + 1, bottom+1):
            ma[i][right_idx] = squared_numbers[iter]
            iter = iter + 1

        for i in range(right_idx - 1, left_idx-1, -1):
            ma[bottom][i] = squared_numbers[iter]
            iter = iter + 1

        for i in range(bottom-1, top, -1):
            ma[i][left_idx] = squared_numbers[iter]
            iter = iter + 1

        left_idx = left_idx + 1
        top = top + 1
        bottom = bottom - 1
        right_idx = right_idx - 1



    print(squared_numbers)
    print(ma)


if __name__ == "__main__":
    A = 3
    print(return_generated_matrix(A))
