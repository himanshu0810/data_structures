
# Permutations of a string - Completed
from copy import copy


def return_permutations_of_a_string_util(input_list, current_str, all_strings):
    if not input_list:
        all_strings.append(current_str)

    for each in input_list:
        in1 = copy(input_list)
        in1.remove(each)
        return_permutations_of_a_string_util(in1, current_str + each, all_strings)
    return all_strings


def return_permutations_of_a_string(input_str):
    return return_permutations_of_a_string_util(list(input_str), '', [])


if __name__ == "__main__":
    input_str = "ABC"
    print("Generating all permutations of this string")
    print(return_permutations_of_a_string(input_str))
