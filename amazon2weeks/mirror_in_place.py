

# Mirror in place - Completed


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class Tree:
    def __init__(self):
        self.root = None

    def inorder(self, root):
        if root:
            self.inorder(root.left)
            print(root.data, end=' ')
            self.inorder(root.right)

    def return_mirror_tree_in_place(self, root):
        if root is None:
            return

        if root.left:
            self.return_mirror_tree_in_place(root.left)

        if root.right:
            self.return_mirror_tree_in_place(root.right)

        root.left, root.right = root.right, root.left
        return root


if __name__ == "__main__":
    tree = Tree()

    tree.root = Node(1)

    tree.root.left = Node(3)
    tree.root.right = Node(2)

    tree.root.right.left = Node(5)
    tree.root.right.right = Node(4)

    tree.inorder(tree.root)
    print()
    tree.return_mirror_tree_in_place(tree.root)
    tree.inorder(tree.root)