
# Buy and Sell Stocks One Chance - Completed


def return_max_profit(arr):
    min = None
    profit = 0
    for each in arr:
        if not min:
            min = each
            continue

        inst = each - min
        if each > min and profit < inst:
            profit = inst
        elif each < min:
            min = each

    return profit


if __name__ == "__main__":
    arr = [100, 180, 260, 310, 40, 535, 695]
    print("Maximum profit is ", return_max_profit(arr))
