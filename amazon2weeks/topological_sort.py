
# Topological sort - o(V+E)


from collections import defaultdict


class Graph:
    def __init__(self):
        self.graph = defaultdict(list)
        self.vertices = set()

    def add_edge(self, src, dst):
        self.graph[src].append(dst)
        self.vertices.add(src)
        self.vertices.add(dst)
        return

    def topological_sort(self):
        visited = []
        stack = []
        for v in self.vertices:
            if v not in visited:
                self.topological_sort_util(v, visited, stack)
                print()

        print(stack)

    def topological_sort_util(self, vertex, visited, stack):

        visited.append(vertex)

        for child in self.graph[vertex]:
            if child not in visited:
                self.topological_sort_util(child, visited, stack)

        stack.insert(0, vertex)



if __name__ == "__main__":
    graph = Graph()
    graph.add_edge(4, 1)
    graph.add_edge(2, 3)
    graph.add_edge(3, 1)
    graph.add_edge(5, 2)
    graph.add_edge(5, 0)
    graph.add_edge(4, 0)


    graph.topological_sort()
