
# Count all bst in sorted array - Complete


def return_count(arr):
    t = [1]
    for i in range(1, len(arr)+1):
        t.append(0)
        for j in range(i):
            l = i - j - 1
            t[i] += t[j] * t[l]

    return t[len(arr)]


if __name__ == "__main__":
    arr = [10, 11, 12, 13, 14]
    print("Total BSTs can be formed using this array ", return_count(arr))
