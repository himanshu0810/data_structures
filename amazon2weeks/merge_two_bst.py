
class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.data)


class Stack:
    def __init__(self):
        self.top = -1
        self.elements = []

    def __repr__(self):
        return str(self.elements)

    def is_empty(self):
        return self.top == -1

    def pop(self):
        if self.is_empty():
            return

        element = self.elements.pop()
        self.top = self.top - 1
        return element

    def peek(self):
        if self.is_empty():
            return
        return self.elements[self.top].data

    def push(self, element):
        if not element:
            return
        self.elements.append(element)
        self.top = self.top + 1
        return element


class BinaryTree:
    def __init__(self):
        self.root = None

    def preorder(self, node):
        if node:
            self.preorder(node.left)
            print(node.data, end=' ')
            self.preorder(node.right)


def merge_and_print_two_bst(tree_1, tree_2):
    st1 = Stack()
    st2 = Stack()
    merge_and_print_two_bst_util(tree_1.root, tree_2.root, st1, st2)


def merge_and_print_two_bst_util(tree_1, tree_2, st1, st2):
    if not tree_1 and not tree_2:
        return
    elif tree_1 and not tree_2:
        st1.push(tree_1)
        merge_and_print_two_bst_util(tree_1.left, tree_2, st1, st2)
    elif tree_2 and not tree_1:
        st2.push(tree_2)
        merge_and_print_two_bst_util(tree_1, tree_2.left, st1, st2)
    else:
        st1.push(tree_1)
        st2.push(tree_2)
        merge_and_print_two_bst_util(tree_1.left, tree_2.left, st1, st2)

    while not st1.is_empty() or not st2.is_empty():
        if not st1.is_empty() and not st2.is_empty():
            if st1.peek() < st2.peek():
                node = st1.pop()
                print(node.data, end=' ')
                merge_and_print_two_bst_util(node.right, None, st1, st2)
            else:
                node = st2.pop()
                print(node.data, end=' ')
                merge_and_print_two_bst_util(None, node.right, st1, st2)
        elif not st1.is_empty() and st2.is_empty():
            node = st1.pop()
            print(node.data, end=' ')
            merge_and_print_two_bst_util(node.right, None, st1, st2)
        elif st1.is_empty() and not st2.is_empty():
            node = st2.pop()
            print(node.data, end=' ')
            merge_and_print_two_bst_util(None, node.right, st1, st2)


if __name__ == "__main__":
    binary_tree = BinaryTree()
    binary_tree.root = Node(8)
    binary_tree.root.left = Node(2)
    binary_tree.root.left.left = Node(1)
    binary_tree.root.right = Node(10)

    binary_tree1 = BinaryTree()
    binary_tree1.root = Node(5)
    binary_tree1.root.left = Node(3)
    binary_tree1.root.left.left = Node(0)

    print("Printing first tree")
    binary_tree.preorder(binary_tree.root)
    print()
    print("Printing second tree")
    binary_tree1.preorder(binary_tree1.root)

    print()
    merge_and_print_two_bst(binary_tree, binary_tree1)
