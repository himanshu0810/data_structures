
# Insertion Sort - O(n2)


def return_sorted_array(ar):

    for i in range(len(ar)):

        j = i
        while j > 0:
            if ar[j] > ar[j-1]:
                break
            ar[j], ar[j-1] = ar[j-1], ar[j]
            j = j - 1

    return ar


if __name__ == "__main__":
    ar = [10, 1, 4, 9, 13]
    print(return_sorted_array(ar))
