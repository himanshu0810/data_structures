
# Complete - Union Find


from collections import defaultdict


class Graph:
    def __init__(self):
        self.graph = defaultdict(list)
        self.vertices = set()

    def add_edge(self, src, dst):
        self.vertices.add(src)
        self.vertices.add(dst)
        self.graph[src].append(dst)
        return

    def print_graph(self):
        for key, value in self.graph.items():
            print(key, "-->", value)

    def find_subset(self, parent, vertex):
        if parent[vertex] == -1:
            return vertex

        while parent[vertex] != -1:
            vertex = parent[vertex]
        return vertex

    def union_set(self, parent, parent_subset, child_subset):
        x = self.find_subset(parent, parent_subset)
        y = self.find_subset(parent, child_subset)
        parent[x] = y
        return y

    def is_cyclic(self):

        parent = [-1 for _ in range(len(self.vertices))]
        for vertex in self.vertices:
            parent_subset = self.find_subset(parent, vertex)

            for each in self.graph[vertex]:
                child_subset = self.find_subset(parent, each)
                if parent_subset == child_subset:
                    return True

                parent_subset = self.union_set(parent, parent_subset, child_subset)

        return False


if __name__ == "__main__":
    graph = Graph()
    graph.add_edge(0, 1)
    graph.add_edge(1, 2)
    graph.add_edge(2, 3)
    print("Printing the graph ")
    graph.print_graph()

    print("Check whether the graph has cycle or not - ", graph.is_cyclic())


