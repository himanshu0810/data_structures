
# Find all Paths between two nodes - Completed


from collections import defaultdict

from copy import copy


class Graph:
    def __init__(self):
        self.graph = defaultdict(list)
        self.vertices = set()

    def add_edge(self, src, dst):
        self.vertices.add(src)
        self.vertices.add(dst)
        self.graph[src].append(dst)

    def print_graph(self):
        for k, v in self.graph.items():
            print(k, "--->", v)

    def return_all_paths(self, src, dst):
        return self.return_all_paths_util(dst, src, [], [], {})

    def return_all_paths_util(self, dst, current_vertex, current_path, paths, visited):
        if current_vertex == dst:
            current_path.append(dst)
            paths.append(current_path)
            return paths

        visited[current_vertex] = True
        current_path.append(current_vertex)

        for vertex in self.graph[current_vertex]:
            if vertex in visited:
                continue

            self.return_all_paths_util(dst, vertex, copy(current_path), paths, visited)

        visited.pop(current_vertex)
        return paths


if __name__ == "__main__":
    graph = Graph()
    graph.add_edge(0, 1)
    graph.add_edge(0, 2)
    graph.add_edge(0, 3)
    graph.add_edge(1, 3)
    graph.add_edge(2, 0)
    graph.add_edge(2, 1)

    graph.print_graph()
    print("Printing all paths")
    print(graph.return_all_paths(0, 3))