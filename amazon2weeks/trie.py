
# Trie - insertion and print words, removal, prefix all words - Completed


class Node:
    def __init__(self, ch):
        self.ch = ch
        self.children = {}
        self.is_end_of_word = False

    def __repr__(self):
        return str(self.ch)


class Trie:
    def __init__(self):
        self.root = None

    def insert(self, data):

        root_node = self.root
        for ch in data:
            node = Node(ch)
            if self.root is None:
                root_node = Node("*")
                self.root = root_node
                root_node.children[ch] = node
                root_node = node
                continue

            if ch in root_node.children.keys():
                root_node = root_node.children[ch]
                continue
            else:
                root_node.children[ch] = node
                root_node = node

        root_node.is_end_of_word = True
        return

    def print_all_words_util(self, node, all_strings, current_str):
        if node is None:
            return all_strings

        for ch in node.children.keys():
            if current_str is None:
                self.print_all_words_util(node.children[ch], all_strings, ch)
            else:
                self.print_all_words_util(node.children[ch], all_strings, current_str+ch)

        if node.is_end_of_word:
            all_strings.append(current_str)

        return all_strings

    def print_all_words(self):
        all_strings = self.print_all_words_util(self.root, [], None)
        print(all_strings)

    def remove_a_word(self, word):
        self.remove_a_word_util(word, 0, self.root)

    def remove_a_word_util(self, word, idx, node):
        if len(word) == idx:
            node.is_end_of_word = False
            return bool(node.children)

        ch = word[idx]
        if ch not in node.children:
            return True

        flag = self.remove_a_word_util(word, idx+1, node.children[ch])
        if flag:
            return True

        node.children.pop(ch)
        return bool(node.children) or node.is_end_of_word

    def return_words_from_prefix(self, prefix):
        node = self.root
        for ch in prefix:
            if ch not in node.children.keys():
                return False
            node = node.children[ch]
        all_strings = self.print_words_util([], prefix, node)
        print(all_strings)

    def print_words(self):
        all_strings = []
        current_str = ""
        node = self.root
        print(self.print_words_util(all_strings, current_str, node))

    def print_words_util(self, all_strings, current_str, node):
        if node is None:
            return all_strings

        for ch in node.children.keys():
            self.print_words_util(all_strings, current_str + ch, node.children[ch])

        if node.is_end_of_word:
            all_strings.append(current_str)

        return all_strings


if __name__ == "__main__":
    trie = Trie()
    trie.insert("hi")
    trie.insert("him")
    trie.insert("ab")
    trie.insert("abcd")
    trie.insert("abd")
    trie.insert("b")

    trie.print_words()
    # trie.remove_a_word("h")
    print("After removing the words")
    # trie.print_all_words()
    trie.return_words_from_prefix("c")
