
# N Queen problem - Completed


from pprint import pprint


def is_safe(matrix, row, col):
    for i in range(row):
        for k in range(len(matrix)):
            if k == col:
                if matrix[i][k] == 1:
                    return False
            elif abs((row - i) / (col - k)) == 1 and matrix[i][k] == 1:
                return False
    return True


def return_n_queen_position_util(matrix, row):
    if row == len(matrix):
        return True

    for j in range(len(matrix)):
        if not is_safe(matrix, row, j):
            continue

        matrix[row][j] = 1
        flag = return_n_queen_position_util(matrix, row + 1)
        if flag:
            return True

        matrix[row][j] = None

    return False


def return_n_queen_position(matrix):
    return_n_queen_position_util(matrix, 0)


if __name__ == "__main__":
    matrix = [[None for _ in range(8)] for _ in range(8)]
    pprint(matrix)
    return_n_queen_position(matrix)
    pprint(matrix)
