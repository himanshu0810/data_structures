
# Next greater Element - Completed


class Stack:
    def __init__(self):
        self.top = -1
        self.elements = []

    def push(self, data):
        self.elements.append(data)
        self.top = self.top + 1

    def is_empty(self):
        return self.top == -1

    def peek(self):
        if self.is_empty():
            return

        return self.elements[self.top]

    def pop(self):
        if self.is_empty():
            return
        element = self.elements.pop()
        self.top = self.top - 1
        return element


def print_next_greater_element(arr):
    st = Stack()
    for each in arr:
        if st.is_empty():
            st.push(each)
        elif st.peek() > each:
            st.push(each)
        else:
            while not st.is_empty() and st.peek() < each:
                element = st.pop()
                print(element, "-->", each)
            st.push(each)

    while not st.is_empty():
        element = st.pop()
        print(element, "-->", -1)


if __name__ == "__main__":
    arr = [4, 5, 2, 25]
    arr1 = [13, 7, 6, 12]
    print(print_next_greater_element(arr))
    print("Second Array")
    print(print_next_greater_element(arr1))
    print()
