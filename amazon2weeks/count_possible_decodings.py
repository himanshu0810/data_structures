from copy import copy


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

    def __repr__(self):
        return str(self.data)


class LinkedList:
    def __init__(self):
        self.head = None

    def insert(self, data):
        if not self.head:
            node = Node(data)
            self.head = node
            return node

        node = self.head
        while node.next is not None:
            node = node.next

        new_node = Node(data)
        node.next = new_node
        return new_node

    def print(self):
        node = self.head
        while node is not None:
            print(node.data, end=' ')
            node = node.next

    def remove(self):
        node = self.head

        nodes = self.remove_util(node, [], 0, [])
        print(nodes)

    def remove_util(self, node, nodes_list, sum, nodes_to_be_removed):
        if node is None:
            return nodes_to_be_removed

        if nodes_list and sum == 0:
            for each in nodes_list:
                if each in nodes_to_be_removed:
                    return nodes_to_be_removed

            for each in nodes_list:
                nodes_to_be_removed.append(each)
            return nodes_to_be_removed

        nodes_copy = copy(nodes_list)
        nodes_copy.append(node)
        self.remove_util(node.next, nodes_copy, sum + node.data, nodes_to_be_removed)
        self.remove_util(node.next, nodes_list, sum, nodes_to_be_removed)
        return nodes_to_be_removed


if __name__ == "__main__":
    lin = LinkedList()
    lin.insert(6)
    lin.insert(-6)
    lin.insert(3)
    lin.insert(2)
    lin.insert(1)
    lin.insert(-6)

    lin.remove()
