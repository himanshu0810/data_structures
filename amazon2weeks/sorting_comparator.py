# Python3 Program to get the maximum
# possible integer from given array
# of integers...


# custom comparator to sort according
# to the ab, ba as mentioned in description
def comparator(a, b):
    ab = str(a) + str(b)
    ba = str(b) + str(a)
    a1 = int(ba) > int(ab)
    b1 = int(ba) < int(ab)
    res = a1 - b1
    return res


def myCompare(mycmp):

    # Convert a cmp= function into a key= function
    class K(object):
        def __init__(self, obj, *args):
            self.obj = obj

        # def __cmp__(self, other):
        #     ab = str(self.obj) + str(other)
        #     ba = str(other) + str(self.obj)
        #     res = int(int(ba) > int(ab)) - (int(ba) < int(ab))
        #     return res

        def __lt__(self, other):
            a = mycmp(self.obj, other.obj)
            return a < 0

        # def __gt__(self, other):
        #     a = mycmp(self.obj, other.obj)
        #     return a > 0

        # def __eq__(self, other):
        #     a = mycmp(self.obj, other.obj)
        #     return a == 0
        #
        # def __le__(self, other):
        #     a = mycmp(self.obj, other.obj)
        #     return a <= 0
        #
        # def __ge__(self, other):
        #     a = mycmp(self.obj, other.obj)
        #     return a >= 0
        #
        # def __ne__(self, other):
        #     a = mycmp(self.obj, other.obj)
        #     return a != 0

    return K


# driver code
if __name__ == "__main__":
    a = [1, 34, 3, 98, 9, 76, 45, 4]
    sorted_array = sorted(a, key=myCompare(comparator))
    number = "".join([str(i) for i in sorted_array])
    print(number)

# This code is Contributed by SaurabhTewary
