

class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

    def __repr__(self):
        return str(self.data)


class LinkedList:
    def __init__(self):
        self.head = None

    def insert(self, data):
        if not self.head:
            node = Node(data)
            self.head = node
            return node

        node = self.head
        while node.next is not None:
            node = node.next

        new_node = Node(data)
        node.next = new_node
        return new_node

    def print(self):
        node = self.head
        while node is not None:
            print(node.data, end=' ')
            node = node.next

    def reverse_linked_list(self, k):
        node = self.head
        new_head = prev_node = None
        while node is not None:
            head, node = self.reverse_linked_list_util(node, k-1)
            if not new_head:
                new_head = head
                self.head = new_head
                prev_node = node
            else:
                prev_node.next = head
                prev_node = node
            node = node.next

    def reverse_linked_list_util(self, node, k):
        if node.next is None or k < 1:
            return node, node

        new_head, node_1 = self.reverse_linked_list_util(node.next, k-1)
        if node.next is None or k < 1:
            return new_head, node

        temp = node.next.next
        node.next.next = node
        node.next = temp
        return new_head, node


if __name__ == "__main__":
    lin = LinkedList()
    lin.insert(1)
    lin.insert(2)
    lin.insert(3)
    lin.insert(4)
    lin.insert(5)
    lin.insert(6)
    lin.insert(7)
    lin.insert(8)
    lin.print()
    lin.reverse_linked_list(5)
    print()
    lin.print()
