
# Union Find by Rank


from collections import defaultdict


def find_subset(subsets, node):
    if subsets[node].parent != node:
        subsets[node].parent = find_subset(subsets, subsets[node].parent)
    return subsets[node].parent


def union_by_rank(subsets, u, v):
    if subsets[u].rank > subsets[v].rank:
        subsets[v].parent = u
    elif subsets[u].rank < subsets[v].rank:
        subsets[u].parent = v
    else:
        subsets[v].parent = u
        subsets[u].rank += 1


class Subset:
    def __init__(self, parent, rank=0):
        self.rank = rank
        self.parent = parent

    def __repr__(self):
        return str(self.parent)


class Graph:
    def __init__(self):
        self.vertices = set()
        self.graph = defaultdict(list)

    def add_edge(self, src, dst):
        self.vertices.add(src)
        self.vertices.add(dst)
        self.graph[src].append(dst)

    def print_graph(self):
        for k, v in self.graph.items():
            print(k, "--->", v)

    def is_cycle(self):

        subsets = []
        for u in range(len(self.vertices)):
            subsets.append(Subset(u))

        for u in self.vertices:
            parent_subset = find_subset(subsets, u)
            for v in self.graph[u]:
                child_subset = find_subset(subsets, v)
                if parent_subset == child_subset:
                    return True
                union_by_rank(subsets, parent_subset, child_subset)

        return False


if __name__ == "__main__":
    graph = Graph()
    graph.add_edge(0, 1)
    graph.add_edge(1, 2)
    # graph.add_edge(0, 2)
    print("Printing the graph ")
    graph.print_graph()
    print("Graph is cyclic or not - ", graph.is_cycle())
