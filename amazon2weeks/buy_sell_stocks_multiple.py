
# Buy and Sell Stocks - Completed


def return_maximize_profit(arr):
    profit, idx = 0, 0
    buy = False
    buy_price = None
    while idx < len(arr):
        if not buy and idx == len(arr) - 1:
            break

        if not buy:
            if arr[idx + 1] < arr[idx]:
                idx = idx + 1
                continue

            print("buy at ", idx)
            buy, buy_price = True, arr[idx]
            continue

        if idx == len(arr) - 1:
            print("Sell at ", idx)
            profit = profit - buy_price + arr[idx]
            break

        if arr[idx] < arr[idx+1]:
            idx = idx + 1
            continue

        print("Sell at ", idx)
        profit = profit - buy_price + arr[idx]
        buy, buy_price = False, None

    return profit


if __name__ == "__main__":
    arr = [100, 180, 260, 310, 40, 535, 695]
    print("Maximize profit is ", return_maximize_profit(arr))
