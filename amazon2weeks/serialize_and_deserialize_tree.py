
# Serialize and deserialize binary tree - Completed


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.data)


class BinaryTree:
    def __init__(self):
        self.root = None

    def preorder(self, node):
        if node:
            self.preorder(node.left)
            print(node.data, end=' ')
            self.preorder(node.right)

    def serialize_tree(self):
        node = self.root
        return self.serialized_tree_util("", node)

    def serialized_tree_util(self, serialized_str, node):
        if node is None:
            return serialized_str + "/"

        serialized_str = serialized_str + node.data
        if not node.left and not node.right:
            return serialized_str

        serialized_str = serialized_str + "'"
        serialized_str = self.serialized_tree_util(serialized_str, node.left)
        serialized_str = self.serialized_tree_util(serialized_str, node.right)
        return serialized_str


def deserialize_tree_util(serialize_str, idx):

    if len(serialize_str) == idx or serialize_str[idx] is "/":
        return None, idx

    new_node = Node(serialize_str[idx])

    if idx == len(serialize_str) - 1:
        return new_node, idx

    if serialize_str[idx + 1] == "'":
        new_node.left, updated_index = deserialize_tree_util(serialize_str, idx + 2)
        new_node.right, idx = deserialize_tree_util(serialize_str, updated_index + 1)

    return new_node, idx


def deserialize_tree(serialize_str):
    binary_tree = BinaryTree()
    binary_tree.root, _ = deserialize_tree_util(serialize_str, 0)
    return binary_tree


if __name__ == "__main__":
    binary_tree = BinaryTree()
    binary_tree.root = Node('A')

    binary_tree.root.left = Node('B')
    binary_tree.root.left.left = Node('D')
    binary_tree.root.left.right = Node('E')
    binary_tree.root.left.right.left = Node('G')

    binary_tree.root.right = Node('C')
    binary_tree.root.right.right = Node('F')

    binary_tree.preorder(binary_tree.root)
    print()
    serialize_str = binary_tree.serialize_tree()

    print("Serialized String is - ", serialize_str)

    bt1 = deserialize_tree(serialize_str)
    bt1.preorder(bt1.root)
