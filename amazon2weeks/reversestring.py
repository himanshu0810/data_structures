
# Reverse string - Complete


def return_reverse_string(st):
    return ''.join(reversed(st))


if __name__ == "__main__":
    st = "abvd"
    print("reversed string is ", return_reverse_string(st))
