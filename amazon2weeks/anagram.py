

# Anagram - Complete


def return_anagram_match(str1, str2):
    count1 = [0] * 256
    count2 = [0] * 256

    for ch in str1:
        count1[ord(ch)] = count1[ord(ch)] + 1

    for ch in str2:
        count2[ord(ch)] = count2[ord(ch)] + 1

    for i in range(256):
        if count1[i] != count2[i]:
            return False

    return True

def return_anagram_match_1(str1, str2):
    count1 = {}
    count2 = {}
    for ch in str1:
        if ch not in count1:
            count1[ch] = 1
        else:
            count1[ch] = count1[ch] + 1

    for ch in str2:
        if ch not in count2:
            count2[ch] = 1
        else:
            count2[ch] = count2[ch] + 1

    for key, value in count1.items():
        if key not in count2:
            return False

        if value != count2[key]:
            return False

    return True


if __name__ == "__main__":
    str1 = "geeksforgeeks"
    str2 = "forgeekssgeeks"

    print("Both strings are anagram or not - ", return_anagram_match_1(str1, str2))
