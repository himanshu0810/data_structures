
# Bubble sort - O(n2)


def return_sorted_array(ar):

    for i in range(1, len(ar)):
        for j in range(1, len(ar)-i):
            if ar[j-1] > ar[j]:
                ar[j], ar[j-1] = ar[j-1], ar[j]


    return ar


if __name__ == "__main__":
    ar = [10, 1, 4, 9, 13]
    print(return_sorted_array(ar))

