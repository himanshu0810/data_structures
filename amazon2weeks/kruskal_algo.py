
# Complete - Kruskal Algorithm using union find by rank


from pprint import pprint

from amazon2weeks.union_find_by_rank import Subset, find_subset, union_by_rank


class Graph:
    def __init__(self):
        self.vertices = set()
        self.graph = []

    def add_edge(self, src, dst, weight):
        self.vertices.add(src)
        self.vertices.add(dst)
        self.graph.append([src, dst, weight])

    def print_graph(self):
        pprint(self.graph)

    def kruskal_algo(self):

        sorted_edges = sorted(self.graph, key=lambda item: item[2])
        pprint(sorted_edges)
        print("----------------------")
        i = 0
        result = []

        subsets = []
        for u in range(len(self.vertices)):
            subsets.append(Subset(u))

        V = len(self.vertices)
        e = 0
        while e < V:

            edge = sorted_edges[i]
            u, v, w = edge
            i = i + 1
            u_parent = find_subset(subsets, u)
            v_parent = find_subset(subsets, v)
            if u_parent == v_parent:
                continue

            result.append([u, v, w])
            union_by_rank(subsets, u, v)
            e = e + 1

        pprint(result)


if __name__ == "__main__":
    graph = Graph()

    graph.add_edge(0, 1, 4)
    graph.add_edge(0, 7, 8)
    graph.add_edge(1, 7, 11)
    graph.add_edge(1, 2, 8)
    graph.add_edge(2, 3, 7)
    graph.add_edge(2, 5, 4)
    graph.add_edge(2, 2, 8)
    graph.add_edge(3, 4, 9)
    graph.add_edge(3, 5, 14)
    graph.add_edge(4, 5, 10)
    graph.add_edge(5, 6, 2)
    graph.add_edge(6, 8, 6)
    graph.add_edge(6, 7, 1)
    graph.add_edge(7, 8, 7)

    graph.print_graph()
    print("Running kruskal Algo")
    graph.kruskal_algo()
