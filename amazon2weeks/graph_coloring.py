
# Graph Coloring - In Progress

from collections import defaultdict


class Graph:
    def __init__(self):
        self.graph = defaultdict(list)
        self.vertices = set()

    def add_edge(self, src, dst):
        self.vertices.add(src)
        self.vertices.add(dst)
        self.graph[src].append(dst)

    def print_graph(self):
        for k, v in self.graph.items():
            print(k, "--->", v)

    def return_color_for_each_vertex(self):
        output = {}
        return self.return_color_for_each_vertex_util()

    def return_color_for_each_vertex_util(self, vertex, ):
        pass


if __name__ == "__main__":
    graph = Graph()
    graph.add_edge(0, 1)
    graph.add_edge(0, 2)
    graph.add_edge(0, 3)
    graph.add_edge(1, 3)
    graph.add_edge(2, 0)
    graph.add_edge(2, 1)

    print("Printing the graph", "\n")
    graph.print_graph()

