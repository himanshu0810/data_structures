

class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

class LinkedList:
    def __init__(self):
        self.root = None

    def insert(self, data):
        node = Node(data)
        if not self.root:
            self.root = node
            return self.root

        head = self.root
        while head.next != None:
            head = head.next

        head.next = node
        return node

    def traverse(self, root):
        head = root
        while head != None:
            print(head.data, end=' ')
            head = head.next


def reverse_list(root):
    if root.next == None:
        return root

    remaining = reverse_list(root.next)
    root.next.next = root
    root.next = None
    return remaining

if __name__ == "__main__":
    li = LinkedList()
    root = li.insert(10)
    li.insert(5)
    li.insert(7)
    li.insert(1)

    # li.traverse(root)
    root = reverse_list(root)
    li.traverse(root)