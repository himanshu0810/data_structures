from classes.minheap import MinHeap

if __name__ == "__main__":
    min_heap = MinHeap()
    min_heap.insert(10)
    min_heap.insert(7)
    min_heap.insert(3)
    min_heap.insert(11)
    min_heap.insert(4)
    min_heap.insert(1)
    min_heap.remove(1)
    print(min_heap.elements)
