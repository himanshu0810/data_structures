import sys
import os


# Create words dict from the given file
def create_words_dict(filename):
    words_dict = {}
    file = open(filename, 'r')

    if not os.path.isfile(filename):
        raise Exception("Please provide the required dictionary file")

    for word in file:
        words_dict[word.strip()] = True

    return words_dict


# Anagrams of a given word util
def find_anagrams_of_a_words_util(all_words, words_list, current_word):
    if not words_list:
        all_words.add(current_word)
        return all_words

    for each in words_list:
        copy_word_list = words_list[:]
        copy_word_list.remove(each)
        find_anagrams_of_a_words_util(all_words, copy_word_list, current_word+each)

    return all_words


# find anagrams of a given word
def find_anagrams_of_a_words(word):
    all_words = set()
    return find_anagrams_of_a_words_util(all_words, list(word), "")


# Find anagrams and check each anagram in words_dict
def check_and_return_results(words_dict, word):

    anagrams = find_anagrams_of_a_words(word)

    if not anagrams:
        print("No word found ")
        return

    output = [each for each in anagrams if each in words_dict]
    return output


if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("Please provide the jumbled word")
        sys.exit(1)

    word = sys.argv[1]
    if not word:
        print("Please enter the proper word")
        sys.exit(1)

    filename = 'dictionary.txt'
    words_dict = create_words_dict('dictionary.txt')
    output = check_and_return_results(words_dict, word)
    print(output)
