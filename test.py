

from unittest import TestCase

from solver import create_words_dict, check_and_return_results, find_anagrams_of_a_words


class JumbleSolverTest(TestCase):
    def setUp(self):
        self.words_dict = create_words_dict('dictionary.txt')

    def test_empty_word(self):
        word = ""
        output = check_and_return_results(self.words_dict, word)
        self.assertEqual(output, [])

    def test_valid_output(self):
        word = "bat"
        output = check_and_return_results(self.words_dict, word)
        self.assertEqual(len(output), 2)

    def test_valid_output_type(self):
        word = "bat"
        output = check_and_return_results(self.words_dict, word)
        self.assertIsInstance(output, list)

    def test_return_anagrams(self):
        word = "cat"
        output = find_anagrams_of_a_words(word)
        self.assertEqual(output, {'tac', 'cat', 'act', 'atc', 'tca', 'cta'})

    def test_return_anagrams_type(self):
        word = "cat"
        output = find_anagrams_of_a_words(word)
        self.assertIsInstance(output, set)
