


class Node:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class Bst:
    def __init__(self):
        self.root = None

    def inorder(self, root=None):
        if not root:
            root= self.root

        if root.left:
            self.inorder(root.left)
        print(root.data, end=' ')

        if root.right:
            self.inorder(root.right)

    def insert_node(self, data):
        if self.root == None:
            self.root = Node(data)
            return self.root

        head = self.root
        node = Node(data)
        while head != None:
            if data > head.data:
                if not head.right:
                    head.right = node
                    break
                head = head.right
            else:
                if not head.left:
                    head.left = node
                    break
                head = head.left

        return node

    def findMin(self, root):
        while root.left != None:
            root = root.left
        return root

    def deleteanode(self, root, data):
        if root == None:
            return

        if data < root.data:
            root.left = self.deleteanode(root.left, data)
        elif data > root.data:
            root.right = self.deleteanode(root.right, data)
        else:
            if root.left == None and root.right == None:
                root = None
            elif root.left == None:
                root = root.right
            elif root.right == None:
                root = root.left
            else:
                temp = self.findMin(root.right)
                root.data = temp.data
                root.right = self.deleteanode(root.right, temp.data)
        return root


if __name__ == "__main__":
    bst = Bst()
    root = bst.insert_node(10)
    bst.insert_node(5)
    bst.insert_node(15)
    bst.insert_node(1)
    bst.insert_node(7)
    bst.insert_node(11)
    bst.insert_node(18)
    bst.insert_node(6)
    bst.insert_node(13)
    bst.insert_node(20)
    bst.insert_node(0)
    # In Order traversal
    bst.inorder(root)
    bst.deleteanode(root, 10)
    print("")
    bst.inorder(root)
