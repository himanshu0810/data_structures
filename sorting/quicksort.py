
def quick_sort(arr, low, high):
    l = low
    pivot = arr[high]
    h = high - 1
    while l < h:
        if arr[h] > pivot:
            h = h - 1
        elif arr[l] < pivot:
            l = l + 1
        else:
            arr[h], arr[l] = arr[l], arr[h]

    if arr[l] < pivot:
        arr[high], arr[l + 1] = arr[l + 1], arr[high]
        return l + 1

    arr[high], arr[l] = arr[l], arr[high]
    return l


def partition(array, low, high):
    if low < high:
        p = quick_sort(array, low, high)
        partition(array, low, p-1)
        partition(array, p+1, high)


if __name__ == "__main__":
    array = [3, 1, 4, 5, 2]
    partition(array, 0, len(array)-1)
    print("array is ", array)
