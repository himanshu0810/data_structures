

def merge_array(left_arr, right_arr):
    i = 0
    j = 0
    merged_arr = []
    while i < len(left_arr) and j < len(right_arr):
        if left_arr[i] < right_arr[j]:
            merged_arr.append(left_arr[i])
            i = i + 1
            continue
        merged_arr.append(right_arr[j])
        j = j + 1

    while i < len(left_arr):
        merged_arr.append(left_arr[i])
        i = i + 1

    while j < len(right_arr):
        merged_arr.append(right_arr[j])
        j = j + 1

    return merged_arr


def merge_sort(arr, low, right):

    if low < right:
        middle = (right - low) // 2
        left_arr = merge_sort(arr[low:middle+1], low, middle)

        right_arr = arr[middle+1:right+1]
        right_arr = merge_sort(right_arr, 0, len(right_arr)-1)

        merged_arr = merge_array(left_arr, right_arr)
        return merged_arr

    return arr


if __name__ == "__main__":
    arry = [5, 4, 1, 10, 9]
    sorted_array = merge_sort(arry, 0, len(arry)-1)
    print("Sorted array is ", sorted_array)
