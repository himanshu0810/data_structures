

def selection_sort(list):

    for i in range(len(list)-1):
        print("after each iteration  - ", list)
        min = list[i]
        idx = i
        for j in range(i, len(list)-1):
            if list[j] < min:
                min = list[j]
                idx = j
        temp = list[i]
        list[idx] = temp
        list[i] = min
    return list


def insertion_sort(list):

    for i in range(1, len(list)):
        temp = list[i]
        for j in range(i):
            temp2 = list[j]
            if temp2 > temp:
                list[j] = temp
                temp = temp2
        list[i] = temp
    return list


def bubble_sort(list):
    for i in range(len(li) - 1):
        for j in range(len(li) - i - 1):
            if list[j] > list[j + 1]:
                list[j+1], list[j] = list[j], list[j + 1]
    return list


def merge_sort(list):

    if len(list) == 1:
        return list
    mid = len(list) // 2
    left_part = merge_sort(list[:mid])
    right_part = merge_sort(list[mid:])
    merged_list = merge_part(left_part, right_part)
    return merged_list

def merge_part(left_part, right_part):
    i = j = 0
    merged_list = []
    while i < len(left_part) and j < len(right_part):
        if left_part[i] < right_part[j]:
            merged_list.append(left_part[i])
            i = i + 1
        else:
            merged_list.append(right_part[j])
            j = j + 1

    while i < len(left_part):
        merged_list.append(left_part[i])
        i = i + 1
    while j < len(right_part):
        merged_list.append(right_part[j])
        j = j + 1
    print("coming here in each iteration - ", merged_list)
    return merged_list


def counting_sort(list):
    index_counted_list = [0 for _ in range(9)]
    for each in list:
        index_counted_list[each-1] = index_counted_list[each-1] + 1

    sorted_list = []
    for idx, each in enumerate(index_counted_list):
        for _ in range(each):
            sorted_list.append(idx+1)

    return sorted_list


def partition(list, start, end):
    pathindex = start
    for each in range(start, end):
        if list[each] < list[end]:
            if each != pathindex:
                list[each], list[pathindex] = list[pathindex], list[each]
            pathindex = pathindex + 1
    list[end], list[pathindex] = list[pathindex], list[end]
    return pathindex


def quick_sort(list, start, end):
    if start < end:
        pindex = partition(list, start, end)
        quick_sort(list, start, pindex-1)
        quick_sort(list, pindex+1, end)
    return list

if __name__ == "__main__":
    # counted_unsorted_list = [3, 2, 1, 6, 2, 9, 3]
    li = [9, 12,41,1,3,10,11]

    # sorted_li = selection_sort(li)
    # sorted_li = insertion_sort(li)
    # sorted_li = bubble_sort(li)
    # sorted_li = counting_sort(counted_unsorted_list)
    sorted_li = quick_sort(li, 0, 6)

    print(sorted_li)
