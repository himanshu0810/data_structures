import hashlib
import bisect


class ConsistentHashing:
    def __init__(self, replicas):
        self.nodes = {}
        self.keys = []
        self.replicas = replicas

    def _hash(self, key):
        return int(hashlib.md5(key.encode()).hexdigest(), 16)

    def _repl_iteration(self, nodename):
        return (self._hash(nodename + str(i)) for i in range(self.replicas))

    def __setitem__(self, nodename, node):
        for hash_ in self._repl_iteration(nodename):
            if hash_ in self.nodes:
                raise ValueError("Hash Already exists")
            self.nodes[hash_] = node
            bisect.insort(self.keys, hash_)

    def __getitem__(self, item):
        hash_ = self._hash(item)
        index = bisect.bisect(self.keys, hash_)
        if index == len(self.keys):
            index = 0
        return self.nodes[self.keys[index]]

    def __delitem__(self, key):
        for hash_ in self._repl_iteration(key):
            del self.nodes[hash_]
            index = bisect.bisect_left(self.keys, hash_)
            del self.keys[index]


if __name__ == "__main__":
    cr = ConsistentHashing(5)
    cr["node1"] = "himanshu"
    cr["node2"] = "yo rocks"

    client = cr["some_key"]
    print(client)
    client = cr["some_key1"]
    print(client)
    client = cr["some_key2"]
    print(client)
    del cr["node1"]
